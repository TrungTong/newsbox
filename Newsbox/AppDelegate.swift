//
//  AppDelegate.swift
//  Newsbox
//
//  Created by Trung Tong on 1/31/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    UserDefaults.standard.set(true, forKey: ConstantUD.isCellFull)
    
    self.handleNotify(launchOptions: launchOptions)
    
    OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
    
    // Recommend moving the below line to prompt for push after informing the user about
    //   how your app will use them.
    OneSignal.promptForPushNotifications(userResponse: { accepted in
      print("User accepted notifications: \(accepted)")
    })
    self.configureUI()
    self.checkSetRoot()
    let BarButtonItemAppearance = UIBarButtonItem.appearance()
    BarButtonItemAppearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.clear], for: .normal)
    
    return true
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }
  
  private func configureUI() {
    let navigationBarAppearace = UINavigationBar.appearance()
    
    navigationBarAppearace.tintColor = UIColor.white
    navigationBarAppearace.barTintColor = normalBackground
    // change navigation item title color
    navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
  }
  func setMainRootViewController() {
    let storyboard = UIStoryboard(name:"Main", bundle: nil)
    
    if let mainVC = storyboard.instantiateViewController(withClass: TabBarController.self) {
      self.window?.rootViewController = mainVC
      self.getAllData()
    }
  }
  
  func setLoginRootViewController() {
    let storyboard = UIStoryboard(name:"Main", bundle: nil)
    
    if let loginVC = storyboard.instantiateViewController(withClass: LoginViewController.self) {
      let nvc: UINavigationController = UINavigationController(rootViewController: loginVC)
      self.window?.rootViewController = nvc
    }
  }
  func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
      do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
      } catch {
        print(error.localizedDescription)
      }
    }
    return nil
  }
  
  func handleNotify(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
    let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
      
      print("Received Notification: \(notification!.payload.notificationID)")
    }
    
    let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
      // This block gets called when the user reacts to a notification received
      let payload: OSNotificationPayload = result!.notification.payload
      
      if let fullMessage = payload.additionalData as? [String: String], let idNews = fullMessage["id"] {
        if let tabbar = self.window?.rootViewController as? TabBarController, let vcArray = tabbar.viewControllers, let navi = vcArray[0] as? UINavigationController  {
         tabbar.selectedIndex = 0
          var navigationArray = navi.viewControllers
          for _ in 1..<navigationArray.count {
            navigationArray.removeLast()
          }
          let storyboard = UIStoryboard(name:"Main", bundle: nil)
          if let newsDetailVC = storyboard.instantiateViewController(withClass: NewsDetailsViewController.self) {
            newsDetailVC.idNews = idNews
            navigationArray.append(newsDetailVC)
          }
          navi.viewControllers = navigationArray
        } else {
           (self.window?.rootViewController as? UITabBarController)?.selectedIndex = 0
          let storyboard = UIStoryboard(name:"Main", bundle: nil)
          if let newsDetailVC = storyboard.instantiateViewController(withClass: NewsDetailsViewController.self) {
            newsDetailVC.idNews = idNews
          self.window?.rootViewController?.navigationController?.pushViewController(newsDetailVC, animated: true)
          }
        }
      }
      
    }
    
    let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: true,
                                 kOSSettingsKeyInAppLaunchURL: false,
                                 kOSSSettingsKeyPromptBeforeOpeningPushURL: false]
    
    OneSignal.initWithLaunchOptions(launchOptions,
                                    appId: "5c8a77b2-6471-4226-aa07-d4b15316650a",
                                    handleNotificationReceived: notificationReceivedBlock,
                                    handleNotificationAction: notificationOpenedBlock,
                                    settings: onesignalInitSettings)
    
    OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
  }
  private func checkSetRoot() {
    if UserManager.shareInstance.isLogin {
      self.setMainRootViewController()
    } else {
      self.setLoginRootViewController()
    }
  }
  
  private func getAllData() {
    DataManager.shareInstance.listSourceType = typeDomain.allValues
    DataManager.shareInstance.listSourceTypeDefault = typeDomain.allValues
    DispatchQueue.global(qos: .background).async {
      let shareService = ShareService()
      shareService.getListUser { listUser, _ in
        if let users = listUser {
          DataManager.shareInstance.listUser = users
        }
      }
      DataManager.shareInstance.getListSubject()
      let dispatchGroup = DispatchGroup()
      
      dispatchGroup.enter()
      DataManager.shareInstance.getUserInfo(completion: {
        dispatchGroup.leave()
      })
      dispatchGroup.enter()
      DataManager.shareInstance.getListCategoryDefault(completion: {
        dispatchGroup.leave()
      })
      dispatchGroup.enter()
      DataManager.shareInstance.getListLanguageDefault(completion: {
        dispatchGroup.leave()
      })
      dispatchGroup.enter()
      DataManager.shareInstance.getListCountryDefault(completion: {
        dispatchGroup.leave()
      })
      dispatchGroup.enter()
      DataManager.shareInstance.getListRegionDefault(completion: {
        dispatchGroup.leave()
      })
      dispatchGroup.notify(queue: .global()) {
        print(" 👍 hoan thanh lay du lieu bat dau filter")
        DataManager.shareInstance.getListCountry()
        DataManager.shareInstance.getListCategory()
        DataManager.shareInstance.getListLanguage()
        DataManager.shareInstance.getListRegion()
        DataManager.shareInstance.getListSourceType()
      }
      DataManager.shareInstance.getListSource()
    }
  }
}

