//
//  Issue.swift
//  Newsbox
//
//  Created by TrungTong on Feb/19/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import ObjectMapper

class Issue : Mappable {
  private struct SerializationKeys {
    static let idIssue                      = "id"
    static let title                        = "title"
    static let duedate                      = "duedate"
    static let createdBy                    = "created_by"
    static let updatedBy                    = "updated_by"
    static let description                  = "description"
    static let updatedTime                  = "updated_time"
    static let createdTime                  = "created_time"
    static let assignees                    = "assignees"
    static let completed                    = "completed"
    static let starred                      = "starred"
    static let important                    = "important"
    static let deleted                      = "deleted"
    static let tags                         = "tags"
  }
  var idIssue: Int?
  var title: String?
  var duedate: String?
  var createdBy: Int?
  var updatedBy: Int?
  var description: String?
  var updatedTime: String?
  var createdTime: String?
  var assignees = [Int]()
  var completed: Bool?
  var starred: Bool?
  var important: Bool?
  var deleted: Bool?
  var tags = [Int]()
  
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    idIssue                                 <- map[SerializationKeys.idIssue]
    title                                   <- map[SerializationKeys.title]
    duedate                                 <- map[SerializationKeys.duedate]
    createdBy                               <- map[SerializationKeys.createdBy]
    updatedBy                               <- map[SerializationKeys.updatedBy]
    description                             <- map[SerializationKeys.description]
    updatedTime                             <- map[SerializationKeys.updatedTime]
    createdTime                             <- map[SerializationKeys.createdTime]
    assignees                               <- map[SerializationKeys.assignees]
    completed                               <- map[SerializationKeys.completed]
    starred                                 <- map[SerializationKeys.starred]
    important                               <- map[SerializationKeys.important]
    deleted                                 <- map[SerializationKeys.deleted]
    tags                                    <- map[SerializationKeys.tags]
  }
}

class SubIssue : Mappable {
  private struct SerializationKeys {
    static let count                    = "count"
    static let next                     = "next"
    static let previous                 = "previous"
    static let results                  = "results"
  }
  var count: Int?
  var next: String?
  var previous: String?
  var results = [Issue]()
  
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    count                               <- map[SerializationKeys.count]
    next                                <- map[SerializationKeys.next]
    previous                            <- map[SerializationKeys.previous]
    results                             <- map[SerializationKeys.results]
  }
}
