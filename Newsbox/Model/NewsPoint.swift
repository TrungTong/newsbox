//
//  NewsPoint.swift
//  Newsbox
//
//  Created by TrungTong on Feb/21/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import ObjectMapper

class NewsPoint: Mappable {
  
  private struct SerializationKeys {
    static let description              = "description"
    static let title                    = "title"
    static let creator                  = "creator"
    static let updatedTime              = "updated_time"
    static let notify                   = "notify"
    static let idNewsPoint              = "id"
  }
  var description: String?
  var title: String?
  var creator: String?
  var updatedTime: String?
  var notify: Bool?
  var idNewsPoint: String?
  
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    description                         <- map[SerializationKeys.description]
    title                               <- map[SerializationKeys.title]
    creator                             <- map[SerializationKeys.creator]
    updatedTime                         <- map[SerializationKeys.updatedTime]
    notify                              <- map[SerializationKeys.notify]
    idNewsPoint                         <- map[SerializationKeys.idNewsPoint]
  }
}
class SubNewsPoint: Mappable {
  private struct SerializationKeys {
    static let count              = "count"
    static let result             = "result"
  }
  var count: Int?
  var result = [NewsPoint]()

  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    count                       <- map[SerializationKeys.count]
    result                      <- map[SerializationKeys.result]
  }
}
