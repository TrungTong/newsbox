//
//  Subjects.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation

import ObjectMapper
class SearchQuery: NSObject, Mappable {
  private struct SerializationKeys {
    static let category                           = "category"
    static let lang                               = "lang"
    static let domain                             = "domain"
    static let from                               = "from"
    static let sourceType                         = "sourcetype"
    static let source                             = "source"
    static let country                            = "country"
    static let region                             = "region"
    static let search                             = "search"
    static let page_numbber                       = "page_numbber"
    static let to                                 = "to"
    static let time                               = "time"
  }
  var category = [String]()
  var lang = [String]()
  var domain = [String]()
  var from: String?
  var sourceType = [String]()
  var source = [String]()
  var country = [String]()
  var region = [String]()
  var search: String?
  var to: String?
  var time: String?
  // MARK: - Init
  override init() {
    
  }
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    category                                <- map[SerializationKeys.category]
    lang                                    <- map[SerializationKeys.lang]
    domain                                  <- map[SerializationKeys.domain]
    from                                    <- map[SerializationKeys.from]
    sourceType                              <- map[SerializationKeys.sourceType]
    source                                  <- map[SerializationKeys.source]
    country                                 <- map[SerializationKeys.country]
    region                                     <- map[SerializationKeys.region]
    search                                    <- map[SerializationKeys.search]
    to                                 <- map[SerializationKeys.to]
    time                                  <- map[SerializationKeys.time]
  }
}

class Subject: NSObject, Mappable {
    private struct SerializationKeys {
        static let notify                           = "notify"
        static let matchedCount                     = "matched_count"
        static let userId                           = "user_id"
        static let name                             = "name"
        static let searchQuery                      = "search_query"
        static let avatar                           = "avatar"
        static let hidden                           = "hidden"
        static let _id                              = "_id"
        static let type                             = "type"
        static let channel                          = "channel"
        static let shares                           = "shares"
    }
    var notify: String?
    var matchedCount: Int?
    var userId: Int?
    var name: String?
    var searchQuery: SearchQuery?
    var avatar: String?
    var hidden: Bool?
    var _id: String?
    var type: String?
    var channel: String?
    var shares = [String]()
    // MARK: - Init
    override init() {
        
    }
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        notify                                  <- map[SerializationKeys.notify]
        matchedCount                            <- map[SerializationKeys.matchedCount]
        userId                                  <- map[SerializationKeys.userId]
        name                                    <- map[SerializationKeys.name]
        searchQuery                             <- map[SerializationKeys.searchQuery]
        avatar                                  <- map[SerializationKeys.avatar]
        hidden                                  <- map[SerializationKeys.hidden]
        _id                                     <- map[SerializationKeys._id]
        type                                    <- map[SerializationKeys.type]
        channel                                 <- map[SerializationKeys.channel]
        shares                                  <- map[SerializationKeys.shares]
    }
}
