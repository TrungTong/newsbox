//
//  Source.swift
//  Newsbox
//
//  Created by Trung Tong on 2/7/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import ObjectMapper

class Details: Mappable {
    private struct SerializationKeys {
        static let body                     = "body"
        static let title                    = "title"
        static let image                    = "image"
        static let author                   = "author"
        static let subcontent               = "subcontent"
        static let time                     = "time"
    }
    var body: String?
    var title: String?
    var image: String?
    var author: String?
    var subcontent: String?
    var time: String?
    
    // MARK: - Init
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        body                                <- map[SerializationKeys.body]
        title                               <- map[SerializationKeys.title]
        image                               <- map[SerializationKeys.image]
        author                              <- map[SerializationKeys.author]
        subcontent                          <- map[SerializationKeys.subcontent]
        time                                <- map[SerializationKeys.time]
    }
}

class Source: Mappable {
    private struct SerializationKeys {
        static let idSource                     = "_id"
        static let name                         = "name"
        static let domain                       = "domain"
        static let logo                         = "logo"
        static let url                          = "url"
        static let crawlingTime                 = "crawling_time"
        static let status                       = "status"
        static let error                        = "error"
        static let lastModified                 = "last_modified"
        static let message                      = "message"
        static let total                        = "total"
        static let lastestNews                  = "lastest_news"
//        static let area                         = "area"
//
//        static let enable                       = "enable"
//        static let urlTest                      = "url_test"
//        static let lang                         = "lang"
//        static let language                     = "language"
//        static let country                      = "country"
//        static let region                       = "region"
//        static let category                     = "category"
//        static let interval                     = "interval"
//        static let userId                       = "user_id"
//        static let headers                      = "headers"
//        static let updatedTime                  = "updated_time"
//        static let createdTime                  = "created_time"
//        static let sourceType                   = "sourcetype"
//        static let newsInterval                 = "news_interval"
//        static let details                      = "details"
//
        
    }
    var domain: String?
    var idSource: String?
    var name: String?
    var logo: String?
    var url: String?
    var crawlingTime: Float?
    var status: String?
    var error: Bool?
    var lastModified: String?
     var message: String?
    var total: Int?
    var lastestNews: String?
  //  var area: String?
    
//    var enable: String?
//    var urlTest: String?
//    var lang: String?
//    var language: String?
//    var country: String?
//    var region: String?
//    var category: String?
//    var interval: Int?
//    var userId: Int?
//    var headers: String?
//    var updatedTime: String?
//    var createdTime: String?
//    var sourceType: Int?
//    var newsInterval: Int?
//    var details: Details?
    
    // MARK: - Init
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        domain                              <- map[SerializationKeys.domain]
        idSource                            <- map[SerializationKeys.idSource]
        name                                <- map[SerializationKeys.name]
        logo                                <- map[SerializationKeys.logo]
        url                                 <- map[SerializationKeys.url]
        crawlingTime                        <- map[SerializationKeys.crawlingTime]
        status                              <- map[SerializationKeys.status]
        error                               <- map[SerializationKeys.error]
        lastModified                        <- map[SerializationKeys.lastModified]
        message                             <- map[SerializationKeys.message]
        total                               <- map[SerializationKeys.total]
        lastestNews                         <- map[SerializationKeys.lastestNews]
       // area                                <- map[SerializationKeys.area]
        
//        enable                              <- map[SerializationKeys.enable]
//        urlTest                             <- map[SerializationKeys.urlTest]
//        lang                                <- map[SerializationKeys.lang]
//        language                            <- map[SerializationKeys.language]
//        country                             <- map[SerializationKeys.country]
//        region                              <- map[SerializationKeys.region]
//        category                            <- map[SerializationKeys.category]
//        interval                            <- map[SerializationKeys.interval]
//        userId                              <- map[SerializationKeys.userId]
//        headers                             <- map[SerializationKeys.headers]
//        updatedTime                         <- map[SerializationKeys.updatedTime]
//        createdTime                         <- map[SerializationKeys.createdTime]
//        sourceType                          <- map[SerializationKeys.sourceType]
//        newsInterval                        <- map[SerializationKeys.newsInterval]
//        details                             <- map[SerializationKeys.details]
    }
}
