//
//  News.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import ObjectMapper

class Comment: Mappable {
  private struct SerializationKeys {
    static let created_time              = "created_time"
    static let message                   = "message"
    static let fromPic                   = "from"
  }
  var created_time: String?
  var message: String?
  var fromPic: FromPictureFB?
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    created_time                       <- map[SerializationKeys.created_time]
    message                            <- map[SerializationKeys.message]
    fromPic                            <- map[SerializationKeys.fromPic]
  }
}
class Reaction: Mappable {
  private struct SerializationKeys {
    static let love              = "love"
    static let like              = "like"
    static let angry              = "angry"
    static let haha              = "haha"
    static let sad              = "sad"
    static let wow              = "wow"
  }
  var love: Int?
  var like: Int?
  var angry: Int?
  var haha: Int?
  var sad: Int?
  var wow: Int?
  
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    love                       <- map[SerializationKeys.love]
    like                       <- map[SerializationKeys.like]
    angry                       <- map[SerializationKeys.angry]
    haha                       <- map[SerializationKeys.haha]
    sad                       <- map[SerializationKeys.sad]
    wow                       <- map[SerializationKeys.wow]
  }
}

class DataPic: Mappable {
  private struct SerializationKeys {
    static let urlPic              = "url"
  }
  var urlPic: String?

  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    urlPic                       <- map[SerializationKeys.urlPic]
  }
}

class PictureFB: Mappable {
  private struct SerializationKeys {
    static let dataPic              = "data"
  }
  var dataPic: DataPic?
  
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    dataPic                       <- map[SerializationKeys.dataPic]
  }
}

class FromPictureFB: Mappable {
  private struct SerializationKeys {
    static let pictureFB              = "picture"
    static let name                   = "name"
  }
  var pictureFB: PictureFB?
  var name: String?
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    pictureFB                       <- map[SerializationKeys.pictureFB]
    name                            <- map[SerializationKeys.name]
  }
}
class News: Mappable {
  
  private struct SerializationKeys {
    static let shares                   = "shares"
    static let uniqueId                 = "unique_id"
    static let lang                     = "lang"
    static let domain                   = "domain"
    static let image                    = "image"
    static let logo                     = "logo"
    static let collectedTime            = "collected_time"
    static let idNews                   = "id"
    static let category                 = "category"
    static let title                    = "title"
    static let url                      = "url"
    static let country                  = "country"
    static let region                   = "region"
    static let source                   = "source"
    static let time                     = "time"
    static let body                     = "body"
    static let name                     = "name"
    static let search                   = "search"
    static let link                     = "link"
    static let root                     = "root"
    static let fromPic                  = "from"
    static let message                  = "message"
    static let reactions                = "reactions"
    static let comments                 = "comments"
    static let fbInfo                   = "fb_info.from"
  }
  var comments = [Comment]()
  var reactions: Reaction?
  var fromPic: FromPictureFB?
  var fbInfo: FromPictureFB?
  var lang: String?
  var domain: String?
  var image: String?
  var logo: String?
  var collectedTime: String?
  var idNews: String?
  var category: String?
  var title: String?
  var url: String?
  var country: String?
  var region: String?
  var source: String?
  var time: String?
  var body: String?
  var name: String?
  var uniqueId: Int?
  var search: String?
  var link: String?
  var root: String?
  var message: String?
  var subcontent: String?
  var shares = [Int]()
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    comments                          <- map[SerializationKeys.comments]
    reactions                         <- map[SerializationKeys.reactions]
    fromPic                           <- map[SerializationKeys.fromPic]
    root                              <- map[SerializationKeys.root]
    lang                              <- map[SerializationKeys.lang]
    domain                            <- map[SerializationKeys.domain]
    image                             <- map[SerializationKeys.image]
    logo                              <- map[SerializationKeys.logo]
    collectedTime                     <- map[SerializationKeys.collectedTime]
    idNews                            <- map[SerializationKeys.idNews]
    category                          <- map[SerializationKeys.category]
    title                             <- map[SerializationKeys.title]
    url                               <- map[SerializationKeys.url]
    country                           <- map[SerializationKeys.country]
    region                            <- map[SerializationKeys.region]
    source                            <- map[SerializationKeys.source]
    time                              <- map[SerializationKeys.time]
    body                              <- map[SerializationKeys.body]
    name                              <- map[SerializationKeys.name]
    uniqueId                          <- map[SerializationKeys.uniqueId]
    search                            <- map[SerializationKeys.search]
    link                              <- map[SerializationKeys.link]
    message                           <- map[SerializationKeys.message]
    fbInfo                            <- map[SerializationKeys.fbInfo]
    subcontent                        <- map["subcontent"]
    shares                            <- map[SerializationKeys.shares]
    
  }
}
class HitChild: Mappable {
  private struct SerializationKeys {
    static let _type                  = "_type"
    static let _index                 = "_index"
    static let  sort                  = "sort"
    static let  _source               = "_source"
    static let _id                    = "_id"
  }
  
  var _type: String?
  var _index: String?
  var sort = [Double]()
  var _source: News?
  var _id: String?
  
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    _type                       <- map[SerializationKeys._type]
    _index                      <- map[SerializationKeys._index]
    sort                        <- map[SerializationKeys.sort]
    _source                     <- map[SerializationKeys._source]
    _id                         <- map[SerializationKeys._id]
  }
}

class Hit: Mappable {
  private struct SerializationKeys {
    static let Hitchild                  = "hits"
  }
  
  var Hitchild = [HitChild]()
  
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    Hitchild                       <- map[SerializationKeys.Hitchild]
  }
}

class Post: Mappable {
  private struct SerializationKeys {
    static let Hit                  = "hits"
  }
  
  var Hit: Hit?
  
  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    Hit                       <- map[SerializationKeys.Hit]
  }
}

class Item: Mappable {
  private struct SerializationKeys {
    static let key                  = "key"
    static let docCount             = "doc_count"
    static let posts                = "posts"
  }
  
  var key: String?
  var docCount: Int?
  var posts: Post?

  // MARK: - Init
  required init?(map: Map) {
    
  }
  func mapping(map: Map) {
    key                       <- map[SerializationKeys.key]
    docCount                  <- map[SerializationKeys.docCount]
    posts                    <- map[SerializationKeys.posts]
  }
}
