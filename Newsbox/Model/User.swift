//
//  User.swift
//  Newsbox
//
//  Created by Trung Tong on 2/9/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import ObjectMapper

class Settings: Mappable {
    private struct SerializationKeys {
        static let category              = "category"
        static let country               = "country"
        static let region                = "region"
        static let lang                  = "lang"
        static let sourceType            = "sourcetype"
    }
    var category = [String]()
    var country = [String]()
    var region = [String]()
    var lang = [String]()
    var sourcetype = [String]()
  
    // MARK: - Init
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        category                       <- map[SerializationKeys.category]
        country                        <- map[SerializationKeys.country]
        region                         <- map[SerializationKeys.region]
        lang                           <- map[SerializationKeys.lang]
        sourcetype                     <- map[SerializationKeys.sourceType]
    }
}

class User: Mappable {
    private struct SerializationKeys {
        static let idUser                       = "id"
        static let username                     = "username"
        static let firstName                    = "first_name"
        static let lastName                     = "lastName"
        static let email                        = "email"
        static let phoneNumber                  = "phone_number"
        static let userRole                     = "user_role"
        static let role                         = "role"
        static let following                    = "following"
        static let dateJoined                   = "date_joined"
        static let lastLogin                    = "last_login"
        static let isSuperuser                  = "is_superuser"
        static let isStaff                      = "is_staff"
        static let settings                     = "settings"
    }
    var idUser: Int?
    var username: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var phoneNumber: String?
    var userRole: String?
    var role: Int?
    var following = [String]()
    var dateJoined: String?
    var lastLogin: String?
    var isSuperuser: Bool?
    var isStaff: Bool?
    var settings: Settings?

    // MARK: - Init
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        idUser                              <- map[SerializationKeys.idUser]
        username                            <- map[SerializationKeys.username]
        firstName                           <- map[SerializationKeys.firstName]
        lastName                            <- map[SerializationKeys.lastName]
        email                               <- map[SerializationKeys.email]
        phoneNumber                         <- map[SerializationKeys.phoneNumber]
        userRole                            <- map[SerializationKeys.userRole]
        role                                <- map[SerializationKeys.role]
        following                           <- map[SerializationKeys.following]
        dateJoined                          <- map[SerializationKeys.dateJoined]
        lastLogin                           <- map[SerializationKeys.lastLogin]
        isSuperuser                         <- map[SerializationKeys.isSuperuser]
        isStaff                             <- map[SerializationKeys.isStaff]
        settings                            <- map[SerializationKeys.settings]
    }
}
