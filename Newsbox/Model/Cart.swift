//
//  Cart.swift
//  Newsbox
//
//  Created by Trung Tong on 2/8/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import ObjectMapper

class Cart: Mappable {
    private struct SerializationKeys {
        static let count                    = "count"
        static let next                     = "next"
        static let previous                 = "previous"
        static let results                  = "results"
    }
    var count: Int?
    var next: String?
    var previous: String?
    var results = [News]()
    
    // MARK: - Init
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        count                               <- map[SerializationKeys.count]
        next                                <- map[SerializationKeys.next]
        previous                            <- map[SerializationKeys.previous]
        results                             <- map[SerializationKeys.results]
    }
}
