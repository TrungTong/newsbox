//
//  Categories.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import ObjectMapper

class Categorie: Mappable {
    private struct SerializationKeys {
        static let priority              = "priority"
        static let _id                   = "_id"
        static let name                  = "name"
        static let updatedTime           = "updated_time"
    }
    var priority: Int?
    var _id: String?
    var name: String?
    var updatedTime: String?
    
    // MARK: - Init
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        priority                       <- map[SerializationKeys.priority]
        _id                            <- map[SerializationKeys._id]
        name                           <- map[SerializationKeys.name]
        updatedTime                    <- map[SerializationKeys.updatedTime]
    }
}
