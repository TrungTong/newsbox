//
//  LoginViewController.swift
//  Newsbox
//
//  Created by Trung Tong on 1/31/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import OneSignal

class LoginViewController: UIViewController {
    
    // MARK: - Propeties
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    let accountService = AccountService()
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnLogin.backgroundColor = normalBackground
      

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Đăng nhập"
      self.userNameTextField.text = "sonnh"
      self.passwordTextField.text = "123456"
//     //
    //  self.urlTextField.text = "http://167.99.63.13:8080"
self.urlTextField.text = "http://10.1.10.40:8080"
//      let url = UserDefaults.standard.value(forKey: ConstantUD.UDURL) as? String ?? ""
//      if url == "" {
//       // self.urlTextField.text = "http://192.168.92.90:8080"
//      } else {
//          self.urlTextField.text = url
//      }
//      self.userNameTextField.text = UserDefaults.standard.value(forKey: ConstantUD.UDUN) as? String ?? ""
//
    }
    
    // MARK: - private func
    
    private func checkLogin() -> Bool {
        if self.userNameTextField.text?.lenght() == 0 {
            self.showMessageError(message: "Bạn chưa nhập tên đăng nhập!")
            return false
        }
        if self.passwordTextField.text?.lenght() == 0 {
            self.showMessageError(message: "Bạn chưa nhập mật khẩu!")
            return false
        }
        if !Utils.verifyUrl(urlString: self.urlTextField.text) {
            self.showMessageError(message: "Bạn nhập chưa đúng định dạng URL!")
            return false
        }
        // save url
        UserDefaults.standard.set(self.urlTextField.text, forKey: ConstantUD.UDURL)
        UserDefaults.standard.set(self.userNameTextField.text, forKey: ConstantUD.UDUN)
        return true
    }
    
    private func handleRequestLogin(_ trySender: Loadable) {
        if let userNameText = self.userNameTextField.text, let passWordText = self.passwordTextField.text {
            accountService.login(sender: trySender,
                                 user: userNameText, pass: passWordText,
                                 completionHandler: { result in
                                    if result {
                                        self.showMessageSucces(message: "login_success".localized)
                                        Utils.getDelegate().setMainRootViewController()
                                        self.sendTagNotifycation()
                                    } else {
                                        self.showMessageError(message: "login_error".localized)
                                    }
            })
        }
    }
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
  
  private func sendTagNotifycation() {
    let url = UserDefaults.standard.value(forKey: ConstantUD.UDURL) as? String ?? ""
    var u = url.matches(for: "\\w+:\\/\\/")
    u = u.matches(for: ":\\d+")
    let tag = ["user_id": u,
               "ip": url] as [String : Any]
    OneSignal.sendTags(tag)

  }
    // click button Login
    @IBAction func onClickLogin(_ sender: Any) {
        // check Logic login first
        if self.checkLogin() {
            if let trySender = sender as? Loadable {
                handleRequestLogin(trySender)
            }
        }
    }
}

// MARK: - UITextFieldDelegate

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.passwordTextField.resignFirstResponder()
        self.userNameTextField.resignFirstResponder()
        self.urlTextField.resignFirstResponder()
        return true
    }
}
