//
//  ListCartViewController.swift
//  Newsbox
//
//  Created by Trung Tong on 2/7/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import MBProgressHUD

class ListCartViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  let postService = PostService()
  var isShowFull: Bool = true
  var listNews = [News]()
  var cart: Cart?
  private var refreshControl: UIRefreshControl!
  var spinner: UIActivityIndicatorView!
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.tableView.rowHeight = UITableViewAutomaticDimension
    self.tableView.estimatedRowHeight = 200
    self.navigationItem.title = "Lưu trữ"
    self.isShowFull = UserDefaults.standard.bool(forKey: ConstantUD.isCellFull)
    // add Indicator
    setupSpiner()
    self.configureUI()
    configureNavigationBar()
    self.registerNib()
    getData()
    
    NotificationCenter.default.addObserver(self, selector: #selector(ListCartViewController.reloadData), name: .NotifyShowFullOrLess, object: nil)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  private func configureNavigationBar() {
    let buttonItem = UIBarButtonItem.init(image: Utils.defaultMenuImage(), style: .plain, target: self, action: #selector(leftBarTapped))
    self.navigationItem.leftBarButtonItem = buttonItem
    let barItem = UIBarButtonItem.init(barButtonSystemItem: .trash, target: self, action: #selector(deleteAllNews))

    self.navigationItem.rightBarButtonItem = barItem
  }
  
  @objc func deleteAllNews() {
    self.postService.deleteAllCart { result in
      if result {
        self.listNews.removeAll()
        self.tableView.reloadData()
      }
    }
  }
  @objc func reloadData() {
    self.isShowFull = UserDefaults.standard.bool(forKey: ConstantUD.isCellFull)
    self.tableView.reloadData()
  }
  @objc private func leftBarTapped() {
    let storyboard = UIStoryboard(name:"Main", bundle: nil)
    
    if let menuVC = storyboard.instantiateViewController(withClass: MenuViewController.self) {
      menuVC.hidesBottomBarWhenPushed = true
      self.navigationController?.pushViewController(menuVC, animated: true)
    }
  }
  private func setupSpiner() {
    spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    spinner.color = UIColor.white
    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
    spinner.hidesWhenStopped = true
  }
  private func getData(isShow:Bool = true) {
    //show loading
    if isShow {
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.showAdded(to: window, animated: true)
      }
    }
   
    postService.getListCart(url: nil) { cart, error in
      if let carts = cart {
        self.cart = carts
        self.listNews = carts.results
        self.tableView.reloadData()
        self.refreshControl.endRefreshing()
      } else {
        self.showMessageError(message: "Có lỗi xảy ra. Vui lòng thử lại sau!")
        self.refreshControl.endRefreshing()
      }
    }
  }
  func getDataLoadMore() {
    if let count = self.cart?.count, let next = self.cart?.next, count > self.listNews.count {
      DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
        self.postService.getListCart(url: next) { cart, _ in
          if let carts = cart {
            self.listNews += carts.results
            self.tableView.reloadData()
          }
          if self.spinner.isAnimating {
            self.spinner.stopAnimating()
          }
          self.tableView.tableFooterView?.isHidden = true
        }
      }
    } else {
      if self.spinner.isAnimating {
        self.spinner.stopAnimating()
      }
      self.tableView.tableFooterView?.isHidden = true
    }
  }
  @objc func onPullToRefresh(_ sender: AnyObject) {
   self.getData(isShow: false)
  }
  @objc func reoadData() {
    self.isShowFull = UserDefaults.standard.bool(forKey: ConstantUD.isCellFull)
    self.tableView.reloadData()
  }
  private func configureUI() {
    // add pull refresh
    
    self.automaticallyAdjustsScrollViewInsets = false
    self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.01))
    refreshControl = UIRefreshControl()
    refreshControl.tintColor = UIColor.gray
    refreshControl.addTarget(self, action: #selector(onPullToRefresh(_:)), for: .valueChanged)
    tableView.addSubview(refreshControl)
  }
  private func registerNib() {
    self.tableView.register(UINib.init(nibName: "MainLessTableViewCell",
                                       bundle: nil),
                            forCellReuseIdentifier: "MainLessTableViewCell")
    self.tableView.register(UINib.init(nibName: "MainFullTableViewCell",
                                       bundle: nil),
                            forCellReuseIdentifier: "MainFullTableViewCell")
  }
}
// MARK: UIScrollViewDelegate

extension ListCartViewController: UIScrollViewDelegate {
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    // UITableView only moves in one direction, y axis
    let currentOffset = scrollView.contentOffset.y
    let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
    print(maximumOffset - currentOffset)
    
    // Change 10.0 to adjust the distance from bottom
    if maximumOffset - currentOffset <= 10.0 {
      self.tableView.tableFooterView = spinner
      self.tableView.tableFooterView?.isHidden = false
      spinner.startAnimating()
      self.getDataLoadMore()
    }
  }
}
// MARK: - UITableViewDataSource, UITableViewDelegate

extension ListCartViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if isShowFull {
      return heightCellNewsFull
    } else {
      return 50
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if listNews.count == 0 {
      self.tableView.setEmptyMessage("Không có kết quả nào")
      return 0
    } else {
      self.tableView.restore()
      return self.listNews.count
    }
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if self.isShowFull {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainFullTableViewCell", for: indexPath) as? MainFullTableViewCell else {
        fatalError("can not init cell")
      }
      let news = self.listNews[indexPath.row]
      cell.configureCell(news: news)
      return cell
    } else {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainLessTableViewCell", for: indexPath) as? MainLessTableViewCell else {
        fatalError("can not init cell")
      }
      let news = self.listNews[indexPath.row]
      cell.configureCell(news: news)
      return cell
    }
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: false)
    let storyboard = UIStoryboard(name:"Main", bundle: nil)
    let news = self.listNews[indexPath.row]
    let domain = news.domain ?? ""
    if domain.range(of:"facebook") != nil {
      if let newsDetailFBVC = storyboard.instantiateViewController(withClass: NewsDetailsFacebookViewController.self) {
        newsDetailFBVC.idNews = news.idNews ?? ""
        self.navigationController?.pushViewController(newsDetailFBVC, animated: true)
      }
    } else {
      if let newsDetailVC = storyboard.instantiateViewController(withClass: NewsDetailsViewController.self) {
        
        newsDetailVC.idNews = news.idNews ?? ""
        self.navigationController?.pushViewController(newsDetailVC, animated: true)
      }
    }
  }
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if (editingStyle == UITableViewCellEditingStyle.delete) {
      // handle delete (by removing the data from your array and updating the tableview)
      let uniqueId = self.listNews[indexPath.row].uniqueId ?? 0
      postService.deleteACart(uniqueID: uniqueId.toString(), completionHandler: { result in
        self.listNews.remove(at: indexPath.row)
        self.tableView.deleteRows(at: [indexPath], with: .fade)
      })
      
    }
  }
}
