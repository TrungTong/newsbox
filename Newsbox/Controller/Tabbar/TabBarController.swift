//
//  TabBarController.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    let tabBarIndex = tabBarController.selectedIndex
    if var vcArray = tabBarController.viewControllers, tabBarIndex == 0 {
      NotificationCenter.default.post(name: .NotifyHomeResetAll, object: nil)
      vcArray.remove(at: tabBarController.selectedIndex)
      vcArray.insert(viewController, at: 0)
      tabBarController.viewControllers = viewControllers
    }

  }
}
