//
//  SettingTableViewCell.swift
//  Newsbox
//
//  Created by TrungTong on Feb/10/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
  
  @IBOutlet weak var lblCategory: UILabel!
  @IBOutlet weak var lblDetail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
