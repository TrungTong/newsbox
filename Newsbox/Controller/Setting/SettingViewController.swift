//
//  SettingViewController.swift
//  Newsbox
//
//  Created by TrungTong on Feb/10/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
  
  var arrayCategory = DataManager.shareInstance.listCategory.map { categorie in
    categorie.name ?? ""
  }
  var arrayLanguage = DataManager.shareInstance.listLanguages.map { categorie in
    categorie.name ?? ""
  }
  var arrayCountry = DataManager.shareInstance.listCountry.map { categorie in
    categorie.name ??  ""
  }
  var arrayRegion = DataManager.shareInstance.listRegion.map { categorie in
    categorie.name ?? ""
  }
  var arraySourcetype = DataManager.shareInstance.listSourceType
  var arrayNotify = [String]()
  var arrayIdNotify = [String]()
  var listSubject = [Subject]()
  var listCategory = [category]()
  @IBOutlet weak var tableView: UITableView!
  override func viewDidLoad() {
    self.getListNotify()
    super.viewDidLoad()
    listCategory = [("Danhmuc".localized, .settingCategory),
                    ("Ngonngu".localized, .settingLanguages),
                    ("Quocgia".localized, .settingCountries),
                    ("Loainguon".localized, .settingSourcetype),
                    ("Khuvuc".localized, .settingRegions)]
    // Do any additional setup after loading the view.
    self.title = "Cài đặt"
    self.registerNib()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  private func registerNib() {
    self.tableView.register(UINib(nibName: "MainHeaderView",
                                  bundle: nil),
                            forHeaderFooterViewReuseIdentifier: "MainHeaderView")
    self.tableView.estimatedRowHeight = 200
    self.tableView.rowHeight = UITableViewAutomaticDimension
  }
  
  func getListNotify() {
    let dispatchGroup = DispatchGroup()
    let settingService = SettingService()
    dispatchGroup.enter()
    settingService.getListNotifyList { listNotify in
      if let noties = listNotify {
        DataManager.shareInstance.listIDNoti = noties
        self.arrayIdNotify = noties
       // self.arrayNotify.append("summaries")
      }
      dispatchGroup.leave()
    }
    let subjectService = SubjectService()
    dispatchGroup.enter()
    subjectService.getListSubject { listSubject, error in
      if let subjects = listSubject {
        self.listSubject = subjects
        let subject = Subject()
        subject._id = "summaries"
        subject.name = "Tin tham khảo"
        self.listSubject.insert(subject, at: 0)
        DataManager.shareInstance.listSubject = self.listSubject
      }
      dispatchGroup.leave()
    }
    dispatchGroup.notify(queue: .main) {
      let listNotiSubject = self.listSubject.filter {
        !self.arrayIdNotify.contains($0._id!)
      }
      let arrayName = listNotiSubject.map({ subject in
        subject.name ?? ""
      })
      self.arrayNotify.removeAll()
      self.arrayNotify += arrayName
      self.tableView.reloadData()
    }
  }
  
  func setDataList() {
    arrayCategory = DataManager.shareInstance.listCategory.map { categorie in
      categorie.name ?? ""
    }
    arrayLanguage = DataManager.shareInstance.listLanguages.map { categorie in
      categorie.name ?? ""
    }
    arrayCountry = DataManager.shareInstance.listCountry.map { categorie in
      categorie.name ?? ""
    }
    arrayRegion = DataManager.shareInstance.listRegion.map { categorie in
      categorie.name ?? ""
    }
    arraySourcetype = DataManager.shareInstance.listSourceType
  }
}
extension SettingViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
      return listCategory.count
    default:
      return 1
    }
  }
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 0
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 44
  }
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    if let headerOtherView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MainHeaderView") as? MainHeaderView {
      headerOtherView.lblMore.isHidden = true
      headerOtherView.btnMore.isHidden = true
      if section == 0 {
        headerOtherView.lblTitle.text = "Nội dung"
      }
      if section == 1 {
        headerOtherView.lblTitle.text = "Thông báo"
      }
      if section == 2 {
        headerOtherView.lblTitle.text = "Khác"
      }
      return headerOtherView
    }
    return nil
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell", for: indexPath) as? SettingTableViewCell else {
      fatalError("can not init cell")
    }
    switch indexPath.section {
    case 0:
      if indexPath.row == 0 {
        cell.lblCategory.text = "Danhmuc".localized
        cell.lblDetail.text = arrayCategory.flatMap{ $0 }.joined(separator: ", ")
      }
      if indexPath.row == 1 {
        cell.lblCategory.text = "Ngonngu".localized
        cell.lblDetail.text = arrayLanguage.flatMap{ $0 }.joined(separator: ", ")
      }
      if indexPath.row == 2 {
        cell.lblCategory.text = "Quocgia".localized
        cell.lblDetail.text = arrayCountry.flatMap{ $0 }.joined(separator: ", ")
      }
      if indexPath.row == 3 {
        cell.lblCategory.text = "Loainguon".localized
        cell.lblDetail.text = arraySourcetype.flatMap{ $0.captalizeFirstCharacter() }.joined(separator: ", ")
      }
      if indexPath.row == 4 {
        cell.lblCategory.text = "Khuvuc".localized
        cell.lblDetail.text = arrayRegion.flatMap{ $0 }.joined(separator: ", ")
      }
    case 1:
      cell.lblCategory.text = "categoryNoti".localized
      cell.lblDetail.text = arrayNotify.flatMap{ $0 }.joined(separator: ", ")
    default:
      cell.lblCategory.text = "feedback".localized
    }
    return cell
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    switch indexPath.section {
    case 0:
      let category = listCategory[indexPath.item]
      Dropdown.sharedInstance.showDialog(type: category.1, dissmissTap: {
        
      }, selectedTapped: {
        self.setDataList()
        self.tableView.reloadData()
        NotificationCenter.default.post(name: .NotifySetting, object: nil)
      })
      
    default:
        Dropdown.sharedInstance.showDialog(type: .settingNotify, dissmissTap: {
            
        }, selectedTapped: {
          self.getListNotify()
         // self.tableView.reloadData()
        })
      break
    }
  }
}
