//
//  MenuViewController.swift
//  Newsbox
//
//  Created by Trung Tong on 1/31/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
  
  @IBOutlet weak var colletionView: UICollectionView!
  let subjectService = SubjectService()
  var listSubject = [TopicSubject]()
  var listTopicType = [TopicType]()
  @IBOutlet weak var btnFull: UIButton!
  @IBOutlet weak var btnLess: UIButton!
  
  var isFullSelected: Bool = true {
    
    didSet {
      if isFullSelected {
        btnFull.backgroundColor = normalBackground
        btnFull.setTitleColor(UIColor.white, for: .normal)
        btnLess.backgroundColor = UIColor.white
        btnLess.setTitleColor(normalBackground, for: .normal)
      } else {
        btnLess.backgroundColor = normalBackground
        btnLess.setTitleColor(UIColor.white, for: .normal)
        btnFull.backgroundColor = UIColor.white
        btnFull.setTitleColor(normalBackground, for: .normal)
      }
    }
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    configureUI()
    getData()
    self.isFullSelected = UserDefaults.standard.bool(forKey: ConstantUD.isCellFull)
    // Do any additional setup after loading the view.
  }
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.title = "Danh mục"
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    self.tabBarController?.tabBar.isHidden = false
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  private func configureUI() {
    let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: navigationController, action: nil)
    navigationItem.leftBarButtonItem = backButton
    
    let barButtonItem = UIBarButtonItem(image: UIImage(named: "ic_back"),
                                        style: .plain,
                                        target: self,
                                        action: #selector(backTap))
    self.navigationItem.rightBarButtonItem = barButtonItem
  }
  
  @objc func backTap() {
    self.navigationController?.popViewController(animated: true)
  }
  private func getData() {
    let dispatchGroup = DispatchGroup()
    
    dispatchGroup.enter()
    subjectService.getListTopicType { listTopicType, error in
      if let topicTypes = listTopicType {
        self.listTopicType = topicTypes
      }
      dispatchGroup.leave()
    }
    dispatchGroup.enter()
    subjectService.getListTopic { listTopic, error in
      if let topics = listTopic {
        self.listSubject = topics
      }
      dispatchGroup.leave()
    }
    dispatchGroup.notify(queue: .main) {
      self.colletionView.reloadData()
      print("Both functions complete 👍")
    }
  }
  @IBAction func fullTapped(_ sender: Any) {
    self.isFullSelected = true
    // Post a notification
    UserDefaults.standard.set(self.isFullSelected, forKey: ConstantUD.isCellFull)
    NotificationCenter.default.post(name: .NotifyShowFullOrLess, object: nil)
    self.navigationController?.popViewController(animated: true)
  }
  @IBAction func lessTapped(_ sender: Any) {
    self.isFullSelected = false
    UserDefaults.standard.set(self.isFullSelected, forKey: ConstantUD.isCellFull)
    NotificationCenter.default.post(name: .NotifyShowFullOrLess, object: nil)
    self.navigationController?.popViewController(animated: true)
  }
  
}
// MARK: - UICollectionViewDataSource, UICollectionViewDelegate

extension MenuViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: collectionView.frame.width / 2 - 2, height: (collectionView.frame.width / 2 - 2) * 3 / 4)
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0.0
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0.0
  }
  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    
    switch kind {
    case UICollectionElementKindSectionHeader:
      let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "MenuCollectionReusableView", for: indexPath) as! MenuCollectionReusableView
      let key = listSubject[indexPath.section].0
      for item in self.listTopicType {
        if item.0 == key {
          reusableview.lblCategory.text = item.1
          break
        }
      }
      
      //do other header related calls or settups
      return reusableview
      
    default:  fatalError("Unexpected element kind")
    }
  }
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return listSubject.count
  }
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return listSubject[section].1.count
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCell", for: indexPath) as? MenuCollectionViewCell else {
      fatalError("error chanel cell")
    }
    let subsject = listSubject[indexPath.section].1[indexPath.item]
    let name = subsject.name ?? ""
    cell.lblCategory.text = name.captalizeFirstCharacter()
    cell.lblNumberBadge.text = (subsject.matchedCount ?? 0).toString()
    if let avatarLink = subsject.avatar, avatarLink.lenght() > 0 {
      cell.imgCategory.kf.setImage(with: URL.init(string: avatarLink))
    } else {
      cell.imgCategory.setImageWith("")
    }
    return cell
  }
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if let vcArray = self.tabBarController?.viewControllers, let navi = vcArray.first as? UINavigationController {
      // remove all view pushed
      self.tabBarController?.selectedIndex = 0
      var navigationArray = navi.viewControllers
      for _ in 1..<navigationArray.count {
        navigationArray.removeLast()
      }
      
      if let mainChildVC = storyboard?.instantiateViewController(withClass: MainChildViewController.self) {
        let subsject = listSubject[indexPath.section].1[indexPath.item]
        mainChildVC.search = subsject.searchQuery?.search ?? ""
        if let searchQuerry = subsject.searchQuery {
          self.getListSelected(searchQuerry)
        } else {
          DataManager.shareInstance.listCategorySelected.removeAll()
          DataManager.shareInstance.listSourceSelected.removeAll()
          DataManager.shareInstance.listCountrySelected.removeAll()
          DataManager.shareInstance.listRegionSelected.removeAll()
          DataManager.shareInstance.listLanguageSelected.removeAll()
          DataManager.shareInstance.listSourceTypeSelected.removeAll()
          DataManager.shareInstance.stringFrom = ""
          DataManager.shareInstance.stringEnd = ""
          
        }
        
        mainChildVC.title = subsject.name
        navigationArray.append(mainChildVC)
      }
      navi.viewControllers = navigationArray
    }
  }
  func getListSelected(_ searchQuery: SearchQuery) {
    if let defaultCategory = DataManager.shareInstance.listCategoryDefault {
      DataManager.shareInstance.listCategorySelected = defaultCategory.filter {
        searchQuery.category.contains($0._id!)
      }
      //      if DataManager.shareInstance.listCategorySelected.count == 0 {
      //        DataManager.shareInstance.listCategorySelected = defaultCategory
      //      }
    }
    if let defaultLanguage = DataManager.shareInstance.listLanguageDefault {
      DataManager.shareInstance.listLanguageSelected = defaultLanguage.filter {
        searchQuery.lang.contains($0._id!)
      }
      //      if DataManager.shareInstance.listLanguageSelected.count == 0 {
      //        DataManager.shareInstance.listLanguageSelected = defaultLanguage
      //      }
    }
    if let defaultCountry = DataManager.shareInstance.listCountryDefault {
      DataManager.shareInstance.listCountrySelected = defaultCountry.filter {
        searchQuery.country.contains($0._id!)
      }
      //      if DataManager.shareInstance.listCountrySelected.count == 0 {
      //        DataManager.shareInstance.listCountrySelected = defaultCountry
      //      }
    }
    if let defaultRegion = DataManager.shareInstance.listRegionDefault {
      DataManager.shareInstance.listRegionSelected = defaultRegion.filter {
        searchQuery.region.contains($0._id!)
      }
      //      if DataManager.shareInstance.listCountrySelected.count == 0 {
      //        self.listRegion = defaultRegion
      //      }
    }
    DataManager.shareInstance.listSourceTypeSelected = searchQuery.sourceType
    DataManager.shareInstance.listSourceSelected = DataManager.shareInstance.listSource.filter {
      searchQuery.source.contains($0.idSource!)
    }
    DataManager.shareInstance.stringFrom = searchQuery.from ?? ""
    DataManager.shareInstance.stringEnd = searchQuery.to ?? ""
  }
  
}
