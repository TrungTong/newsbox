//
//  MenuCollectionViewCell.swift
//  Newsbox
//
//  Created by Trung Tong on 2/5/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBadge: UIView!
    @IBOutlet weak var lblNumberBadge: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblCategory: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        viewBadge.layer.cornerRadius = viewBadge.bounds.width / 2
//        viewBadge.clipsToBounds = true
    }
}
