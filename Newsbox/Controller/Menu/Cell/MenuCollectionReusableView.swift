//
//  MenuCollectionReusableView.swift
//  Newsbox
//
//  Created by Trung Tong on 2/6/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class MenuCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var lblCategory: UILabel!
}
