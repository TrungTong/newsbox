//
//  NewsPointFullTableViewCell.swift
//  Newsbox
//
//  Created by Trung Tong on 2/22/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class NewsPointFullTableViewCell: UITableViewCell {
    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      if Utils.isIpad {
        lblTitle.font = UIFont.boldSystemFont(ofSize: 24)
      } else {
        lblTitle.font = UIFont.boldSystemFont(ofSize: 18)
      }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(newsPoint: NewsPoint) {
        self.imgImage.image = UIImage.init(named: "news")
        lblTitle.text = newsPoint.title ?? ""
        let time = newsPoint.updatedTime ?? ""
        lblTime.text = Utils.timeAgoSince(.DATETTT, dateString: time)
        lblName.text = newsPoint.creator ?? ""
    }
}
