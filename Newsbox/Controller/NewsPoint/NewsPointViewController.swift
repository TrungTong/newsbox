//
//  NewsPointViewController.swift
//  Newsbox
//
//  Created by TrungTong on Feb/10/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class NewsPointViewController: UIViewController {
    let limit = 10
    var offset = 0
    var listNewsPoint = [NewsPoint]()
    var isShowFull: Bool = true
    // pull refresh
    private var refreshControl: UIRefreshControl!
    // indicator for loadmore
    var indicator: UIActivityIndicatorView!
    let newsPointService = NewsPointService()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Tin tham khảo"
        self.isShowFull = UserDefaults.standard.bool(forKey: ConstantUD.isCellFull)
        NotificationCenter.default.addObserver(self, selector: #selector(NewsPointViewController.reloadData), name: .NotifyShowFullOrLess, object: nil)
        self.configureUI()
        self.registerNib()
        self.getDataNewsPoint()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private func
    // setupdate UI
    private func configureUI() {
        // add pull refresh
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(onPullToRefresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
      self.automaticallyAdjustsScrollViewInsets = false
      self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.01))

        indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.color = UIColor.white
        indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        indicator.hidesWhenStopped = true
    }
    //
    private func registerNib() {
        
        self.tableView.register(UINib.init(nibName: "MainLessTableViewCell",
                                           bundle: nil),
                                forCellReuseIdentifier: "MainLessTableViewCell")
        self.tableView.register(UINib.init(nibName: "NewsPointFullTableViewCell",
                                           bundle: nil),
                                forCellReuseIdentifier: "NewsPointFullTableViewCell")
    }
    @objc func reloadData() {
        self.isShowFull = UserDefaults.standard.bool(forKey: ConstantUD.isCellFull)
        self.tableView.reloadData()
    }
    // hande pull
    @objc func onPullToRefresh(_ sender: Any) {
        self.getDataNewsPoint()
    }
    func getDataNewsPoint( isShow:Bool = true) {
        self.offset = 0
        newsPointService.getListSummaries(limit: self.limit,
                                          offset: self.offset) { subNewsPoint, error in
                                            // remove pull
                                            self.refreshControl.endRefreshing()
                                            if let subs = subNewsPoint {
                                                self.listNewsPoint = subs.result
                                                self.tableView.reloadData()
                                            } else {
                                                //error
                                                Utils.showMessageError(message: "Có lỗi xảy ra. Vui lòng thử lại sau")
                                            }
        }
    }
    
    func getDataLoadMore() {
        self.offset += self.limit
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            self.newsPointService.getListSummaries(limit: self.limit,
                                                   offset: self.offset) { subNewsPoint, error in
                                                    if let subs = subNewsPoint {
                                                        self.listNewsPoint += subs.result
                                                        self.tableView.reloadData()
                                                    }
                                                    if self.indicator.isAnimating {
                                                        self.indicator.stopAnimating()
                                                    }
                                                    self.tableView.tableFooterView?.isHidden = true
            }
        }
    }
    func getDataNewsPointDetails(idNewsPoint: String) {
        newsPointService.getDetailsSummary(idNewsPoint: idNewsPoint) { newsPoint, error in
            if let point = newsPoint {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                if let newsPointDetailVC = storyboard.instantiateViewController(withClass: NewsPointDetailsViewController.self) {
                    newsPointDetailVC.newsPoint = point
                    self.navigationController?.pushViewController(newsPointDetailVC, animated: true)
                }
            } else {
                //error
                Utils.showMessageError(message: "Có lỗi xảy ra. Vui lòng thử lại sau")
            }
        }
    }
}

// MARK: UIScrollViewDelegate

extension NewsPointViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        print(maximumOffset - currentOffset)
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            self.tableView.tableFooterView = indicator
            self.tableView.tableFooterView?.isHidden = false
            indicator.startAnimating()
            self.getDataLoadMore()
        }
    }
}
extension NewsPointViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isShowFull {
            return heightCellNewsFull
        } else {
            return 50
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listNewsPoint.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.isShowFull {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsPointFullTableViewCell", for: indexPath) as? NewsPointFullTableViewCell else {
                fatalError("can not init cell")
            }
            let newsPoint = self.listNewsPoint[indexPath.row]
            cell.configureCell(newsPoint: newsPoint)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainLessTableViewCell", for: indexPath) as? MainLessTableViewCell else {
                fatalError("can not init cell")
            }
            let newsPoint = self.listNewsPoint[indexPath.row]
            cell.configureCell(newsPoint: newsPoint)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let newsPoint = self.listNewsPoint[indexPath.row]
        if let idNewsPoint = newsPoint.idNewsPoint {
            self.getDataNewsPointDetails(idNewsPoint: idNewsPoint)
        }
    }
}
