//
//  NewsPointDetailsViewController.swift
//  Newsbox
//
//  Created by TrungTong on Feb/21/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import Material

class NewsPointDetailsViewController: UIViewController {
  var idNewsPoint: String = ""
  var newsPoint: NewsPoint?
  @IBOutlet weak var textView: UILabel!
  @IBOutlet weak var lblTitle: UILabel!
//  @IBOutlet weak var viewContent: UIView!
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.configUI()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func configUI() {
    self.title = newsPoint?.title
   // self.lblTitle.text = newsPoint?.title
    let modifiedFont = newsPoint?.description ?? ""

//    let htmlData = NSString(string: modifiedFont).data(using: String.Encoding.unicode.rawValue)
//
//    let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
//
//    let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
    
    self.textView.attributedText = modifiedFont.htmlAttributedString()
    //        var darkBlur:UIBlurEffect = UIBlurEffect()
    //
    //        if #available(iOS 10.0, *) { //iOS 10.0 and above
    //            darkBlur = UIBlurEffect(style: UIBlurEffectStyle.prominent)//prominent,regular,extraLight, light, dark
    //        } else { //iOS 8.0 and above
    //            darkBlur = UIBlurEffect(style: UIBlurEffectStyle.dark) //extraLight, light, dark
    //        }
    //        let blurView = UIVisualEffectView(effect: darkBlur)
    //        blurView.frame = self.viewContent.frame //your view that have any objects
    //        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    //        viewContent.addSubview(blurView)
  }
}
extension String {
  func htmlAttributedString() -> NSAttributedString? {
    guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
    guard let html = try? NSMutableAttributedString(
      data: data,
      options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
      documentAttributes: nil) else { return nil }
    html.beginEditing()
    html.enumerateAttribute(NSAttributedStringKey.font, in: NSMakeRange(0, html.length), options: .init(rawValue: 0)) {
      (value, range, stop) in
      if let font = value as? UIFont {
        let resizedFont = font.withSize(font.pointSize * 1.5)
        html.removeAttribute(NSAttributedStringKey.font, range: range)
        html.addAttribute(NSAttributedStringKey.font,
                          value: resizedFont,
                          range: range)
      }
    }
    html.endEditing()
    return html
  }
}
