//
//  FocusDetailViewController.swift
//  Newsbox
//
//  Created by Trung Tong on 2/21/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Material

class FocusDetailViewController: UIViewController {
  
  // MARK: Properties
  // IBOutlet
  // service
  let issueService = IssueService()
  // callback declare when update or create success
 // @IBOutlet weak var richView: RichEditorView!
  var onSuccesUpdate: (() -> Void)?
  var issue: Issue?
  // check isUpdate or CreateNews
  var isUpdate = true

  var isImportant: Bool = false {
    didSet {
      if isImportant {
        imgImportant.image = UIImage.init(named: "radiobutton_on")
      } else {
        imgImportant.image = UIImage.init(named: "radiobutton")
      }
    }
  }
  var isStar: Bool = false {
    didSet {
      if isStar {
        imgStar.image = UIImage.init(named: "radiobutton_on")
      } else {
        imgStar.image = UIImage.init(named: "radiobutton")
      }
    }
  }
  var isFinish: Bool = false {
    didSet {
      if isFinish {
        self.btnFinish.setTitle("Hoàn thành", for: .normal)
      } else {
        self.btnFinish.setTitle("Chưa hoàn thành", for: .normal)
      }
    }
  }
  var listAssigne = [Int]()
  
  //IBOutlet
  @IBOutlet weak var viewContent: UIView!
  @IBOutlet weak var txtDes: TextView!

  @IBOutlet weak var txtTitle: TextField!
  @IBOutlet weak var txtDateStart: TextField!
  @IBOutlet weak var txtDateEnd: TextField!
  @IBOutlet weak var txtAssignees: TextField!
  @IBOutlet weak var imgImportant: UIImageView!
  @IBOutlet weak var imgStar: UIImageView!
  @IBOutlet weak var btnFinish: UIButton!
 // @IBOutlet weak var imgFinish: UIImageView!
  
  // MARK: - View lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.txtTitle.isClearIconButtonEnabled = true
    self.txtDateStart.isClearIconButtonEnabled = true
    self.txtDateEnd.isClearIconButtonEnabled = true
    self.txtAssignees.isClearIconButtonEnabled = true
    self.configureUI()
    self.configureNavigationBar()
    // keyboard
    
    IQKeyboardManager.sharedManager().enable = true
    txtDes.isScrollEnabled = false

  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    txtDes.isScrollEnabled = true
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  // MARK: - Private Func
  
  // MARK: - setup UI
  private func configureNavigationBar() {
    let buttonItem = UIBarButtonItem.init(image: UIImage.init(named: "ic_close_dialog"), style: .plain, target: self, action: #selector(leftBarTapped))
    self.navigationItem.leftBarButtonItem = buttonItem
    
    let buttonItemRight = UIBarButtonItem.init(image: Icon.check, style: .plain, target: self, action: #selector(rightBarTapped))
    self.navigationItem.rightBarButtonItem = buttonItemRight
    
  }
  
  private func configureUI() {
    self.txtAssignees.isEnabled = false
    if isUpdate {
      self.title = "Cập nhật tiêu điểm"
    } else {
      self.title = "Tạo tiêu điểm"
      self.btnFinish.isHidden = true
    }
    
    if let iss = self.issue {
      txtDes.text = iss.description ?? ""
      self.isImportant = iss.important ?? false
      self.isStar = iss.starred ?? false
      self.isFinish = iss.completed ?? false
      
      self.txtTitle.text = iss.title
      if let timeStart = iss.createdTime {
        self.txtDateStart.text = timeStart.dateFormatFromString(formatInt: .DATEFULL, formatOut: .DDMMYYYY)
      }
      if let timeDue = iss.duedate {
        let time = timeDue.components(separatedBy: "T").first ?? ""
        self.txtDateEnd.text = time.dateFormatFromString(formatInt: .DATE, formatOut: .DDMMYYYY)
      }
      self.listAssigne = iss.assignees
      let arrayUser = DataManager.shareInstance.listUser!.filter {
        self.listAssigne.contains($0.idUser!)
      }
      let arrayUserName = arrayUser.map({ user in
        user.username
      })
      if arrayUserName.count > 0 {
        self.txtAssignees.text = arrayUserName.flatMap{ $0 }.joined(separator: ", ")
      }
    }
  }
  
  // MARK: -func Request Server update and create
  
  private func updateIssue() {
    let dateNow = Date().stringFromDate
    let dueDate = txtDateEnd.text?.dateFormatFromString(formatInt: .DDMMYYYY, formatOut: .DATEFULL) ?? ""
    let createdDate = txtDateStart.text?.dateFormatFromString(formatInt: .DDMMYYYY, formatOut: .DATEFULL) ?? ""
   // var isFinish = false
    let des = self.txtDes.text ?? ""
    if let iss = self.issue {
      issueService.updateIssue(idIssue: iss.idIssue ?? 0,
                               issueTitle: self.txtTitle.text ?? "",
                               duedate: dueDate,
                               createdBy: iss.createdBy ?? 0,
                               updatedBy: DataManager.shareInstance.user?.idUser ?? 0,
                               description: des,
                               updatedTime: dateNow,
                               createdTime: createdDate,
                               assignees: self.listAssigne,
                               completed: isFinish,
                               starred: self.isStar,
                               important: self.isImportant,
                               deleted: false,
                               completionHandler: { result in
                                if result {
                                  if let success = self.onSuccesUpdate {
                                    success()
                                  }
                                  self.navigationController?.dismiss(animated: true, completion: nil)
                                  Utils.showMessageSucces(message: "Cập nhật thành công!")
                                } else {
                                  Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau!")
                                }
      })
    }
  }
  
  func finishIssue() {
     let dateNow = Date().stringFromDate
    if let iss = self.issue {
      issueService.updateIssue(idIssue: iss.idIssue ?? 0,
                               issueTitle: iss.title ?? "",
                               duedate: iss.duedate ?? "",
                               createdBy: iss.createdBy ?? 0,
                               updatedBy: DataManager.shareInstance.user?.idUser ?? 0,
                               description: iss.description ?? "",
                               updatedTime: dateNow,
                               createdTime: iss.createdTime ?? "",
                               assignees: iss.assignees,
                               completed: self.isFinish,
                               starred: iss.starred ?? false,
                               important: iss.important ?? false,
                               deleted: false,
                               completionHandler: { result in
                                if result {
                                  if let success = self.onSuccesUpdate {
                                    success()
                                  }
                                  self.navigationController?.dismiss(animated: true, completion: nil)
                                  Utils.showMessageSucces(message: "Cập nhật thành công!")
                                } else {
                                  Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau!")
                                }
      })
    }
  }
  
  private func createNewIssue() {
    let dueDate = txtDateEnd.text?.dateFormatFromString(formatInt: .DDMMYYYY, formatOut: .DATEFULL) ?? ""
    let createdDate = txtDateStart.text?.dateFormatFromString(formatInt: .DDMMYYYY, formatOut: .DATEFULL) ?? ""
    let des = txtDes.text ?? ""
    issueService.createIssue(issueTitle: self.txtTitle.text ?? "",
                             duedate: dueDate,
                             description: des,
                             startTime: createdDate,
                             assignees: self.listAssigne,
                             completed: false,
                             starred: self.isStar,
                             important: self.isImportant) { result in
                              if result {
                                if let success = self.onSuccesUpdate {
                                  success()
                                }
                                self.navigationController?.dismiss(animated: true, completion: nil)
                                Utils.showMessageSucces(message: "Tạo tiêu điểm thành công!")
                              } else {
                                Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau!")
                              }
    }
  }
  
  // MARK: - func handle
  @objc func datePickerStartValueChanged(_ sender: UIDatePicker) {
    
    // Create date formatter
    let dateFormatter: DateFormatter = DateFormatter()
    // Set date format
    dateFormatter.dateFormat = DateFormatString.DDMMYYYY.rawValue
    // Apply date format
    let selectedDate: String = dateFormatter.string(from: sender.date)
    txtDateStart.text = selectedDate
  }
  @objc func datePickerEndValueChanged(_ sender: UIDatePicker) {
    
    // Create date formatter
    let dateFormatter: DateFormatter = DateFormatter()
    // Set date format
    dateFormatter.dateFormat = DateFormatString.DDMMYYYY.rawValue
    // Apply date format
    let selectedDate: String = dateFormatter.string(from: sender.date)
    txtDateEnd.text = selectedDate
  }
  @objc func leftBarTapped() {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
  @objc func rightBarTapped() {
    if self.txtTitle.text?.lenght() == 0 {
      Utils.showMessageError(message: "Bạn không được để trống tiêu đề!")
      return
    }
    if isUpdate {
      self.updateIssue()
    } else {
      self.createNewIssue()
    }
  }
  // MARK: - func IBAction
  
  @IBAction func tappedAssigneList(_ sender: Any) {
      let listUser = DataManager.shareInstance.listUser ?? [User]()
      ListUserDialog.sharedInstance.showDialogListUser(type: .issue,
                                                       values: listUser,
                                                       listUserSelected: self.listAssigne,
                                                       selectCellHandler: { listSelected in
                                                        self.listAssigne = listSelected
                                                        let arrayUser = DataManager.shareInstance.listUser!.filter {
                                                          self.listAssigne.contains($0.idUser!)
                                                        }
                                                        let arrayUserName = arrayUser.map({ user in
                                                          user.username
                                                        })
                                                        if arrayUserName.count > 0 {
                                                          self.txtAssignees.text = arrayUserName.flatMap{ $0 }.joined(separator: ", ")
                                                        }
      })
  }
  
  @IBAction func tappedImportant(_ sender: Any) {
    isImportant = !isImportant
  }
  @IBAction func tappedStar(_ sender: Any) {
    isStar = !isStar
  }

  @IBAction func tappedUpdateOrCreateIssuse(_ sender: Any) {
    if isFinish {
      self.showAlertView(message: "Chưa hoàn thành tiêu điểm") {
        self.isFinish = false
        self.finishIssue()
      }
    }
    self.showAlertView(message: "Hoàn thành tiêu điểm") {
      self.isFinish = true
      self.finishIssue()
    }
  }
  
  //MARK: -func Date PickerView
  func showDatePickerStartDate(){
    //Formate Date
    let datePicker = UIDatePicker()
    datePicker.datePickerMode = .date
    // add datepicker to textField
    txtDateStart.inputView = datePicker
    datePicker.addTarget(self, action: #selector(datePickerStartValueChanged(_:)), for: .valueChanged)
  }
  
  func showDatePickerEndDate(){
    //Formate Date
    let datePicker = UIDatePicker()
    datePicker.datePickerMode = .date
    // add datepicker to textField
    txtDateEnd.inputView = datePicker
    datePicker.addTarget(self, action: #selector(datePickerEndValueChanged(_:)), for: .valueChanged)
  }
  //MARK: - Helper
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.view.endEditing(true)
    self.viewContent.endEditing(true)
  }
}
//MARK: - UITextFieldDelegate
extension FocusDetailViewController: UITextFieldDelegate {
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if textField == txtDateStart {
      showDatePickerStartDate()
    }
    if textField == txtDateEnd {
      showDatePickerEndDate()
    }
    return true
  }
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}
