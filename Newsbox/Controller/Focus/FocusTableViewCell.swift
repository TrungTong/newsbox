//
//  FocusTableViewCell.swift
//  Newsbox
//
//  Created by TrungTong on Feb/21/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import UIImageView_Letters

class FocusTableViewCell: UITableViewCell {
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCreateBy: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var imgFinish: UIImageView!
    @IBOutlet weak var imgImportant: UIImageView!
    
    @IBOutlet weak var contraintHeightImportant: NSLayoutConstraint!
    @IBOutlet weak var contraintLeadingImportant: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configCellWithIssue(issue: Issue) {
      //  self.lblDescription.text = issue.description?.removeHTMLtag ?? ""
        self.lblTitle.text = issue.title
        if let user = DataManager.shareInstance.listUser?.first(where: {$0.idUser == issue.createdBy}) {
            self.lblCreateBy.text = user.username
        } else {
          self.lblCreateBy.text = ""
      }
//        if let star = issue.starred {
//            if star {
//                self.imgStar.isHidden = false
//            } else {
//                self.imgStar.isHidden = true
//            }
//        } else {
            self.imgStar.isHidden = true
    //    }
        
        if let completed = issue.completed {
            if completed {
                self.imgFinish.isHidden = false
            } else {
                self.imgFinish.isHidden = true
            }
        } else {
            self.imgFinish.isHidden = true
        }
//
//        if let important = issue.important {
//            if important {
//                self.imgImportant.isHidden = false
//                self.contraintHeightImportant.constant = 12
//                self.contraintLeadingImportant.constant = 4
//            } else {
//                self.imgImportant.isHidden = true
//                self.contraintHeightImportant.constant = 0
//                self.contraintLeadingImportant.constant = 0
//            }
//        } else {
            self.imgImportant.isHidden = true
            self.contraintHeightImportant.constant = 0
            self.contraintLeadingImportant.constant = 0
      //  }
  
        imgCategory.setImageWith(self.lblCreateBy.text ?? "", color: nil, circular: false)
      if let time = issue.createdTime {
            self.lblTime.text = time.dateFormatFromString(formatInt: .DATEFULL, formatOut: .DDMMYYYY)
        }
    }
}
