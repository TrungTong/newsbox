//
//  FocusViewController.swift
//  Newsbox
//
//  Created by TrungTong on Feb/19/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import MBProgressHUD

class FocusViewController: UIViewController {
  
  // MARK: Properties
  // IBOutlet
  
  @IBOutlet weak var segment: UISegmentedControl!
  @IBOutlet weak var tableView: UITableView!
  
  //Variables
  let issueService = IssueService()
  // pull refresh
  private var refreshControl: UIRefreshControl!
  // indicator for loadmore
  var indicator: UIActivityIndicatorView!
  var subIssue: SubIssue?
  // list được giao and đã giao
  var assignedIssue = [Issue]()
  var deliveredIssue = [Issue]()
  //check được giao or đã giao
  var isDelivered = true
  
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    self.title = "Tiêu điểm"
    self.isDelivered = true
    configureUI()
    getDataIssue()
 
   _ =  Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.fetchData), userInfo: nil, repeats: true)
 //   "http://api.mymemory.translated.net/get?q=" + query + "&langpair=" + from + "|" + to + "&de=1_email_nao_do"
  }
 
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  @objc private func fetchData() {
    DispatchQueue.global(qos: .background).async {
      self.getDataIssue(isShow: false)
    }
  }
  // MARK: - Private func
  // setupdate UI
  private func configureUI() {
    // add pull refresh
    refreshControl = UIRefreshControl()
    refreshControl.tintColor = UIColor.gray
    refreshControl.addTarget(self, action: #selector(onPullToRefresh(_:)), for: .valueChanged)
    tableView.addSubview(refreshControl)
    
    indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    indicator.color = UIColor.white
    indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
    indicator.hidesWhenStopped = true
  }
  // Get data
  private func getDataIssue(isShow:Bool = true) {
    if isShow {
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.showAdded(to: window, animated: true)
      }
    }
    let dispatchGroup = DispatchGroup()
    // check if list User == nil =>> get list User
    if DataManager.shareInstance.listUser == nil {
      dispatchGroup.enter()
      let shareService = ShareService()
      shareService.getListUser { listUser, _ in
        if let users = listUser {
          DataManager.shareInstance.listUser = users
        }
        dispatchGroup.leave()
      }
    }
    // get info user if user nil
    if DataManager.shareInstance.user == nil {
      dispatchGroup.enter()
      let accountService = AccountService()
      accountService.getUserInfo(completionHandler: { user, _ in
        if let userGet = user {
          DataManager.shareInstance.user = userGet
        }
        dispatchGroup.leave()
      })
    }
    
    // get List Issue
    dispatchGroup.enter()
    
    issueService.getListIssue(url: nil) { listIssue, error in
      // remove pull
      self.refreshControl.endRefreshing()
      if let sIssue = listIssue {
        self.subIssue = sIssue
        // set value for list duoc giao and da giao
        self.assignedIssue = sIssue.results.filter({ issue in
          issue.createdBy == DataManager.shareInstance.user?.idUser
        })
        self.deliveredIssue = sIssue.results.filter {
          $0.assignees.contains(DataManager.shareInstance.user?.idUser ?? 0)
        }
      } else {
        //error
        Utils.showMessageError(message: "Có lỗi xảy ra. Vui lòng thử lại sau")
      }
      dispatchGroup.leave()
    }
    
    // Update UI
    dispatchGroup.notify(queue: .main) {
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      self.tableView.reloadData()
      print("Both functions complete 👍")
    }
  }
  // load more
  func getDataLoadMore() {
    if let count = self.subIssue?.count,let listResult = self.subIssue?.results, let next = self.subIssue?.next, count > listResult.count {
      DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
        self.issueService.getListIssue(url: next, completionHandler: { subIssue, _ in
          if let sIssue = subIssue {
            self.subIssue = sIssue
            // set value for list duoc giao and da giao
            self.assignedIssue += sIssue.results.filter({ issue in
              issue.createdBy == DataManager.shareInstance.user?.idUser
            })
            self.deliveredIssue += sIssue.results.filter {
              $0.assignees.contains(1)
            }
            self.tableView.reloadData()
          }
          if self.indicator.isAnimating {
            self.indicator.stopAnimating()
          }
          self.tableView.tableFooterView?.isHidden = true
        })
      }
    } else {
      if self.indicator.isAnimating {
        self.indicator.stopAnimating()
      }
      self.tableView.tableFooterView?.isHidden = true
    }
  }
  // hande pull
  @objc func onPullToRefresh(_ sender: Any) {
    self.getDataIssue(isShow: false)
  }
  // MARK: - IBAction func
  @IBAction func createNewIssueTapped(_ sender: Any) {
    let storyboard = UIStoryboard(name:"Main", bundle: nil)
    
    if let focusDetailVC = storyboard.instantiateViewController(withClass: FocusDetailViewController.self) {
      let nvc: UINavigationController = UINavigationController(rootViewController: focusDetailVC)
      focusDetailVC.isUpdate = false
      focusDetailVC.onSuccesUpdate = {
        self.getDataIssue()
      }
      self.navigationController?.present(nvc, animated: true, completion: nil)
    }
  }
  @IBAction func segementTap(_ sender: Any) {
    switch segment.selectedSegmentIndex {
    case 0:
      if self.isDelivered == false {
        // được giao
        self.isDelivered = true
        self.tableView.reloadData()
      }
    default:
      if self.isDelivered == true {
        // đã giao
        self.isDelivered = false
        self.tableView.reloadData()
      }
    }
  }
}
// MARK: UIScrollViewDelegate

extension FocusViewController: UIScrollViewDelegate {
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    // UITableView only moves in one direction, y axis
    let currentOffset = scrollView.contentOffset.y
    let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
    print(maximumOffset - currentOffset)
    
    // Change 10.0 to adjust the distance from bottom
    if maximumOffset - currentOffset <= 10.0 {
      self.tableView.tableFooterView = indicator
      self.tableView.tableFooterView?.isHidden = false
      indicator.startAnimating()
      self.getDataLoadMore()
    }
  }
}

// MARK: - UITableViewDelegate, UITableViewDataSource extension
extension FocusViewController: UITableViewDelegate, UITableViewDataSource {
  //UITableViewDataSource
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if isDelivered {
      return self.deliveredIssue.count
    } else {
      return self.assignedIssue.count
    }
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "FocusTableViewCell", for: indexPath) as? FocusTableViewCell else {
      fatalError("error forcuss cell")
    }
    if isDelivered {
      // được giao
      let issue = self.deliveredIssue[indexPath.row]
      cell.configCellWithIssue(issue: issue)
    } else {
      // đã giao
      let issue = self.assignedIssue[indexPath.row]
      cell.configCellWithIssue(issue: issue)
    }
    
    return cell
  }
  //UITableViewDelegate
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let storyboard = UIStoryboard(name:"Main", bundle: nil)
    
    if let focusDetailVC = storyboard.instantiateViewController(withClass: FocusDetailViewController.self) {
      let nvc: UINavigationController = UINavigationController(rootViewController: focusDetailVC)
      if isDelivered {
        // được giao
        let issue = self.deliveredIssue[indexPath.row]
        focusDetailVC.issue = issue
      } else {
        // đã giao
        let issue = self.assignedIssue[indexPath.row]
        focusDetailVC.issue = issue
      }
      // set is update
      focusDetailVC.isUpdate = true
      // when update success, request again list
      focusDetailVC.onSuccesUpdate = {
        self.getDataIssue()
      }
      self.navigationController?.present(nvc, animated: true, completion: nil)
    }
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    if self.isDelivered {
      return false
    } else {
      return true
    }
  }
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    let deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Xóa tin", handler:{action, indexpath in
      // đã giao
      let issue = self.assignedIssue[indexPath.row]
      self.issueService.updateIssue(idIssue: issue.idIssue ?? 0,
                                    issueTitle: "",
                                    duedate: "", createdBy: 0,
                                    updatedBy: 0,
                                    description: "",
                                    updatedTime: "",
                                    createdTime: "", assignees: [], completed: false, starred: false, important: false, deleted: true) { result in
                                      if result {
                                        Utils.showMessageSucces(message: "Xoá thành công!")
                                        self.assignedIssue.remove(at: indexPath.row)
                                        self.tableView.deleteRows(at: [indexPath], with: .fade)
                                      } else {
                                        Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau!")
                                      }
      }
    })
    
    return [deleteRowAction]
  }
}
