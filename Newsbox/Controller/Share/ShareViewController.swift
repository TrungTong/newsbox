//
//  ShareViewController.swift
//  Newsbox
//
//  Created by Trung Tong on 2/8/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController {
  
  @IBOutlet weak var segment: UISegmentedControl!
  @IBOutlet weak var tableView: UITableView!
  var listNews = [News]()
  let shareService = ShareService()
  var isShared = true
  var cart: Cart?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.isShared = true
    // Do any additional setup after loading the view.
    registerNib()
    getDataShare()
    self.automaticallyAdjustsScrollViewInsets = false
    self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.01))
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func segementTap(_ sender: Any) {
    switch segment.selectedSegmentIndex {
    case 0:
      self.isShared = true
      self.getDataShare()
    default:
      self.isShared = false
      self.getDataShare()
    }
  }
  private func getDataShare() {
    // get info user if user nil
    let dispatchGroup = DispatchGroup()
    if DataManager.shareInstance.listUser == nil {
      dispatchGroup.enter()
      let shareService = ShareService()
      shareService.getListUser { listUser, _ in
        if let users = listUser {
          DataManager.shareInstance.listUser = users
        }
        dispatchGroup.leave()
      }
    }
    dispatchGroup.enter()
    shareService.getListSharedOrShared(isShared: isShared) { cart, error in
      dispatchGroup.leave()
      if let carts = cart {
        self.cart = carts
        self.listNews = carts.results
        
      } else {
        self.showMessageError(message: "Có lỗi xảy ra. Vui lòng thử lại sau!")
      }
    }
    dispatchGroup.notify(queue: .main) {
      self.tableView.reloadData()
      print("Both functions complete 👍")
    }
  }
  
  private func registerNib() {
    self.tableView.register(UINib.init(nibName: "MainLessTableViewCell",
                                       bundle: nil),
                            forCellReuseIdentifier: "MainLessTableViewCell")
    self.tableView.register(UINib.init(nibName: "ShareFullTableViewCell",
                                       bundle: nil),
                            forCellReuseIdentifier: "ShareFullTableViewCell")
  }
}
// MARK: - UITableViewDataSource, UITableViewDelegate

extension ShareViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.listNews.count
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShareFullTableViewCell", for: indexPath) as? ShareFullTableViewCell else {
      fatalError("can not init cell")
    }
    let news = self.listNews[indexPath.row]
    cell.configureCell(news: news)
    return cell
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: false)
    let storyboard = UIStoryboard(name:"Main", bundle: nil)
    let news = self.listNews[indexPath.row]
    let domain = news.domain ?? ""
    
    if domain.range(of:"facebook") != nil {
      if let newsDetailFBVC = storyboard.instantiateViewController(withClass: NewsDetailsFacebookViewController.self) {
          newsDetailFBVC.idNews = news.idNews ?? ""
        self.navigationController?.pushViewController(newsDetailFBVC, animated: true)
      }
    } else {
      if let newsDetailVC = storyboard.instantiateViewController(withClass: NewsDetailsViewController.self) {
        
        newsDetailVC.idNews = news.idNews ?? ""
        self.navigationController?.pushViewController(newsDetailVC, animated: true)
      }
    }

  }
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    var title = ""
    if self.isShared {
      title = "Từ chối chia sẻ"
    } else {
      title = "Xóa"
    }
    let deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: title, handler:{action, indexpath in
      let news = self.listNews[indexPath.row]
      self.shareService.ignoreOrDeleteANews(isIgnore: self.isShared, idNewIgnore: news.idNews ?? "", completionHandler: { result in
        if result {
          self.listNews.remove(at: indexPath.row)
          self.tableView.deleteRows(at: [indexPath], with: .fade)
        }
      })
    })
    
    return [deleteRowAction]
  }
  
}
