//
//  ShareFullTableViewCell.swift
//  Newsbox
//
//  Created by TrungTong on May/16/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class ShareFullTableViewCell: MainFullTableViewCell {
  @IBOutlet weak var lblUser: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  override func configureCell(news: News) {
    super.configureCell(news: news)
    lblUser.text = "abc"
    let arrayUser = DataManager.shareInstance.listUser!.filter {
      news.shares.contains($0.idUser!)
    }
    let arrayUserName = arrayUser.map({ user in
      user.username
    })
    if arrayUserName.count > 0 {
      lblUser.text = arrayUserName.flatMap{ $0 }.joined(separator: ", ")
    }
  }
}
