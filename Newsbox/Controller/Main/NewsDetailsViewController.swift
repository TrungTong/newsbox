//
//  NewsDetailsViewController.swift
//  Newsbox
//
//  Created by TrungTong on Feb/6/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class NewsDetailsViewController: UIViewController {
  
  // MARK: Properties
  // IBOutlet
  @IBOutlet weak var heightWebView: NSLayoutConstraint!
  @IBOutlet weak var webView: UIWebView!
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var lblDomain: UILabel!
  @IBOutlet weak var lblTime: UILabel!
  @IBOutlet weak var lblCategory: UILabel!
  @IBOutlet weak var imgDomain: UIImageView!
  @IBOutlet weak var widthImageDomain: NSLayoutConstraint!
  @IBOutlet weak var lblSubcontent: UILabel!
  //Variables
  
  var idNews = ""
  var activity: UIActivityIndicatorView!
  let newsService = NewsService()
  var newsS: News?
  let shareService = ShareService()
  var listNewsSave = [News]()
  var cart: Cart?
  var isSaved = false
  var isyoutube = false
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.configNavigationBar()
    self.configureUI()
    self.getDataAndSetUI()
    // Do any additional setup after loading the view.
    let tap = UITapGestureRecognizer(target: self, action: #selector(NewsDetailsViewController.tapFunction))
    lblDomain.isUserInteractionEnabled = true
    lblDomain.addGestureRecognizer(tap)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  @objc func tapFunction(sender:UITapGestureRecognizer) {
    
    if let urlString = self.newsS?.url, let url = URL.init(string: urlString) {
        if #available(iOS 10.0, *) {
          UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
          UIApplication.shared.openURL(url)
        }
    }
  }
  // MARK: - Private func
  
  private func getDataAndSetUI() {
    let dispatchGroup = DispatchGroup()
    
    dispatchGroup.enter()
    let postService = PostService()
    postService.getListCart(url: nil) { cart, error in
      dispatchGroup.leave()
      if let carts = cart {
        self.cart = carts
        self.listNewsSave = carts.results
      } else {
        self.showMessageError(message: "Có lỗi xảy ra. Vui lòng thử lại sau!")
      }
    }
    dispatchGroup.enter()
    newsService.getNewsDetails(idNews: idNews) { news, error in
      dispatchGroup.leave()
      if let n = news {
          self.updateUI(news: n)
      }
    }
    // Update UI
    dispatchGroup.notify(queue: .main) {
      let results = self.listNewsSave.filter { news in news.idNews == self.newsS?.idNews }
      if results.count > 0 {
        self.isSaved = true
        self.configNavigationBar()
      } else {
        // not found
      }
    }
  }
  func updateDataTitle(news: News) {
    let subContent = news.subcontent?.normalizedHtmlString ?? ""
    lblSubcontent.text = subContent
    if let image = news.logo {
      self.imgDomain.setImageWithURl(string: image)
    } else {
      self.widthImageDomain.constant = 0
    }
    let domain = news.domain ?? ""
    let root = news.root ?? ""
    if domain == "www.facebook.com" || domain == "www.google.com" {
      if let strUrl = news.link, let url = URL.init(string: strUrl) {
        self.webView.loadRequest(URLRequest.init(url: url))
      }
    } else if root == "google" {
      if let strUrl = news.url, let url = URL.init(string: strUrl) {
        self.webView.loadRequest(URLRequest.init(url: url))
      }
    } else if domain.range(of:"youtube") != nil {
      self.isyoutube = true
      let body = news.body ?? ""
      let url = URL.init(string: body)
      let request = URLRequest(url: url!)
      webView.loadRequest(request)
    } else {
      if let body = news.body {
        var strTemplateHTML = "<html><head><font size='5'> <style>img{max-width:100%;height:auto !important;width:auto !important;};</style></head><body style='margin:0; padding:8;text-align: justify;text-justify: inter-word;'></body></html>"
        strTemplateHTML.append(body)
        self.webView.loadHTMLString(strTemplateHTML, baseURL: nil)
      }
    }
  }
  func updateUI(news: News) {
  
    let cat = news.category ?? ""
    lblCategory.text = DataManager.shareInstance.listCategory.first(where: { catego -> Bool in
      return cat == catego._id
    })?.name ?? ""
    self.newsS = news
    if let title = news.title {
      self.title = title.normalizedHtmlString
      self.lblTitle.text = title.normalizedHtmlString
    }
    let domain = news.domain ?? ""
    self.lblDomain.text = domain
    
    if let time = news.collectedTime, let date = time.convertStringToDate(formatInt: .DATEFULL) {
      self.lblTime.text = date.stringFromDateOther
    }
    self.updateDataTitle(news: news)
  }
  private func configureUI() {
    if self.webView != nil {
      webView.scrollView.isScrollEnabled = false
      
      activity = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
      activity.center = self.webView.center
      self.webView.addSubView(activity)
      activity.startAnimating()
      activity.isHidden = false
    }
  }
  
  private func configNavigationBar()  {
    
    let menuShowImg = UIImage(named: "ic_menu")!
    let saveImg = UIImage(named: "tabbar_save")!
    
    let editButton  = UIBarButtonItem(image: menuShowImg,  style: .plain, target: self, action: #selector(NewsDetailsViewController.didTapShowMenu)
    )
    let searchButton = UIBarButtonItem(image: saveImg,  style: .plain, target: self, action: #selector(NewsDetailsViewController.didTapSaveAction))
    if isSaved {
       searchButton.tintColor = UIColor.blue
    } else {
       searchButton.tintColor = UIColor.white
    }
   
    navigationItem.rightBarButtonItems = [editButton, searchButton]
  }
  
  private func getListUserShare(priority: Int) {
    
    shareService.getListUser { listUser, error in
      if let users = listUser {
        ListUserDialog.sharedInstance.showDialogListUser(values: users, listUserSelected: nil, selectCellHandler: { listValue in
          self.shareANew(priority: priority, listUser: listValue)
        })
      } else {
        Utils.showMessageError(message: "Có lỗi xảy ra. Vui lòng thử lại sau")
      }
    }
  }
  
  private func shareANew(priority: Int,
                         listUser: [Int]) {
    if let news = self.newsS {
      shareService.shareANews(news: news,
                              priority: priority,
                              listUser: listUser) { result in
                                
      }
    }
  }
  
  // MARK: - Handle rightnavigationbar
  
  @objc func didTapShowMenu() {
    let arrOption = ["Chia sẻ",
                     "Copy đường dẫn"]
    DialogMenu.sharedInstance.showDialogMenu(arrOption) { index in
      DialogMenu.sharedInstance.dismissDialog()
      if index == 1 {
        UIPasteboard.general.string = self.newsS?.url ?? ""
        self.showToast("Copy thành công!")
      }
      if index == 0 {
        var prioritys = [String]()
        for priority in EnumPriority.allValues{
          prioritys.append(priority.stringValue)
        }
        ImportantDialog.sharedInstance.showDialogMenu(values: prioritys, selectCellHandler: { value in
          self.getListUserShare(priority: value + 1)
        })
      }
    }
  }
  @objc func didTapSaveAction() {
    if let news = self.newsS {
       let postService = PostService()
      if self.isSaved {
      } else {
        postService.saveACart(news: news, completionHandler: { result in
          if result {
            self.isSaved = true
            self.configNavigationBar()
          }
        })
      }
  
    }
  }
}
// MARK: - UIWebViewDelegate
extension NewsDetailsViewController: UIWebViewDelegate {
  func webViewDidStartLoad(_ webView: UIWebView) {
    activity.isHidden = false
  }
  
  func webViewDidFinishLoad(_ webView: UIWebView) {
    webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.fontFamily =\"-apple-system\"")
    activity.startAnimating()
    activity.isHidden = true
    if !self.isyoutube {
      webView.frame.size.height = 1
      webView.frame.size = webView.sizeThatFits(.zero)
      webView.scrollView.isScrollEnabled=false;
      heightWebView.constant = webView.scrollView.contentSize.height
    }

  }
  func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
    activity.startAnimating()
    activity.isHidden = true
    print("chuan cmnr")
    print(error.localizedDescription)
  //  self.view.layoutIfNeeded()
  }
  func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
    if navigationType == .linkClicked {
      if let url = request.url {
        if #available(iOS 10.0, *) {
          UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
          UIApplication.shared.openURL(url)
        }
      }
    }
    return true
  }
}

