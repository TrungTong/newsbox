//
//  NewsDetailsFacebookViewController.swift
//  Newsbox
//
//  Created by TrungTong on Apr/5/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import Kingfisher

class NewsDetailsFacebookViewController: NewsDetailsViewController {
  
  @IBOutlet weak var lblMessage: UILabel!
  @IBOutlet weak var imgImage: UIImageView!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var reactHeightConstraint: NSLayoutConstraint!
  var listComment = [Comment]()
  var listReact = [(String, Int)]()
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    imgAvatar.layer.cornerRadius = imgAvatar.layer.frame.size.width / 2
    imgAvatar.clipsToBounds = true
    self.tableView.register(UINib.init(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "CommentTableViewCell")
    self.tableView.estimatedRowHeight = 200
    self.tableView.rowHeight = UITableViewAutomaticDimension
    self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    tableView.layer.removeAllAnimations()
    tableHeightConstraint.constant = tableView.contentSize.height
    UIView.animate(withDuration: 0.5) {
      self.view.updateConstraints()
      self.view.layoutIfNeeded()
    }
    
  }
  override func updateDataTitle(news: News) {
    self.listComment = news.comments
    self.tableView.reloadData()
    if let imageURL = news.fromPic?.pictureFB?.dataPic?.urlPic {
      self.imgAvatar.kf.setImage(with: URL(string: imageURL))
     
    } else {
      self.imgAvatar.setImageWith("facebook")
    }
    
    lblMessage.text = news.message?.normalizedHtmlString
  
    if let image = news.image {
      let size = UIScreen.main.bounds.width - 16
      self.imgImage.kf.setImage(with: URL(string: image), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { ( _, _, _, _) in
          self.imgImage.image = self.imageWithImage(sourceImage: self.imgImage.image!, scaledToWidth: size)
          self.view.layoutIfNeeded()
      })
    }
    if let react = news.reactions {
      self.configReaction(reac: react)
    } else {
      self.reactHeightConstraint.constant = 0
    }
  }
  func configReaction(reac: Reaction) {
    if let numberLike = reac.like {
      listReact.append(("like",numberLike))
    }
    if let numberLove = reac.love {
      listReact.append(("love",numberLove))
    }
    if let numberAngry = reac.angry {
      listReact.append(("angry",numberAngry))
    }
    if let numberHaha = reac.haha {
      listReact.append(("haha",numberHaha))
    }
    if let numberSad = reac.sad {
      listReact.append(("sad",numberSad))
    }
    if let numberWow = reac.wow {
      listReact.append(("wow",numberWow))
    }
    self.collectionView.reloadData()
  }
  func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
    let oldWidth = sourceImage.size.width
    let scaleFactor = scaledToWidth / oldWidth
    
    let newHeight = sourceImage.size.height * scaleFactor
    let newWidth = oldWidth * scaleFactor
    
    UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
    sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage!
  }

}

extension NewsDetailsFacebookViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: collectionView.frame.size.height * 2, height: collectionView.frame.size.height)
    
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 1.0
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 1.0
  }
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return listReact.count
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FBCollectionViewCell", for: indexPath) as? FBCollectionViewCell else {
      fatalError("error chanel cell")
    }
    let reac = listReact[indexPath.item]
    cell.img.image = UIImage.init(named: reac.0)
    cell.numberLike.text = reac.1.toString()
    return cell
  }
}
extension NewsDetailsFacebookViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.listComment.count
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as? CommentTableViewCell else {
        fatalError("can not init cell")
      }
    let comment = self.listComment[indexPath.row]
    if let imageURL = comment.fromPic?.pictureFB?.dataPic?.urlPic {
      cell.imgAvatar.kf.setImage(with: URL(string: imageURL))
    }
    cell.lblName.text = comment.fromPic?.name ?? ""
    if let time =  comment.created_time, let date = time.convertStringToDate(formatInt: .DATEFULL) {
      cell.lblTime.text = date.stringFromDateOther
    }
    cell.lblMessage.text = comment.message ?? ""
    return cell
  }
}
