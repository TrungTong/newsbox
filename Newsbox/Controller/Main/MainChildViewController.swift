//
//  MainChildViewController.swift
//  Newsbox
//
//  Created by Trung Tong on 2/23/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import MBProgressHUD

class MainChildViewController: MainViewController {
    // indicator for loadmore
    var listNews = [News]()
    var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isFull = false
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func registerNofity() {
        NotificationCenter.default.addObserver(self, selector: #selector(MainChildViewController.reoadData), name: .NotifyShowFullOrLess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainChildViewController.reoadData), name: .NotifySetting, object: nil)
    }
    override func configureUI() {
        super.configureUI()
        indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.color = UIColor.white
        indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        indicator.hidesWhenStopped = true
    }
    override func setLeftNavigationBar() {
        
    }
  override func getData(isShow: Bool) {

        var listCategory = [String]()
        listCategory = DataManager.shareInstance.listCategorySelected.map { categorie in
            categorie._id ?? ""
        }
        var listLang = [String]()
        listLang = DataManager.shareInstance.listLanguageSelected.map { categorie in
            categorie._id ?? ""
        }
        
        var listCountry = [String]()
        listCountry = DataManager.shareInstance.listCountrySelected.map { categorie in
            categorie._id ?? ""
        }
        var listRegion = [String]()
        listRegion = DataManager.shareInstance.listRegionSelected.map { categorie in
            categorie._id ?? ""
        }
        let listDomain = [String]()
        
        let from = DataManager.shareInstance.stringFrom
        let to = DataManager.shareInstance.stringEnd
        let time = ""
        let listSourceType = DataManager.shareInstance.listSourceTypeSelected
        var listSource = [String]()
        listSource = DataManager.shareInstance.listSourceSelected.map({ source in
            source.idSource ?? ""
        })
        MBProgressHUD.showAdded(to: self.view, animated: true)
        newsService.getListNewsNotCategory(listCategory: listCategory,
                                           listLang: listLang,
                                           listDomain: listDomain,
                                           from: from,
                                           listSource: listSource,
                                           listCountry: listCountry,
                                           listRegion: listRegion,
                                           listSourcetype: listSourceType,
                                           search: search,
                                           to: to,
                                           pageNumber: pageNumber,
                                           time: time) { (listNews, errorCode) in
                                            // remove pull
                                            self.refreshControl.endRefreshing()
                                            MBProgressHUD.hide(for: self.view, animated: true)
                                            if let items = listNews {
                                                self.listNews = items
                                            } else {
                                              self.listNews.removeAll()
                                              self.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau!")
                                            }
                                            self.tableView.reloadData()
        }
    }
    
    func getDataLoadMore() {
        self.pageNumber += 1
        var listCategory = [String]()
        listCategory = DataManager.shareInstance.listCategorySelected.map { categorie in
            categorie._id ?? ""
        }
        var listLang = [String]()
        listLang = DataManager.shareInstance.listLanguageSelected.map { categorie in
            categorie._id ?? ""
        }
        
        var listCountry = [String]()
        listCountry = DataManager.shareInstance.listCountrySelected.map { categorie in
            categorie._id ?? ""
        }
        var listRegion = [String]()
        listRegion = DataManager.shareInstance.listRegionSelected.map { categorie in
            categorie._id ?? ""
        }
        let listDomain = [String]()
        
        let from = DataManager.shareInstance.stringFrom
        let to = DataManager.shareInstance.stringEnd
        let time = ""
        
        var listSource = [String]()
        listSource = DataManager.shareInstance.listSourceSelected.map({ source in
            source.idSource ?? ""
        })
        let search = ""
      let listSourceType = DataManager.shareInstance.listSourceTypeSelected
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            self.newsService.getListNewsNotCategory(listCategory: listCategory,
                                                    listLang: listLang,
                                                    listDomain: listDomain,
                                                    from: from,
                                                    listSource: listSource,
                                                    listCountry: listCountry,
                                                    listRegion: listRegion,
                                                    listSourcetype: listSourceType,
                                                    search: search,
                                                    to: to,
                                                    pageNumber: self.pageNumber,
                                                    time: time) { (listItem, errorCode) in
                                                        if let items = listItem {
                                                            self.listNews += items
                                                            self.tableView.reloadData()
                                                        }
                                                        if self.indicator.isAnimating {
                                                            self.indicator.stopAnimating()
                                                        }
                                                        self.tableView.tableFooterView?.isHidden = true
            }
        }
        
    }
    
    // MARK: UIScrollViewDelegate
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        print(maximumOffset - currentOffset)
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            self.tableView.tableFooterView = indicator
            self.tableView.tableFooterView?.isHidden = false
            indicator.startAnimating()
            self.getDataLoadMore()
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if listNews.count == 0 {
        self.tableView.setEmptyMessage("Không có kết quả nào")
        return 0
      } else {
        self.tableView.restore()
        return self.listNews.count
      }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isShowFull {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainFullTableViewCell", for: indexPath) as? MainFullTableViewCell else {
                fatalError("can not init cell")
            }
            let news = self.listNews[indexPath.row]
            cell.configureCell(news: news)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainLessTableViewCell", for: indexPath) as? MainLessTableViewCell else {
                fatalError("can not init cell")
            }
            let news = self.listNews[indexPath.row]
            cell.configureCell(news: news)
            return cell
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
      let news = self.listNews[indexPath.row]
      let domain = news.domain ?? ""
      if domain.range(of:"facebook") != nil {
        if let newsDetailFBVC = storyboard.instantiateViewController(withClass: NewsDetailsFacebookViewController.self) {
          newsDetailFBVC.idNews = news.idNews ?? ""
          self.navigationController?.pushViewController(newsDetailFBVC, animated: true)
        }
      } else {
        if let newsDetailVC = storyboard.instantiateViewController(withClass: NewsDetailsViewController.self) {
          
          newsDetailVC.idNews = news.idNews ?? ""
          self.navigationController?.pushViewController(newsDetailVC, animated: true)
        }
      }
    }
}
