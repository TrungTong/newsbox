
//
//  MainViewController.swift
//  Newsbox
//
//  Created by Trung Tong on 1/31/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import MBProgressHUD
import OneSignal
import ObjectMapper

class MainViewController: UIViewController, WWCalendarTimeSelectorProtocol {
  
  @IBOutlet weak var collectionListCategory: UICollectionView!
  @IBOutlet weak var tableView: UITableView!
  fileprivate var singleDate: Date = Date()
  fileprivate var multipleDates: [Date] = []
  
  // pull refresh
  var refreshControl: UIRefreshControl!
  var datePicker = UIDatePicker()
  let newsService = NewsService()
  var listItems =  [Item]()
  var selectCategory = -1
  var pageNumber = 1
  var listCategory = [category]()
  var selectedDataArray = [String]()
  var isShowFull: Bool = true
  var search = ""
  var isFull: Bool = true {
    didSet {
      if isFull {
        listCategory = [("", .time),
                        ("Ngonngu".localized, .languages),
                        ("Quocgia".localized, .countries),
                        ("Khuvuc".localized, .regions),
                        ("Loainguon".localized, .sourceType),
                        ("Nguon".localized, .source)]
      } else {
        listCategory = [("", .time),
                        ("Danhmuc".localized, .categories),
                        ("Ngonngu".localized, .languages),
                        ("Quocgia".localized, .countries),
                        ("Khuvuc".localized, .regions),
                        ("Loainguon".localized, .sourceType),
                        ("Nguon".localized, .source)]
      }
      self.collectionListCategory.reloadData()
    }
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    self.isShowFull = UserDefaults.standard.bool(forKey: ConstantUD.isCellFull)
    isFull = true
    configureNavigationBar()
    getData()
    self.configureUI()
    self.registerNib()
    self.registerNofity()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
  }
  @objc func resetAllData() {
     DataManager.shareInstance.listCategorySelected.removeAll()
    DataManager.shareInstance.listSourceSelected.removeAll()
    DataManager.shareInstance.listCountrySelected.removeAll()
    DataManager.shareInstance.listRegionSelected.removeAll()
    DataManager.shareInstance.listLanguageSelected.removeAll()
    DataManager.shareInstance.listSourceTypeSelected.removeAll()
    DataManager.shareInstance.stringFrom = ""
    DataManager.shareInstance.stringEnd = ""
    
    self.getData()
  }
  @objc func reoadData() {
    self.isShowFull = UserDefaults.standard.bool(forKey: ConstantUD.isCellFull)
    self.tableView.reloadData()
  }
  func registerNofity() {
    self.title = "Trang chủ"
    NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.reoadData), name: .NotifyShowFullOrLess, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.reoadData), name: .NotifySetting, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.resetAllData), name: .NotifyHomeResetAll, object: nil)
  }
  
  func configureUI() {
    self.automaticallyAdjustsScrollViewInsets = false
    self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.01))
    
    // add pull refresh
    refreshControl = UIRefreshControl()
    refreshControl.tintColor = UIColor.gray
    refreshControl.addTarget(self, action: #selector(onPullToRefresh(_:)), for: .valueChanged)
    tableView.addSubview(refreshControl)
  }
  
  private func registerNib() {
    
    self.tableView.register(UINib.init(nibName: "MainLessTableViewCell",
                                       bundle: nil),
                            forCellReuseIdentifier: "MainLessTableViewCell")
    self.tableView.register(UINib.init(nibName: "MainFullTableViewCell",
                                       bundle: nil),
                            forCellReuseIdentifier: "MainFullTableViewCell")
    self.tableView.register(UINib(nibName: "MainHeaderView",
                                  bundle: nil),
                            forHeaderFooterViewReuseIdentifier: "MainHeaderView")
    self.tableView.register(UINib(nibName: "MainFooterView",
                                  bundle: nil),
                            forHeaderFooterViewReuseIdentifier: "MainFooterView")
  }
  func getData( isShow:Bool = true) {
    var listCategory = [String]()
    listCategory = DataManager.shareInstance.listCategorySelected.map { categorie in
      categorie._id ?? ""
    }
    var listLang = [String]()
    listLang = DataManager.shareInstance.listLanguageSelected.map { categorie in
      categorie._id ?? ""
    }
    
    var listCountry = [String]()
    listCountry = DataManager.shareInstance.listCountrySelected.map { categorie in
      categorie._id ?? ""
    }
    var listRegion = [String]()
    listRegion = DataManager.shareInstance.listRegionSelected.map { categorie in
      categorie._id ?? ""
    }
    let listDomain = [String]()
    let listSourceType = DataManager.shareInstance.listSourceTypeSelected
    let from = DataManager.shareInstance.stringFrom
    let to = DataManager.shareInstance.stringEnd
    let time = ""
    
    var listSource = [String]()
   
    listSource = DataManager.shareInstance.listSourceSelected.map({ source in
      source.idSource ?? ""
    })
    let search = ""
    if isShow {
      MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    newsService.getListNews(listCategory: listCategory,
                            listLang: listLang,
                            listDomain: listDomain,
                            from: from,
                            listSource: listSource,
                            listCountry: listCountry,
                            listRegion: listRegion,
                            listSourcetype: listSourceType,
                            search: search,
                            to: to,
                            pageNumber: pageNumber,
                            time: time) { (listItem, errorCode) in
                              // remove pull
                              MBProgressHUD.hide(for: self.view, animated: true)
                              self.refreshControl.endRefreshing()
                              if let items = listItem {
                                self.listItems = items
                              } else {
                                self.listItems.removeAll()
                                self.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau!")
                              }
                              self.tableView.reloadData()
    }
  }
  
  func datePickerChanged(datePicker: UIDatePicker){
    print("date = \(datePicker.date)")
  }
  func setLeftNavigationBar() {
    let buttonItem = UIBarButtonItem.init(image: Utils.defaultMenuImage(), style: .plain, target: self, action: #selector(leftBarTapped))
    self.navigationItem.leftBarButtonItem = buttonItem
  }
  
  func configureNavigationBar() {
    self.setLeftNavigationBar()
    let menuShowImg = UIImage(named: "ic_menu")!
    let searchImg = UIImage(named: "search")!
    
    let editButton  = UIBarButtonItem(image: menuShowImg,  style: .plain, target: self, action: #selector(MainViewController.didTapShowMenu)
    )
    let searchButton = UIBarButtonItem(image: searchImg,  style: .plain, target: self, action: #selector(MainViewController.didTapSearchAction))
    navigationItem.rightBarButtonItems = [editButton, searchButton]
  }
  
  // MARK: - Handle rightnavigationbar
  
  @objc func didTapShowMenu() {
    let arrOption = ["Cài đặt",
                     "Đăng xuất"]
    DialogMenu.sharedInstance.showDialogMenu(arrOption) { index in
      DialogMenu.sharedInstance.dismissDialog()
      if index == 0 {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        
        if let settingVC = storyboard.instantiateViewController(withClass: SettingViewController.self) {
          settingVC.hidesBottomBarWhenPushed = true
          self.navigationController?.pushViewController(settingVC, animated: true)
        }
      }
      if index == 1 {
        Utils.getDelegate().setLoginRootViewController()
         UserManager.shareInstance.isLogin = false
        OneSignal.deleteTags(["user_id", "ip"])
      }
    }
  }
  @objc func didTapSearchAction() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    if let resultsController = storyboard.instantiateViewController(withClass: SearchResultViewController.self) {
      resultsController.onDidSelectCellTappedNews = { news in
        let domain = news.domain ?? ""
        if domain.range(of:"facebook") != nil {
          if let newsDetailFBVC = storyboard.instantiateViewController(withClass: NewsDetailsFacebookViewController.self) {
            newsDetailFBVC.idNews = news.idNews ?? ""
            self.navigationController?.pushViewController(newsDetailFBVC, animated: true)
          }
        } else {
          if let newsDetailVC = storyboard.instantiateViewController(withClass: NewsDetailsViewController.self) {
            
            newsDetailVC.idNews = news.idNews ?? ""
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
          }
        }
      }
      let searchController = self.createSearchBar(viewResult: resultsController)
      if search != "" {
        searchController.searchBar.text = search
      }
      searchController.searchResultsUpdater = resultsController
      searchController.searchBar.delegate = self
      UIApplication.shared.keyWindow?.rootViewController?.present(searchController, animated: true, completion: nil)
    }
  }
  // hande pull
  @objc func onPullToRefresh(_ sender: Any) {
    self.pageNumber = 1
    self.getData(isShow: false)
  }
  @objc private func leftBarTapped() {
    let storyboard = UIStoryboard(name:"Main", bundle: nil)
    
    if let menuVC = storyboard.instantiateViewController(withClass: MenuViewController.self) {
      menuVC.hidesBottomBarWhenPushed = true
      self.navigationController?.pushViewController(menuVC, animated: true)
    }
  }
  func showBeginTimeSelect() {
    
    let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
    selector.delegate = self
    selector.optionCurrentDate = singleDate
    //  selector.optionCurrentDates = Set(multipleDates)
    selector.optionCurrentDateRange.setStartDate(singleDate)
    selector.optionCurrentDateRange.setEndDate(singleDate)
    //  selector.optionSelectionType = WWCalendarTimeSelectorSelection.multiple
    selector.optionMultipleSelectionGrouping = .linkedBalls
    selector.isStart = true
    self.tabBarController?.present(selector, animated: true, completion: nil)
  }
  
  func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
    print("Selected \n\(date)\n---")
    if selector.isStart {
      DataManager.shareInstance.stringSub = date.stringFromFormat("yyyy-MM-dd'T'HH:mm:ss")
      self.showEndTimeSelect()
    } else {
      DataManager.shareInstance.stringFrom = DataManager.shareInstance.stringSub
      DataManager.shareInstance.stringEnd = date.stringFromFormat("yyyy-MM-dd'T'HH:mm:ss")
      self.getData()
    }
  }
  func showEndTimeSelect() {
    
    let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
    selector.delegate = self
    selector.optionCurrentDate = singleDate
    //  selector.optionCurrentDates = Set(multipleDates)
    selector.optionCurrentDateRange.setStartDate(singleDate)
    selector.optionCurrentDateRange.setEndDate(singleDate)
    //  selector.optionSelectionType = WWCalendarTimeSelectorSelection.multiple
    selector.optionMultipleSelectionGrouping = .linkedBalls
    selector.isStart = false
    if self.tabBarController?.presentedViewController == nil {
      self.tabBarController?.present(selector, animated: true, completion: nil)
    } else{
      self.tabBarController?.dismiss(animated: false) { () -> Void in
        self.tabBarController?.present(selector, animated: true, completion: nil)
      }
    }
    
  }

}
// MARK: - UICollectionViewDataSource, UICollectionViewDelegate

extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    if indexPath.item == 0 {
      return CGSize(width: collectionView.frame.size.height, height: collectionView.frame.size.height)
    }
    return CGSize(width: 100, height: collectionView.frame.size.height)
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 1.0
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 1.0
  }
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return listCategory.count
  }
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCollectionViewCell", for: indexPath) as? MainCollectionViewCell else {
      fatalError("error chanel cell")
    }
    if indexPath.item == self.selectCategory {
      cell.isSelect = true
    } else {
      cell.isSelect = false
    }
    let category = listCategory[indexPath.item]
    
    cell.configureCell(cate: category)
    cell.onButtonTapped = {
      if category.1 == EnumCategory.time {
        self.showBeginTimeSelect()
      } else {
        self.selectCategory = indexPath.item
        self.collectionListCategory.reloadData()
        Dropdown.sharedInstance.showDialog(type: category.1, dissmissTap: {
          self.selectCategory = -1
          self.collectionListCategory.reloadData()
        }, selectedTapped: {
          self.getData()
          self.selectCategory = -1
          self.collectionListCategory.reloadData()
        })
      }
    }
    return cell
  }
}
// MARK: UIScrollViewDelegate

extension MainViewController: UIScrollViewDelegate {
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
  }
}
// MARK: - UITableViewDataSource, UITableViewDelegate

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if isShowFull {
      return heightCellNewsFull
    } else {
      return 50
    }
  }
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if isFull {
      return 44
    } else {
      return 0
    }
  }
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    if isFull {
      return 44
    } else {
      return 0
    }
  }
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    if !isFull {
      return nil
    }
    if let footerOtherView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MainFooterView") as? MainFooterView {
      let item = self.listItems[section]
      let listCategory = DataManager.shareInstance.listCategory
      if let itemF = listCategory.first(where: { $0._id == item.key}) {
        // item is the first matching array element
        footerOtherView.onButtonMoreTapped = {
          DataManager.shareInstance.listCategorySelected.removeAll()
          DataManager.shareInstance.listCategorySelected.append(itemF)
          let storyboard = UIStoryboard(name:"Main", bundle: nil)
          if let mainChildVC = storyboard.instantiateViewController(withClass: MainChildViewController.self) {
            mainChildVC.title = itemF.name
            self.navigationController?.pushViewController(mainChildVC, animated: true)
          }
        }

        
      }
      return footerOtherView
    }
    return nil

  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if !isFull {
      return nil
    }
    if let headerOtherView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MainHeaderView") as? MainHeaderView {
      let item = self.listItems[section]
      headerOtherView.lblMore.isHidden = true
      headerOtherView.btnMore.isHidden = true
      let listCategory = DataManager.shareInstance.listCategory
      if let itemF = listCategory.first(where: { $0._id == item.key}) {
        // item is the first matching array element
        headerOtherView.lblTitle.text = itemF.name?.uppercased()
      
      }
      return headerOtherView
    }
    return nil
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    if listItems.count == 0 {
      self.tableView.setEmptyMessage("Không có kết quả nào")
      return 0
    } else {
      self.tableView.restore()
      return self.listItems.count
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.listItems[section].posts?.Hit?.Hitchild.count ?? 0
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if self.isShowFull {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainFullTableViewCell", for: indexPath) as? MainFullTableViewCell else {
        fatalError("can not init cell")
      }
      if let hitchild = self.listItems[indexPath.section].posts?.Hit?.Hitchild[indexPath.row] {
        cell.configureCell(hitchild: hitchild)
      }
      return cell
    } else {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainLessTableViewCell", for: indexPath) as? MainLessTableViewCell else {
        fatalError("can not init cell")
      }
      if let hitchild = self.listItems[indexPath.section].posts?.Hit?.Hitchild[indexPath.row] {
        cell.configureCell(hitchild: hitchild)
      }
      return cell
    }
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: false)
    let storyboard = UIStoryboard(name:"Main", bundle: nil)
    
    if let hitchild = self.listItems[indexPath.section].posts?.Hit?.Hitchild[indexPath.row] {
      let domain = hitchild._source?.domain ?? ""
       if domain.range(of:"facebook") != nil {
        if let newsDetailFBVC = storyboard.instantiateViewController(withClass: NewsDetailsFacebookViewController.self) {
          newsDetailFBVC.idNews = hitchild._id ?? ""
          self.navigationController?.pushViewController(newsDetailFBVC, animated: true)
        }
       } else {
        if let newsDetailVC = storyboard.instantiateViewController(withClass: NewsDetailsViewController.self) {
          newsDetailVC.idNews = hitchild._id ?? ""
          self.navigationController?.pushViewController(newsDetailVC, animated: true)
        }
      }

    }
  }
}
extension MainViewController: UISearchBarDelegate {
  // MARK: - UISearchBarDelegate
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
  }
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    
  }
}
