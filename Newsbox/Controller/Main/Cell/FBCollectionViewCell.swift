//
//  FBCollectionViewCell.swift
//  Newsbox
//
//  Created by TrungTong on Apr/5/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class FBCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var img: UIImageView!
  @IBOutlet weak var numberLike: UILabel!
}
