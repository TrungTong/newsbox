//
//  MainHeaderView.swift
//  Newsbox
//
//  Created by TrungTong on Feb/4/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class MainHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    var onButtonMoreTapped: (() -> Void)?
    @IBAction func moreAction(_ sender: Any) {
      //  if let onButtonMoreTapped = self.onButtonMoreTapped {
        //    onButtonMoreTapped()
        //}
    }
    
}
