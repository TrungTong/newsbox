//
//  CommentTableViewCell.swift
//  Newsbox
//
//  Created by TrungTong on Apr/5/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
  @IBOutlet weak var imgAvatar: UIImageView!
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblTime: UILabel!
  @IBOutlet weak var lblMessage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      imgAvatar.layer.cornerRadius = imgAvatar.frame.size.height / 2
      imgAvatar.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
