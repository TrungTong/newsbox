//
//  MainLessTableViewCell.swift
//  Newsbox
//
//  Created by Trung Tong on 2/7/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import Kingfisher

class MainLessTableViewCell: UITableViewCell {
  @IBOutlet weak var imgImage: UIImageView!
  @IBOutlet weak var lblTitle: UILabel!
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  func configureCell(hitchild: HitChild) {
    let title = hitchild._source?.title ?? ""
    lblTitle.text = title.normalizedHtmlString
    let domain = hitchild._source?.domain ?? ""
    if domain.range(of:"facebook") != nil {
      if let imageURL = hitchild._source?.fromPic?.pictureFB?.dataPic?.urlPic {
          self.imgImage.kf.setImage(with: URL(string: imageURL), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: nil)
      } else {
        self.imgImage.setImageWith("facebook")
      }
    } else if domain.range(of:"youtube") != nil {
      if let imageURL = hitchild._source?.logo {
        self.imgImage.setImageWithURl(string: imageURL)
      } else {
        self.imgImage.setImageWith("youtube")
      }
    } else {
      let htmlString = hitchild._source?.body ?? ""
      if let imgSource = Utils.getImgFromHTMLTag(stringHTML: htmlString).first, let imgString = imgSource {
        self.imgImage.kf.setImage(with: URL(string: imgString), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: nil)
      } else if let image = hitchild._source?.logo {
          self.imgImage.kf.setImage(with: URL(string: image), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: nil)
      } else {
        self.imgImage.setImageWith(title)
      }
    }
    
  }
  func configureCell(news: News) {
    let title = news.title ?? ""
    lblTitle.text = title.normalizedHtmlString
    let domain = news.domain ?? ""
    if domain.range(of:"facebook") != nil {
      if let imageURL = news.fromPic?.pictureFB?.dataPic?.urlPic {
        self.imgImage.kf.setImage(with: URL(string: imageURL), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: nil)
      } else {
        self.imgImage.setImageWith("facebook")
      }
    } else if domain.range(of:"youtube") != nil {
      if let imageURL = news.logo {
        self.imgImage.setImageWithURl(string: imageURL)
      } else {
        self.imgImage.setImageWith("youtube")
      }
    } else {
      let htmlString = news.body ?? ""
      if let imgSource = Utils.getImgFromHTMLTag(stringHTML: htmlString).first, let imgString = imgSource {
        self.imgImage.kf.setImage(with: URL(string: imgString), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: nil)
      } else if let image = news.logo {
         self.imgImage.kf.setImage(with: URL(string: image), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: nil)
      } else {
        self.imgImage.setImageWith(title)
      }
    }
  }
  func configureCell(newsPoint: NewsPoint) {
    self.imgImage.image = UIImage.init(named: "news_favicon")
    lblTitle.text = newsPoint.title ?? ""
  }
}
