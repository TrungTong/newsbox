//
//  MainFullTableViewCell.swift
//  Newsbox
//
//  Created by TrungTong on Feb/3/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit
import Kingfisher
import UIImageView_Letters


class MainFullTableViewCell: UITableViewCell {
  @IBOutlet weak var imgImage: UIImageView!
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var lblDomain: UILabel!
  @IBOutlet weak var lblTime: UILabel!
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    if Utils.isIpad {
      lblTitle.font = UIFont.boldSystemFont(ofSize: 24)
    } else {
      lblTitle.font = UIFont.boldSystemFont(ofSize: 18)
    }
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  func configureCell(hitchild: HitChild) {
    let title = hitchild._source?.title ?? ""
    lblTitle.text = title.normalizedHtmlString
    
    let domain = hitchild._source?.domain ?? ""
    lblDomain.text = domain
    let time = hitchild._source?.collectedTime ?? ""
    
    lblTime.text = Utils.timeAgoSince(.DATEFULLSSSS, dateString: time)
    
    if domain.range(of:"facebook") != nil {
      if let imageURL = hitchild._source?.image {
        self.imgImage.setImageWithURl(string: imageURL)
       //  self.imgImage.kf.setImage(with: URL(string: imageURL), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: nil)
      } else {
        self.imgImage.setImageWith("facebook")
      }
    } else if domain.range(of:"youtube") != nil {
      if let imageURL = hitchild._source?.logo {
        self.imgImage.setImageWithURl(string: imageURL)
      } else {
        self.imgImage.setImageWith("youtube")
      }
    } else {
      let htmlString = hitchild._source?.body ?? ""
//      if let imgSource = Utils.getImgFromHTMLTag(stringHTML: htmlString).first, let imgString = imgSource {
//        self.imgImage.setImageWithURl(string: imgString)
//      } else
        if let img = hitchild._source?.image {
        self.imgImage.kf.setImage(with: URL(string: img), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: { (image, error, _, _) in
          if let _ = error {
            var  baseURL = UserDefaults.standard.string(forKey: ConstantUD.UDURL) ?? ""
            baseURL += img
            self.imgImage.kf.setImage(with: URL(string: baseURL), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: nil)
          }
        })
      } else {
        self.imgImage.setImageWith(domain)
      }

//      let htmlString = hitchild._source?.body ?? ""
//      if let imgSource = Utils.getImgFromHTMLTag(stringHTML: htmlString).first, let imgString = imgSource {
//        self.imgImage.setImageWithURl(string: imgString)
//      } else if var baseURL = UserDefaults.standard.value(forKey: ConstantUD.UDURL) as? String, let image = hitchild._source?.image {
//        baseURL = baseURL + image
//        self.imgImage.kf.setImage(with: URL(string: image), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: { (image, error, _, _) in
//          self.imgImage.kf.setImage(with: URL(string: baseURL), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: nil)
//        })
//
//      } else {
//        self.imgImage.setImageWith(domain)
//      }
    }
  }
  func configureCell(news: News) {
    let title = news.title ?? ""
    lblTitle.text = title.normalizedHtmlString
    let domain = news.domain ?? ""
    lblDomain.text = domain
    
    let time = news.collectedTime ?? ""
    
    lblTime.text = Utils.timeAgoSince(.DATEFULL, dateString: time)
    
    if domain.range(of:"facebook") != nil {
      if let imageURL = news.image {
        self.imgImage.setImageWithURl(string: imageURL)
      } else {
        self.imgImage.setImageWith("facebook")
      }
    } else if domain.range(of:"youtube") != nil {
      if let imageURL = news.logo {
        self.imgImage.setImageWithURl(string: imageURL)
      } else {
        self.imgImage.setImageWith("youtube")
      }
    } else {
      let htmlString = news.body ?? ""
//      if let imgSource = Utils.getImgFromHTMLTag(stringHTML: htmlString).first, let imgString = imgSource {
//        self.imgImage.setImageWithURl(string: imgString)
//      } else
        if let img = news.image {
        self.imgImage.kf.setImage(with: URL(string: img), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: { (image, error, _, _) in
          if let _ = error {
            var  baseURL = UserDefaults.standard.string(forKey: ConstantUD.UDURL) ?? ""
            baseURL += img
             self.imgImage.kf.setImage(with: URL(string: baseURL), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil, completionHandler: nil)
          }
        })
      } else {
        self.imgImage.setImageWith(domain)
      }
    }
  }
  func configureCell(newsPoint: NewsPoint) {
    self.imgImage.image = UIImage.init(named: "news_favicon")
    lblTitle.text = newsPoint.title ?? ""
  }
}
