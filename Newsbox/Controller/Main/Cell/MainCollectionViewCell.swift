//
//  MainCollectionViewCell.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var imgTime: UIImageView!
  private var isShown: Bool = false
  var onButtonTapped: (() -> Void)?
  @IBOutlet weak var imgFilter: UIImageView!
  @IBOutlet weak var viewSelect: UIView!
  
  var isSelect: Bool = false {
    didSet {
      if isSelect {
        self.layer.borderWidth = 1
        self.layer.borderColor = normalBackground.cgColor
        self.lblName.textColor = normalBackground
        self.imgFilter.tintColor = normalBackground
      } else {
        self.layer.borderWidth = 0
        self.lblName.textColor = UIColor.black
        self.imgFilter.tintColor = UIColor.black
      }
    }
  }
  @IBAction func tappedButton(_ sender: Any) {
    if let tapped = self.onButtonTapped {
      tapped()
    }
   
  }
  func configureCell(cate: category) {
    imgTime.image = imgTime.image!.withRenderingMode(.alwaysTemplate)
    imgTime.tintColor = UIColor.gray
    if cate.1 == .time {
      imgFilter.isHidden = true
      imgTime.isHidden = false
      lblName.text = ""
      if DataManager.shareInstance.stringFrom.lenght() > 0 && DataManager.shareInstance.stringEnd.lenght() > 0 {
        self.viewSelect.isHidden = false
      } else {
        self.viewSelect.isHidden = true
      }
    } else {
      if cate.1 == .categories {
        if DataManager.shareInstance.listCategorySelected.count > 0 {
          self.viewSelect.isHidden = false
        } else {
          self.viewSelect.isHidden = true
        }
      }
      
      if cate.1 == .countries {
        if DataManager.shareInstance.listCountrySelected.count > 0 {
          self.viewSelect.isHidden = false
        } else {
          self.viewSelect.isHidden = true
        }
      }
      if cate.1 == .regions {
        if DataManager.shareInstance.listRegionSelected.count > 0 {
          self.viewSelect.isHidden = false
        } else {
          self.viewSelect.isHidden = true
        }
      }
      if cate.1 == .languages {
        if DataManager.shareInstance.listLanguageSelected.count > 0 {
          self.viewSelect.isHidden = false
        } else {
          self.viewSelect.isHidden = true
        }
      }
      if cate.1 == .source {
        if DataManager.shareInstance.listSourceSelected.count > 0 {
          self.viewSelect.isHidden = false
        } else {
          self.viewSelect.isHidden = true
        }
      }
      if cate.1 == .sourceType {
        if DataManager.shareInstance.listSourceTypeSelected.count > 0 {
          self.viewSelect.isHidden = false
        } else {
          self.viewSelect.isHidden = true
        }
      }
      imgFilter.isHidden = false
      imgTime.isHidden = true
      lblName.text = cate.0
    }
    
  }
}
