//
//  MainFooterView.swift
//  Newsbox
//
//  Created by TrungTong on Apr/20/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

class MainFooterView: UITableViewHeaderFooterView {
  var onButtonMoreTapped: (() -> Void)?
  @IBAction func moreAction(_ sender: Any) {
    if let onButtonMoreTapped = self.onButtonMoreTapped {
      onButtonMoreTapped()
    }
  }
}
