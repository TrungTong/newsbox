//
//  Storyboard.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Methods
extension UIStoryboard {
    
    /// SwifterSwift: Get main storyboard for application
    //  public static var mainStoryboard: UIStoryboard? {
    //    let bundle = Bundle.main
    //    guard let name = bundle.object(forInfoDictionaryKey: "UIMainStoryboardFile") as? String else { return nil }
    //    return UIStoryboard(name: name, bundle: bundle)
    //  }
    
    /// SwifterSwift: Instantiate a UIViewController using its class name
    ///
    /// - Parameter name: UIViewController type
    /// - Returns: The view controller corresponding to specified class name
    
    func instantiateViewController<T: UIViewController>(withClass name: T.Type) -> T? {
        if let view = instantiateViewController(withIdentifier: String(describing: name)) as? T {
            return view
        }
        return nil
    }
    // アプリ内で作成されているStoryboardの名前をenumで定義します
    enum Storyboard: String {
        case main
        case exchangeRate
        case favorite
        case weather
    }
    
    // 上で定義したenumでinitできるように
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
}
