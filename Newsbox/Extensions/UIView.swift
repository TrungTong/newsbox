//
//  UIView.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit
import UIImageView_Letters
import Kingfisher

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
extension UIImageView {
  func setImageWithText(text: String) {
    return self.setImageWith(text, color: nil, circular: true)
  }
  func setImageWithURl(string: String) {
    self.kf.setImage(with: URL(string: string), placeholder: UIImage.init(named: "placehoder"), options: nil, progressBlock: nil) { image, _, _, _ in
      if let i = image {
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        self.contentMode = .scaleAspectFit // OR .scaleAspectFill
        self.clipsToBounds = true
        let width = i.size.width
        let height = i.size.height

        if(width > height)
        {
          self.contentMode = .scaleAspectFit
        }
        else
        {
          self.contentMode = .scaleAspectFill
        }
      
      }
    }
  }
}
