//
//  UIViewController.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit
import SwiftMessages
import Toast_Swift

extension UIViewController {
  func createSearchBar(viewResult: UIViewController) -> UISearchController {
    let searchController = UISearchController(searchResultsController: viewResult)
    // Set any properties (in this case, don't hide the nav bar and don't show the emoji keyboard option)
    searchController.hidesNavigationBarDuringPresentation = false
    searchController.searchBar.keyboardType = UIKeyboardType.asciiCapable
    searchController.searchBar.returnKeyType = .done
//    if let image = UIImage(named: "navigation-bar") {
//      searchController.searchBar.setBackgroundImage(image, for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
//    }
    UISearchBar.appearance().barTintColor = UIColor.white
    UISearchBar.appearance().tintColor = UIColor.white
    UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = UIColor.blue
    searchController.searchBar.placeholder = "Tìm kiếm"
    searchController.searchBar.setValue("Đóng", forKey: "_cancelButtonText")
    return searchController
  }
  
  
    func showToast(_ message: String)  {
        self.view.makeToast(message, duration: 1, position: .center)
    }
    func showMessageSucces(message: String) {
        
        let success = MessageView.viewFromNib(layout: .cardView)
        success.configureTheme(.success)
        success.configureContent(title: "Thông báo", body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: nil, buttonTapHandler: { _ in SwiftMessages.hide() })
        success.button?.isHidden = true
        SwiftMessages.show(view: success)
        
    }
    
    func showMessageError(message: String) {
        
        let error = MessageView.viewFromNib(layout: .cardView)
        error.configureTheme(.error)
        error.configureContent(title: "Thông báo", body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: nil, buttonTapHandler: { _ in SwiftMessages.hide() })
        error.button?.isHidden = true
        SwiftMessages.show(view: error)
        
    }
    func showAlertOnlyOk(message: String, handleOk: @escaping (() -> Void)) {
        let alertController = UIAlertController(title: "Thông báo", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            handleOk()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertView(message: String, handleOk: @escaping (() -> Void)) {
        let alertController = UIAlertController(title: "Thông báo", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { _ in
        }))
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            handleOk()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}
