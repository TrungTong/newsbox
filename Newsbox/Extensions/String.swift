//
//  String.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
extension Date {
//  var yesterday: Date {
//    return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
//  }
//  var tomorrow: Date {
//    return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
//  }
  var noon: Date {
    return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
  }
//  var month: Int {
//    return Calendar.current.component(.month,  from: self)
//  }
//  var isLastDayOfMonth: Bool {
//    return tomorrow.month != month
//  }
//  
  var stringFromDate: String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = DateFormatString.DATEFULL.rawValue
    return dateFormatter.string(from: self)
  }
  var stringFromDateOther: String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = DateFormatString.DATEABCDEF.rawValue
    return dateFormatter.string(from: self)
  }
}

extension Int {
  func toString() -> String {
    let myString = String(self)
    return myString
  }
}
extension Double {
  func toString() -> String {
    let myString = String(self)
    return myString
  }
}
extension String {
  var htmlToAttributedString: NSAttributedString? {
    guard let data = data(using: .utf8) else { return NSAttributedString() }
    do {
      return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
    } catch {
      return NSAttributedString()
    }
  }
  var htmlToString: String {
    return htmlToAttributedString?.string ?? ""
  }
  
  var normalizedHtmlString : String {
    
    do {
      if let encodedData = self.data(using: .utf8){
        guard let attributedString = try?  NSAttributedString(data: encodedData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) else {
          return ""
        }
        return String.init(attributedString.string) ?? ""
      }
    }
    catch {
      assert(false, "Please check string")
      //fatalError("Unhandled error: \(error)")
    }
    return self
  }
  var removeHTMLtag: String {
    
    return self.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
  }
  
  var localized: String {
    return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
  }
  
  func lenght() -> Int {
    return self.count
  }
  
  func isEmail() -> Bool {
    let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: self)
  }
  
  func convertHtml() -> NSAttributedString? {
    guard let data = data(using: .utf8) else { return NSAttributedString() }
    guard let attributedString = try?  NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) else {
      return nil
    }
    return attributedString
  }
  func dateFormatFromString(formatInt: DateFormatString, formatOut: DateFormatString) -> String? {
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = formatInt.rawValue
    if let dateObj = dateFormatter.date(from: self) {
      dateFormatter.dateFormat = formatOut.rawValue
      return dateFormatter.string(from: dateObj)
    }
    return nil
  }
  var dateFormatFromString: Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    if let dateObj = dateFormatter.date(from: self) {
      return dateObj
    }
    return nil
  }
  func convertStringToDate(formatInt: DateFormatString) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = formatInt.rawValue
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    if let dateObj = dateFormatter.date(from: self) {
      return dateObj
    }
    dateFormatter.dateFormat = DateFormatString.DATEFULL.rawValue
    if let dateObj = dateFormatter.date(from: self) {
      return dateObj
    }
    dateFormatter.dateFormat = DateFormatString.DATEFULLSSSS.rawValue
    if let dateObj = dateFormatter.date(from: self) {
      return dateObj
    }
    dateFormatter.dateFormat = DateFormatString.DATEFULLNOTSSS.rawValue
    if let dateObj = dateFormatter.date(from: self) {
      return dateObj
    }
    dateFormatter.dateFormat = DateFormatString.DATEABC.rawValue
    if let dateObj = dateFormatter.date(from: self) {
      return dateObj
    }
    dateFormatter.dateFormat = DateFormatString.DATEFULLALL.rawValue
    if let dateObj = dateFormatter.date(from: self) {
      return dateObj
    }
    return nil
  }
  
  var dateFormatFromStringVietNam: String? {
    
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = "yyyyMMdd"
    if let dateObj = dateFormatter.date(from: self) {
      dateFormatter.dateFormat = "dd/MM/yyyy"
      return dateFormatter.string(from: dateObj)
    }
    return nil
  }
  func captalizeFirstCharacter() -> String {
    var result = self
    if result.lenght() > 0 {
      let substr1 = String(self[startIndex]).uppercased()
      result.replaceSubrange(...startIndex, with: substr1)
    }
    return result
  }
  
  func matches(for regexString: String) -> String {
   
    let regex = try? NSRegularExpression(pattern: regexString)
    let value = regex?.stringByReplacingMatches(in: self, options: .reportProgress, range: NSRange(location: 0,length: self.lenght()), withTemplate: "")
  
    // This will print We are big now lot of sales the money and cards Rober Langdon and Ambra Vidal.
    return value ?? ""
  }
}
