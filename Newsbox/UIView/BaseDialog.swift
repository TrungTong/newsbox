//
//  BaseDialog.swift
//  GTVLiveStream
//
//  Created by Trung Tong on 10/27/17.
//  Copyright © 2017 Nobody. All rights reserved.
//

import Foundation
import UIKit
class BaseDialog: NSObject {
    var baseWindow: BaseWindow?
    
    var dialog: UIView!
    
    func showView() {

        if baseWindow == nil {
            self.baseWindow = BaseWindow()
        }
        if let baseWD = self.baseWindow {
            self.dialog.frame = baseWD.rootVC.view.frame
            baseWD.rootVC.view.addSubview(self.dialog)
            self.dialog.alpha = 0.0
            self.dialog.bringSubview(toFront: baseWD)
            UIView.animate(withDuration: 0.01, delay: 0.0, options: .curveEaseInOut, animations: {
                self.dialog.alpha = 1.0
                baseWD.alpha = 1.0
            }, completion: { _ in
                
            })
            baseWD.show()
        }
    }
    /**
     * @brief dismissDialog
     */
    func dismissDialog() {
  
        let lockQueue = DispatchQueue(label: "self", attributes: [])
        lockQueue.sync {
            self.baseWindow?.dismiss()
            self.baseWindow?.removeFromSuperview()
        }
  
    }
    
    /**
     * @brief ダイアログ表示中フラグ
     * @return 表示中判定結果(YES:表示中 / NO:非表示)
     */
    func isShown() -> Bool {
        if let baseWD = self.baseWindow {
            return baseWD.isShown()
        }
        return false
    }
}
