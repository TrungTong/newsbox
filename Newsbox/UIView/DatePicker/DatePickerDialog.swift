//
//  DatePickerDialog.swift
//  Newsbox
//
//  Created by TrungTong on Mar/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit

class DatePickerDialog: NSObject {
  var baseWindow = BaseWindow()
  var datePickerDialogView = DatePickerDialogView()
  static let sharedInstance = DatePickerDialog()
  /**
   * @brief showViewDialog
   * @pram title String
   * @pram content String
   */
  func showDialogPickerDAte(isStart: Bool, selectCellHandler:@escaping ((String) -> Void)) {
    
    let lockQueue = DispatchQueue(label: "self", attributes: [])
    lockQueue.sync {
      if let dialogView = UINib(nibName: "DatePickerDialogView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? DatePickerDialogView {
        self.datePickerDialogView = dialogView
        self.datePickerDialogView.configUI(isStart: isStart)
        self.datePickerDialogView.selectCellHandler = selectCellHandler
        self.showView()
      }
    }
  }
  func showView() {
    
    self.baseWindow = BaseWindow.init()
    self.datePickerDialogView.frame = self.baseWindow.rootVC.view.frame
    self.baseWindow.rootVC.view.addSubview(self.datePickerDialogView)
    self.datePickerDialogView.alpha = 0.0
    self.datePickerDialogView.bringSubview(toFront: self.baseWindow)
    UIView.animate(withDuration: 0.01, delay: 0.0, options: .curveEaseInOut, animations: {
      self.datePickerDialogView.alpha = 1.0
      self.baseWindow.alpha = 1.0
    }, completion: { _ in
      
    })
    self.baseWindow.show()
    
  }
  /**
   * @brief dismissDialog
   */
  func dismissDialog() {
    
    let lockQueue = DispatchQueue(label: "self", attributes: [])
    lockQueue.sync {
      self.baseWindow.dismiss()
      self.baseWindow.removeFromSuperview()
    }
    
  }
  
  /**
   * @brief ダイアログ表示中フラグ
   * @return 表示中判定結果(YES:表示中 / NO:非表示)
   */
  func isShown() -> Bool {
    return (self.baseWindow.isShown())
  }
}
