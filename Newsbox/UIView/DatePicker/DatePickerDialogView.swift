//
//  DatePickerDialogView.swift
//  Newsbox
//
//  Created by TrungTong on Mar/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit

class DatePickerDialogView: UIView {
  
  @IBOutlet weak var datePicker: UIDatePicker!
  @IBOutlet weak var lblTitle: UILabel!
  var selectCellHandler: ((String) -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  func configUI(isStart: Bool) {
    if isStart {
      self.lblTitle.text = "Chọn thời gian bắt đầu"
    } else {
      self.lblTitle.text = "Chọn thời gian kết thúc"
    }
  }
  
  @IBAction func dismissViewAction(_ sender: Any) {
    DatePickerDialog.sharedInstance.dismissDialog()
  }
  
  @IBAction func cancelAction(_ sender: Any) {
    DatePickerDialog.sharedInstance.dismissDialog()
  }
  @IBAction func buyAction(_ sender: Any) {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    let strDate = formatter.string(from: datePicker.date)
    if let callback = self.selectCellHandler {
      callback(strDate)
    }
  }
}
