//
//  BaseWindow.swift
//  Jpanet
//
//  Created by TrungTong on Sep/27/17.
//  Copyright © 2017 TrungTong. All rights reserved.
//

import UIKit
class BaseWindow: UIWindow {
  var dialogView = UIView()
  var rootVC = UIViewController()
  var isShow = Bool()
  
  /**
   * @brief Initializing Process
   */
  convenience init() {
 
    self.init(frame: UIScreen.main.bounds)
    
    self.backgroundColor = UIColor.clear
    self.makeKey()
    self.isHidden = true
    self.alpha = 0.0
    self.rootVC = UIViewController.init()
    self.rootVC.view.frame = UIScreen.main.bounds
    self.rootVC.view.backgroundColor = UIColor.clear
    self.rootViewController = self.rootVC
  
  }
  /**
   * @brief Display Dialog View
   */
  func show() {
 
    //if (self.dialogView != nil) {
    self.alpha = 1.0
    self.makeKeyAndVisible()
    if !self.isShow {
      self.dialogView.isHidden = false
    }
    //}
    self.isShow = true

  }
  
  /**
   * @brief Dismiss Dialog View
   */
  func dismiss() {

    //if (self.dialogView != nil) {
    self.dialogView.isHidden = true
    for subview in self.dialogView.subviews as [UIView] {
      subview.removeFromSuperview()
    }
    self.dialogView.removeFromSuperview()
    //}
    
    // remove all view in window
    for subview in self.subviews as [UIView] {
      subview.removeFromSuperview()
    }
    self.isHidden = true
    self.alpha = 0.0
    self.isShow = false

  }
  
  /**
   * @brief Status of Dialog View
   */
  func isShown() -> Bool {

    return self.isShow
  }
}
