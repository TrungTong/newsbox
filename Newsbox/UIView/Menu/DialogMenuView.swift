//
//  DialogMenuView.swift
//  Jpanet
//
//  Created by TrungTong on Sep/27/17.
//  Copyright © 2017 TrungTong. All rights reserved.
//

import UIKit

class DialogMenuView: UIView, UITableViewDataSource, UITableViewDelegate {
  var arrArrValue = [String] ()
  @IBOutlet weak var tableView: UITableView!
  let cellReuseIdentifier = "cell"
   var selectCellHandler:((Int)->Void)?
  
  @IBOutlet weak var contraintHeightTableview: NSLayoutConstraint!
  override func awakeFromNib() {
    super.awakeFromNib()
    tableView.delegate = self
    tableView.dataSource = self
    self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    tableView.layer.cornerRadius = 5.0;
    tableView.layer.masksToBounds = true;
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.contraintHeightTableview.constant = tableView.contentSize.height
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return arrArrValue.count
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UITableViewCell
    cell.textLabel?.text = arrArrValue[indexPath.row]
    return cell
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.selectCellHandler!(indexPath.row)
  }
  
  @IBAction func dismissViewAction(_ sender: Any) {
    DialogMenu.sharedInstance.dismissDialog()
  }
  
}
