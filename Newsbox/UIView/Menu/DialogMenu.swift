//
//  DialogMenu.swift
//  Jpanet
//
//  Created by TrungTong on Sep/27/17.
//  Copyright © 2017 TrungTong. All rights reserved.
//

import UIKit

class DialogMenu: NSObject {
  var baseWindow = BaseWindow()
  var dialogMenuView = DialogMenuView()
  static let sharedInstance = DialogMenu()
  /**
   * @brief showViewDialog
   * @pram title String
   * @pram content String
   */
  func showDialogMenu(_ values: [String],selectCellHandler:@escaping ((Int)->Void)) {
 
    let lockQueue = DispatchQueue(label: "self",attributes: [])
    lockQueue.sync {
      self.dialogMenuView = UINib(nibName: "DialogMenuView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DialogMenuView
      self.dialogMenuView.arrArrValue = values
      self.dialogMenuView.selectCellHandler = selectCellHandler
      self.showView()
      
    }
  
  }
  func showView() {
  
    self.baseWindow = BaseWindow.init()
    self.dialogMenuView.frame = self.baseWindow.rootVC.view.frame
    self.baseWindow.rootVC.view.addSubview(self.dialogMenuView)
    self.dialogMenuView.alpha = 0.0
    self.dialogMenuView.bringSubview(toFront: self.baseWindow)
    UIView.animate(withDuration: 0.01, delay : 0.0,options : .curveEaseInOut, animations : {
      self.dialogMenuView.alpha = 1.0
      self.baseWindow.alpha = 1.0
    }, completion: { ( finished) in
      
    })
    self.baseWindow.show()
   
  }
  /**
   * @brief dismissDialog
   */
  func dismissDialog() {
   
    let lockQueue = DispatchQueue(label: "self",attributes: [])
    lockQueue.sync {
      self.baseWindow.dismiss()
      self.baseWindow.removeFromSuperview()
    }

  }
  
  /**
   * @brief ダイアログ表示中フラグ
   * @return 表示中判定結果(YES:表示中 / NO:非表示)
   */
  func isShown() -> Bool{
    return (self.baseWindow.isShown())
  }
}
