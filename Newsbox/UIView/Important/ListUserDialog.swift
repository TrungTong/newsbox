//
//  ListUserDialog.swift
//  Newsbox
//
//  Created by Trung Tong on 2/9/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit

class ListUserDialog: NSObject {
    var baseWindow = BaseWindow()
    var listUserDialogView = ListUserDialogView()
    static let sharedInstance = ListUserDialog()
    /**
     * @brief showViewDialog
     * @pram title String
     * @pram content String
     */
    func showDialogListUser(type: TypeDialog = .user,
                            values: [User],
                            listUserSelected: [Int]?,
                        selectCellHandler:@escaping (([Int]) -> Void)) {
        
        let lockQueue = DispatchQueue(label: "self", attributes: [])
        lockQueue.sync {
            if let dialogView = UINib(nibName: "ListUserDialogView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ListUserDialogView {
                self.listUserDialogView = dialogView
                self.listUserDialogView.typeDialog = type
                self.listUserDialogView.arrListUser = values
                self.listUserDialogView.listUserSelected = listUserSelected ?? [Int]()
                self.listUserDialogView.configUI()
                self.listUserDialogView.selectCellHandler = selectCellHandler
                self.showView()
            }
        }
    }
    func showView() {
        
        self.baseWindow = BaseWindow.init()
        self.listUserDialogView.frame = self.baseWindow.rootVC.view.frame
        self.baseWindow.rootVC.view.addSubview(self.listUserDialogView)
        self.listUserDialogView.alpha = 0.0
        self.listUserDialogView.bringSubview(toFront: self.baseWindow)
        UIView.animate(withDuration: 0.01, delay: 0.0, options: .curveEaseInOut, animations: {
            self.listUserDialogView.alpha = 1.0
            self.baseWindow.alpha = 1.0
        }, completion: { _ in
            
        })
        self.baseWindow.show()
        
    }
    /**
     * @brief dismissDialog
     */
    func dismissDialog() {
        
        let lockQueue = DispatchQueue(label: "self", attributes: [])
        lockQueue.sync {
            self.baseWindow.dismiss()
            self.baseWindow.removeFromSuperview()
        }
        
    }
    
    /**
     * @brief ダイアログ表示中フラグ
     * @return 表示中判定結果(YES:表示中 / NO:非表示)
     */
    func isShown() -> Bool {
        return (self.baseWindow.isShown())
    }
}
