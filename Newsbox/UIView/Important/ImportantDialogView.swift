//
//  ImportantDialogView.swift
//  Newsbox
//
//  Created by Trung Tong on 2/9/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit

class ImportantDialogView: UIView, UITableViewDataSource, UITableViewDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    var arrArrValue = [String] ()
    var selectCellHandler: ((Int) -> Void)?
    var selectedIndex: Int?
    @IBOutlet weak var contraintHeightTableview: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: "ImportantTableViewCell", bundle: nil), forCellReuseIdentifier: "ImportantTableViewCell")
        tableView.layer.masksToBounds = true
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contraintHeightTableview.constant = tableView.contentSize.height
    }
    func configUI() {
        lblTitle.text = "Mức độ quan trọng"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrArrValue.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImportantTableViewCell") as? ImportantTableViewCell else {
            fatalError("can not init cell")
        }
        if indexPath.row == selectedIndex {
            cell.imgCheckBox.image = UIImage.init(named: "radiobutton_on")
        } else {
            cell.imgCheckBox.image = UIImage.init(named: "radiobutton")
        }
        cell.lblName.text = arrArrValue[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.tableView.reloadData()
    }
    
    @IBAction func dismissViewAction(_ sender: Any) {
        ImportantDialog.sharedInstance.dismissDialog()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        ImportantDialog.sharedInstance.dismissDialog()
    }
    @IBAction func buyAction(_ sender: Any) {
        if let selectIndex = selectedIndex, let selectedCellHand = self.selectCellHandler {
            ImportantDialog.sharedInstance.dismissDialog()
            selectedCellHand(selectIndex + 1)
        } else {
            Utils.showMessageError(message: "Bạn chưa chọn!")
        }
    }
}
