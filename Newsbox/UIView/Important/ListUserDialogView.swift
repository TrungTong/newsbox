//
//  ListUserDialogView.swift
//  Newsbox
//
//  Created by Trung Tong on 2/9/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit

class ListUserDialogView: UIView, UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var lblTitle: UILabel!
  var arrListUser = [User] ()
  var selectCellHandler: (([Int]) -> Void)?
  var listUserSelected = [Int]()
  var typeDialog = TypeDialog.user
  @IBOutlet weak var contraintHeightTableview: NSLayoutConstraint!
  var isSelectAll = false
  override func awakeFromNib() {
    super.awakeFromNib()
    tableView.delegate = self
    tableView.dataSource = self
    self.tableView.register(UINib.init(nibName: "ImportantTableViewCell", bundle: nil), forCellReuseIdentifier: "ImportantTableViewCell")
    tableView.layer.masksToBounds = true
    tableView.estimatedRowHeight = 200
    tableView.rowHeight = UITableViewAutomaticDimension
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    if tableView.contentSize.height > UIScreen.main.bounds.height / 2 {
      self.contraintHeightTableview.constant = UIScreen.main.bounds.height / 2
    } else {
         self.contraintHeightTableview.constant = tableView.contentSize.height
    } 
  }
  @IBAction func chooseAll(_ sender: Any) {
    if isSelectAll {
      listUserSelected.removeAll()
  
    } else {
      listUserSelected = self.arrListUser.map({ user in
        user.idUser ?? 0
      })
     
    }
    isSelectAll = !isSelectAll
    self.tableView.reloadData()
  }
  func configUI() {
    if typeDialog == .user {
      lblTitle.text = "Chia sẻ cho"
    }
    if typeDialog == .issue {
      lblTitle.text = "Giao cho"
    }
    self.tableView.allowsSelection = true
    self.tableView.allowsMultipleSelection = true
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return arrListUser.count
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImportantTableViewCell") as? ImportantTableViewCell else {
      fatalError("can not init cell")
    }
    
    let user = self.arrListUser[indexPath.row]
    if let _ = listUserSelected.index(where: {$0 == user.idUser}) {
      cell.imgCheckBox.image = UIImage.init(named: "radiobutton_on")
    } else {
      cell.imgCheckBox.image = UIImage.init(named: "radiobutton")
    }
    
    cell.lblName.text = user.username
    cell.selectionStyle = .none
    return cell
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let user = self.arrListUser[indexPath.row]
    if let index = listUserSelected.index(where: {$0 == user.idUser}) {
      listUserSelected.remove(at: index)
    } else {
      listUserSelected.append(user.idUser ?? 0)
    }

   tableView.reloadRows(at: [indexPath], with: .none)
  }
  
  @IBAction func dismissViewAction(_ sender: Any) {
    ListUserDialog.sharedInstance.dismissDialog()
  }
  
  @IBAction func cancelAction(_ sender: Any) {
    ListUserDialog.sharedInstance.dismissDialog()
  }
  @IBAction func buyAction(_ sender: Any) {
    if let selectedCellHand = self.selectCellHandler {
      ListUserDialog.sharedInstance.dismissDialog()
      selectedCellHand(listUserSelected)
    } else {
      Utils.showMessageError(message: "Bạn chưa chọn!")
    }
  }
}
