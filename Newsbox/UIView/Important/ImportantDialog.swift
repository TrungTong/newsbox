//
//  ImportantDialog.swift
//  Newsbox
//
//  Created by Trung Tong on 2/9/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit

class ImportantDialog: NSObject {
    var baseWindow = BaseWindow()
    var importantDialogView = ImportantDialogView()
    static let sharedInstance = ImportantDialog()
    /**
     * @brief showViewDialog
     * @pram title String
     * @pram content String
     */
    func showDialogMenu(values: [String],
                        selectCellHandler:@escaping ((Int) -> Void)) {
        
        let lockQueue = DispatchQueue(label: "self", attributes: [])
        lockQueue.sync {
            if let dialogView = UINib(nibName: "ImportantDialogView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ImportantDialogView {
                self.importantDialogView = dialogView
                self.importantDialogView.arrArrValue = values
                self.importantDialogView.configUI()
                self.importantDialogView.selectCellHandler = selectCellHandler
                self.showView()
            }
        }
    }
    func showView() {
       
        self.baseWindow = BaseWindow.init()
        self.importantDialogView.frame = self.baseWindow.rootVC.view.frame
        self.baseWindow.rootVC.view.addSubview(self.importantDialogView)
        self.importantDialogView.alpha = 0.0
        self.importantDialogView.bringSubview(toFront: self.baseWindow)
        UIView.animate(withDuration: 0.01, delay: 0.0, options: .curveEaseInOut, animations: {
            self.importantDialogView.alpha = 1.0
            self.baseWindow.alpha = 1.0
        }, completion: { _ in
            
        })
        self.baseWindow.show()
    
    }
    /**
     * @brief dismissDialog
     */
    func dismissDialog() {
     
        let lockQueue = DispatchQueue(label: "self", attributes: [])
        lockQueue.sync {
            self.baseWindow.dismiss()
            self.baseWindow.removeFromSuperview()
        }
 
    }
    
    /**
     * @brief ダイアログ表示中フラグ
     * @return 表示中判定結果(YES:表示中 / NO:非表示)
     */
    func isShown() -> Bool {
        return (self.baseWindow.isShown())
    }
}
