//
//  DropDownView.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit

class DropDownView: UIView {
  
  @IBOutlet weak var contraintHeightTableView: NSLayoutConstraint!
  @IBOutlet weak var contraintWidthTableView: NSLayoutConstraint!
  @IBOutlet weak var tableView: UITableView!
  var onButtonCancelTapped: (() -> Void)?
  var onButtonMoreTapped: (() -> Void)?
  
  var filtered = [Categorie]()
  var listSource = [Source]()
  var filteredSource = [Source]()
  var valueSelectedSource = [Source]()
  var searchActive : Bool = false
  var type: EnumCategory = EnumCategory.categories
  var listCategory = [Categorie]()
  var searchBar: UISearchBar?
  let settingService = SettingService()
  var valueSelected = [Categorie]()
  
  var listSubject = [Subject]()
  var listSubjectSelected = [Subject]()
  var listIDNotify = [String]()
  
  var listSourceType = [String]()
  var listSourceTypeSelected = [String]()
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    if UIDevice.current.userInterfaceIdiom == .phone {
      self.contraintWidthTableView.constant = UIScreen.main.bounds.width - 80
      self.contraintHeightTableView.constant = UIScreen.main.bounds.height - 260
    } else {
      self.contraintWidthTableView.constant = UIScreen.main.bounds.width - 300
      self.contraintHeightTableView.constant = UIScreen.main.bounds.height - 400
    }
  }
  
  func configureData() {
    switch type {
    case .categories:
      self.listCategory = DataManager.shareInstance.listCategory
      self.valueSelected = DataManager.shareInstance.listCategorySelected
    case .countries:
      self.listCategory = DataManager.shareInstance.listCountry
      self.valueSelected = DataManager.shareInstance.listCountrySelected
    case .languages:
      self.listCategory = DataManager.shareInstance.listLanguages
      self.valueSelected = DataManager.shareInstance.listLanguageSelected
    case .regions:
      self.listCategory = DataManager.shareInstance.listRegion
      self.valueSelected = DataManager.shareInstance.listRegionSelected
    case .source:
      self.listSource = DataManager.shareInstance.listSource
      self.valueSelectedSource = DataManager.shareInstance.listSourceSelected
    case .settingCountries:
      self.listCategory = DataManager.shareInstance.listCountryDefault ?? [Categorie]()
      self.valueSelected = DataManager.shareInstance.listCountry
    case .settingCategory:
      self.listCategory = DataManager.shareInstance.listCategoryDefault ?? [Categorie]()
      self.valueSelected = DataManager.shareInstance.listCategory
    case .settingLanguages:
      self.listCategory = DataManager.shareInstance.listLanguageDefault ?? [Categorie]()
      self.valueSelected = DataManager.shareInstance.listLanguages
    case .settingRegions:
      self.listCategory = DataManager.shareInstance.listRegionDefault ?? [Categorie]()
      self.valueSelected = DataManager.shareInstance.listRegion
    case .settingSourcetype:
      self.listSourceType = DataManager.shareInstance.listSourceTypeDefault
      self.listSourceTypeSelected = DataManager.shareInstance.listSourceType
    case .settingNotify:
      self.listSubject = DataManager.shareInstance.listSubject
      self.listIDNotify = DataManager.shareInstance.listIDNoti
      self.listSubjectSelected = self.listSubject.filter {
        !self.listIDNotify.contains($0._id!)
      }
    case .sourceType:
      self.listSourceType = DataManager.shareInstance.listSourceType
      self.listSourceTypeSelected = DataManager.shareInstance.listSourceTypeSelected
    case .time:
      break
    }
    self.tableView.reloadData()
  }
  
  func configureView() {
    if self.type == .source {
      searchBar = UISearchBar()
      searchBar?.delegate = self
      searchBar?.sizeToFit()
      searchBar?.barTintColor = UIColor.white
      searchBar?.placeholder = "Tìm kiếm"
      // add as tableHeaderView
      tableView.tableHeaderView = searchBar
    }
    tableView.allowsSelection = true
    
    self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    
    tableView.delegate = self
    tableView.dataSource = self
  }
  
  func updateSettingListCategory(callback:@escaping (() -> Void)) {
    let listCountry = DataManager.shareInstance.listCountry.map { country in
      country._id ?? ""
    }
    let listLang = DataManager.shareInstance.listLanguages.map { lang in
      lang._id ?? ""
    }
    let listCate = valueSelected.map { cate in
      cate._id ?? ""
    }
    let listRegion = DataManager.shareInstance.listRegion.map { region in
      region._id ?? ""
    }
    let souceType = DataManager.shareInstance.listSourceType
    settingService.editSettingList(listCategory: listCate,
                                   listLang: listLang,
                                   listCountry: listCountry,
                                   listRegion: listRegion,
                                   listSouceType: souceType) { result in
                                    if result {
                                      DataManager.shareInstance.listCategory = self.valueSelected
                                      DataManager.shareInstance.listCategorySelected.removeAll()
                                    }
                                    callback()
    }
  }
  
  func updateSettingListCountry(callback:@escaping (() -> Void)) {
    let listCate = DataManager.shareInstance.listCategory.map { cate in
      cate._id ?? ""
    }
    let listLang = DataManager.shareInstance.listLanguages.map { lang in
      lang._id ?? ""
    }
    let listCountry = valueSelected.map { country in
      country._id ?? ""
    }
    let listRegion = DataManager.shareInstance.listRegion.map { region in
      region._id ?? ""
    }
    let souceType = DataManager.shareInstance.listSourceType
    settingService.editSettingList(listCategory: listCate,
                                   listLang: listLang,
                                   listCountry: listCountry,
                                   listRegion: listRegion,
                                   listSouceType: souceType) { result in
                                    if result {
                                      DataManager.shareInstance.listCountry = self.valueSelected
                                      DataManager.shareInstance.listCountrySelected.removeAll()
                                    }
                                    callback()
    }
  }
  
  func updateSettingListRegion(callback:@escaping (() -> Void)) {
    let listCate = DataManager.shareInstance.listCategory.map { cate in
      cate._id ?? ""
    }
    let listLang = DataManager.shareInstance.listLanguages.map { lang in
      lang._id ?? ""
    }
    let listRegion = valueSelected.map { region in
      region._id ?? ""
    }
    let listCountry = DataManager.shareInstance.listCountry.map { country in
      country._id ?? ""
    }
   let souceType = DataManager.shareInstance.listSourceType
    settingService.editSettingList(listCategory: listCate,
                                   listLang: listLang,
                                   listCountry: listCountry,
                                   listRegion: listRegion,
                                   listSouceType: souceType) { result in
                                    if result {
                                      DataManager.shareInstance.listRegion = self.valueSelected
                                      DataManager.shareInstance.listRegionSelected.removeAll()
                                      callback()
                                    }
    }
  }
  func updateSettingListSourceType(callback:@escaping (() -> Void)) {
    let listCate = DataManager.shareInstance.listCategory.map { cate in
      cate._id ?? ""
    }
    let listLang = DataManager.shareInstance.listLanguages.map { lang in
      lang._id ?? ""
    }
    let listRegion = DataManager.shareInstance.listRegion.map { region in
      region._id ?? ""
    }
    let listCountry = DataManager.shareInstance.listCountry.map { country in
      country._id ?? ""
    }
    let souceType = listSourceTypeSelected
    settingService.editSettingList(listCategory: listCate,
                                   listLang: listLang,
                                   listCountry: listCountry,
                                   listRegion: listRegion,
                                   listSouceType: souceType) { result in
                                    if result {
                                      DataManager.shareInstance.listSourceType = self.listSourceTypeSelected
                                  DataManager.shareInstance.listSourceTypeSelected.removeAll()
                                      callback()
                                    }
    }
  }
  
  func updateSettingListLanguage(callback:@escaping (() -> Void)) {
    let listCate = DataManager.shareInstance.listCategory.map { cate in
      cate._id ?? ""
    }
    let listCountry = DataManager.shareInstance.listCountry.map { country in
      country._id ?? ""
    }
    let listLang = valueSelected.map { lang in
      lang._id ?? ""
    }
    let listRegion = DataManager.shareInstance.listRegion.map { region in
      region._id ?? ""
    }
    let souceType = DataManager.shareInstance.listSourceType
    settingService.editSettingList(listCategory: listCate,
                                   listLang: listLang,
                                   listCountry: listCountry,
                                   listRegion: listRegion,
                                   listSouceType: souceType) { result in
                                    if result {
                                      DataManager.shareInstance.listLanguages = self.valueSelected
                                      DataManager.shareInstance.listLanguageSelected.removeAll()
                                      callback()
                                    }
    }
  }
  
  func updateListSource() {
    let categorieService = CategorieService()
    let listCate = DataManager.shareInstance.listCategorySelected.map { cate in
      cate._id ?? ""
    }
    let listLang = DataManager.shareInstance.listLanguageSelected.map { lang in
      lang._id ?? ""
    }
    let listCountry = DataManager.shareInstance.listCountrySelected.map { country in
      country._id ?? ""
    }
    let listRegion = DataManager.shareInstance.listRegionSelected.map { region in
      region._id ?? ""
    }
    let listSourceType = DataManager.shareInstance.listSourceTypeSelected
    categorieService.searchSource(listCategory: listCate,
                                  listLang: listLang,
                                  listCountry: listCountry,
                                  listRegion: listRegion,
                                  listSourceType: listSourceType) { listSource, _ in
                                    if let sources = listSource {
                                      DataManager.shareInstance.listSource = sources
                                      DataManager.shareInstance.listSourceSelected.removeAll()
                                    }
    }
  }
  
  func requestListNotify(callback:@escaping (() -> Void)) {
  //  self.listIDNotify = DataManager.shareInstance.listIDNoti
   
    let a = self.listSubject.filter { subject in
      !self.listSubjectSelected.contains(subject)
    }

    let b = a.map({ sub in
      sub._id ?? ""
    })
    settingService.postNotifyList(listID: b) { result in
      if result {
        DataManager.shareInstance.listIDNoti = b
        callback()
      }
    }
  }
  @IBAction func onSelectedTapped(_ sender: Any) {
    if let buttonTap = self.onButtonMoreTapped {
      switch type {
      case .categories:
        DataManager.shareInstance.listCategorySelected = valueSelected
        self.updateListSource()
        buttonTap()
      case .languages:
        DataManager.shareInstance.listLanguageSelected = valueSelected
        self.updateListSource()
        buttonTap()
      case .countries:
        DataManager.shareInstance.listCountrySelected = valueSelected
        self.updateListSource()
        buttonTap()
      case .regions:
        DataManager.shareInstance.listRegionSelected = valueSelected
        self.updateListSource()
        buttonTap()
      case .settingLanguages:
        self.updateSettingListLanguage(callback: buttonTap)
      case .settingCountries:
        self.updateSettingListCountry(callback: buttonTap)
      case .settingRegions:
        self.updateSettingListRegion(callback: buttonTap)
      case .settingCategory:
        self.updateSettingListCategory(callback: buttonTap)
      case .settingNotify:
        self.requestListNotify(callback: buttonTap)
      case .sourceType:
        DataManager.shareInstance.listSourceTypeSelected = listSourceTypeSelected
        self.updateListSource()
        buttonTap()
      case .settingSourcetype:
        self.updateSettingListSourceType(callback: buttonTap)
      default:
        DataManager.shareInstance.listSourceSelected = valueSelectedSource
        buttonTap()
      }
      Dropdown.sharedInstance.dismissDialog()
    }
    
  }
  @IBAction func onDismissDialog(_ sender: Any) {
    if let cancelTap = self.onButtonCancelTapped {
      cancelTap()
      Dropdown.sharedInstance.dismissDialog()
    }
  }
  // cancel button
  func setCancelButtonAttributes() {
    guard let firstSubView = searchBar?.subviews.first else { return }
    for view in firstSubView.subviews {
      if let cancelButton = view as? UIButton {
        cancelButton.setTitle("Hủy", for: .normal)
        cancelButton.setTitleColor(normalBackground, for: .normal)
        cancelButton.tintColor = normalBackground
      }
    }
  }
  func searchForText(text: String) {
    if text.isEmpty {
      searchActive = false
    } else {
      searchActive = true
      if type == .source {
        filteredSource = self.listSource.filter({ $0.name!.lowercased().hasPrefix(text.lowercased()) })
      } else {
        filtered = self.listCategory.filter({ $0.name!.lowercased().hasPrefix(text.lowercased()) })
      }
    }
    self.tableView.reloadData()
  }
}

extension DropDownView: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if type == .settingNotify {
      // them ca diem tin
      return self.listSubject.count
    }
    if type == .sourceType || type == .settingSourcetype {
      return self.listSourceType.count
    }
    if searchActive {
      if type == .source {
        return filteredSource.count
      } else {
        return filtered.count
      }
    } else {
      if type == .source {
        return self.listSource.count
      } else {
        return self.listCategory.count
      }
    }
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    if type == .settingNotify {
      let subject = self.listSubject[indexPath.row]
      if let _ = listSubjectSelected.index(where: {$0._id == self.listSubject[indexPath.row]._id}) {
        cell.accessoryType = .checkmark
      } else {
        cell.accessoryType = .none
      }
      cell.textLabel?.text = subject.name
      cell.selectionStyle = .none
      return cell
    }
    
    if type == .sourceType || type == .settingSourcetype {
      let subject = self.listSourceType[indexPath.row]
      if let _ = self.listSourceTypeSelected.index(where: {$0 == self.listSourceType[indexPath.row]}) {
        cell.accessoryType = .checkmark
      } else {
        cell.accessoryType = .none
      }
      cell.textLabel?.text = subject.captalizeFirstCharacter()
      cell.selectionStyle = .none
      return cell
    }
    
    if type == .source {
      var source: Source!
      if searchActive {
        if filteredSource.count > 0 {
          source = filteredSource[indexPath.row]
          if let _ = valueSelectedSource.index(where: {$0.idSource == filteredSource[indexPath.row].idSource}) {
            cell.accessoryType = .checkmark
          } else {
            cell.accessoryType = .none
          }
          cell.textLabel?.text = source.name
        }
      } else {
        source = self.listSource[indexPath.row];
        if let _ = valueSelectedSource.index(where: {$0.idSource == listSource[indexPath.row].idSource}) {
          cell.accessoryType = .checkmark
        } else {
          cell.accessoryType = .none
        }
        cell.textLabel?.text = source.name
      }
      
      
    } else {
      var category: Categorie!
      if searchActive {
        category = filtered[indexPath.row]
        if let _ = valueSelected.index(where: {$0._id == filtered[indexPath.row]._id}) {
          cell.accessoryType = .checkmark
        } else {
          cell.accessoryType = .none
        }
      } else {
        category = self.listCategory[indexPath.row];
        if let _ = valueSelected.index(where: {$0._id == listCategory[indexPath.row]._id}) {
          cell.accessoryType = .checkmark
        } else {
          cell.accessoryType = .none
        }
      }
      cell.textLabel?.text = category.name
    }
    cell.selectionStyle = .none
    return cell
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
    if type == .source {
      if searchActive {
        if let _ = valueSelectedSource.index(where: {$0.idSource == filteredSource[indexPath.row].idSource}) {
        } else {
          valueSelectedSource.append(filteredSource[indexPath.row])
        }
      } else {
        if let _ = valueSelectedSource.index(where: {$0.idSource == listSource[indexPath.row].idSource}) {
        } else {
          valueSelectedSource.append(self.listSource[indexPath.row])
        }
      }
    } else if type == .sourceType || type == .settingSourcetype {
      if let _ = listSourceTypeSelected.index(where: {$0 == listSourceType[indexPath.row]}) {
      } else {
        listSourceTypeSelected.append(self.listSourceType[indexPath.row])
      }
    } else if type == .settingNotify {
      if let _ = listSubjectSelected.index(where: {$0._id == self.listSubject[indexPath.row]._id}) {
      } else {
        listSubjectSelected.append(listSubject[indexPath.row])
      }
    } else {
      if searchActive {
        if let _ = valueSelected.index(where: {$0._id == filtered[indexPath.row]._id}) {
        } else {
          valueSelected.append(filtered[indexPath.row])
        }
      } else {
        if let _ = valueSelected.index(where: {$0._id == listCategory[indexPath.row]._id}) {
        } else {
          valueSelected.append(self.listCategory[indexPath.row])
        }
      }
    }
  }
  
  func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    tableView.cellForRow(at: indexPath)?.accessoryType = .none
    if type == .source {
      if searchActive {
        if let index = valueSelectedSource.index(where: {$0.idSource == filteredSource[indexPath.row].idSource}) {
          valueSelectedSource.remove(at: index)
        }
      } else {
        if let index = valueSelectedSource.index(where: {$0.idSource == listSource[indexPath.row].idSource}) {
          valueSelectedSource.remove(at: index)
        }
      }
    } else   if type == .sourceType || type == .settingSourcetype {
      if let index = listSourceTypeSelected.index(where: {$0 == listSourceType[indexPath.row]}) {
        listSourceTypeSelected.remove(at: index)
      }
    } else if type == .settingNotify {
      if let index = listSubjectSelected.index(where: {$0._id == listSubject[indexPath.row]._id}) {
        listSubjectSelected.remove(at: index)
      }
    } else {
      if searchActive {
        if let index = valueSelected.index(where: {$0._id == filtered[indexPath.row]._id}) {
          valueSelected.remove(at: index)
        }
      } else {
        if let index = valueSelected.index(where: {$0._id == listCategory[indexPath.row]._id}) {
          valueSelected.remove(at: index)
        }
      }
    }
  }
}
// MARK:- UISearchBarDelegate
extension DropDownView : UISearchBarDelegate {
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    searchBar.showsCancelButton = true
    searchActive = true
    setCancelButtonAttributes()
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    searchBar.showsCancelButton = false
    searchActive = false
  }
  
  public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    searchActive = false
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    searchActive = false
    searchBar.text = ""
    searchForText(text: "")
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    searchForText(text: searchText)
  }
}
