//
//  Dropdown.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import UIKit
class Dropdown: BaseDialog {
    static let sharedInstance = Dropdown()
    /**
     * @brief showViewDialog
     * @pram title String
     * @pram content String
     */
    func showDialog(type: EnumCategory,
                    dissmissTap: @escaping (() -> Void),
                    selectedTapped: @escaping (() -> Void)) {
        let lockQueue = DispatchQueue(label: "self", attributes: [])
        lockQueue.sync {
            if let dialog = UINib(nibName: "DropDownView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? DropDownView {
                dialog.type = type
                dialog.onButtonMoreTapped = selectedTapped
                dialog.onButtonCancelTapped = dissmissTap
                dialog.configureData()
                dialog.configureView()
                self.dialog = dialog
                self.showView()
            }
        }
    }
}
