//
//  Utils.swift
//  Newsbox
//
//  Created by Trung Tong on 1/31/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//


import Foundation
import UIKit
import SwiftMessages
import MBProgressHUD
import SwiftSoup

class Utils: NSObject {
  class func retrieveCheckListFromJsonFile() -> Any? {
    // Get the url of Persons.json in document directory
    guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil
    }
    let fileUrl = documentsDirectoryUrl.appendingPathComponent("countries.json")
    print(fileUrl)
    // Read data from .json file and transform data into an array
    do {
      let data = try Data(contentsOf: fileUrl, options: [])
      guard let response = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else { return nil }
      return response
    } catch {
      print(error)
    }
    return nil
  }
  class func hideLoad() {
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.hide(for: window, animated: true)
    }
  }
  class func showLoad() {
    //show loading
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.showAdded(to: window, animated: true)
    }
  }
  
  class func defaultMenuImage() -> UIImage {
    var defaultMenuImage = UIImage()
    
    UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
    
    UIColor.black.setFill()
    UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
    UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
    UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
    
    UIColor.white.setFill()
    UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
    UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
    UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
    
    defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
    
    UIGraphicsEndImageContext()
    
    return defaultMenuImage;
  }
  class func verifyUrl (urlString: String?) -> Bool {
    //Check for nil
    if let urlString = urlString {
      // create NSURL instance
      if let url = URL(string: urlString) {
        // check if your application can open the NSURL instance
        return UIApplication.shared.canOpenURL(url)
      }
    }
    return false
  }
  
  class func showMessageSucces(message: String) {
    
    let success = MessageView.viewFromNib(layout: .cardView)
    success.configureTheme(.success)
    success.configureContent(title: "Thông báo", body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: nil, buttonTapHandler: { _ in SwiftMessages.hide() })
    success.button?.isHidden = true
    SwiftMessages.show(view: success)
  }
  
  class func showMessageError(message: String) {
    
    let error = MessageView.viewFromNib(layout: .cardView)
    error.configureTheme(.error)
    error.configureContent(title: "Thông báo", body: message, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: nil, buttonTapHandler: { _ in SwiftMessages.hide() })
    error.button?.isHidden = true
    SwiftMessages.show(view: error)
    
  }
  
  class func showAlertView(title: String,
                           message: String,
                           titleCancel: String,
                           callbackCancel: @escaping (() -> Void?),
                           titleButtonOK: String,
                           callback: @escaping() -> Void) {
    let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
    
    let okAction = UIAlertAction(title: titleButtonOK, style: .default) { _ in
      callback()
    }
    alertController.addAction(okAction)
    let cancelAction = UIAlertAction(title: titleCancel, style: .cancel) { _ in
      callbackCancel()
    }
    alertController.addAction(cancelAction)
    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
    
    alertWindow.rootViewController = UIViewController()
    alertWindow.windowLevel = UIWindowLevelAlert + 1
    alertWindow.makeKeyAndVisible()
    
    alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
  }
  
  class func showAlertViewOnlyOk(title: String,
                                 message: String,
                                 titleButtonOK: String,
                                 callback: @escaping() -> Void) {
    let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
    
    let okAction = UIAlertAction(title: titleButtonOK, style: .default) { _ in
      callback()
    }
    alertController.addAction(okAction)
    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
    
    alertWindow.rootViewController = UIViewController()
    alertWindow.windowLevel = UIWindowLevelAlert + 1
    alertWindow.makeKeyAndVisible()
    
    alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    
  }
  class func getDelegate() -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
  }
  class func timeAgoSince(_ formatInt: DateFormatString, dateString: String) -> String {
    
    let date = dateString.convertStringToDate(formatInt: formatInt)
    
    
    if date == nil {
      return ""
    }
    let calendar = Calendar.current
    let currentDate = Date()
    let CurrentTimeZone = NSTimeZone(abbreviation: "GMT")
    let SystemTimeZone = NSTimeZone.system as NSTimeZone
    let currentGMTOffset: Int? = CurrentTimeZone?.secondsFromGMT(for: currentDate)
    let SystemGMTOffset: Int = SystemTimeZone.secondsFromGMT(for: currentDate)
    let interval = TimeInterval((SystemGMTOffset - currentGMTOffset!))
    let now = Date(timeInterval: interval, since: currentDate)
    
    
    let currentGMTOffset1: Int? = CurrentTimeZone?.secondsFromGMT(for: date!)
    let SystemGMTOffset1: Int = SystemTimeZone.secondsFromGMT(for: date!)
    let interval1 = TimeInterval((SystemGMTOffset1 - currentGMTOffset1!))
    let now1 = Date(timeInterval: interval1, since: date!)
  //  let now = Date()
   
    let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
    var components = (calendar as NSCalendar).components(unitFlags, from: now1, to: now, options: [])
  components.timeZone = TimeZone.current
//    components.
//    dateFormatter.timeZone = TimeZone.current
//    dateFormatter.dateFormat = formatInt.rawValue
//    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    components.timeZone = TimeZone(abbreviation: "UTC")
    if let year = components.year, year >= 2 {
      return "\(year) năm trước"
    }
    
    if let year = components.year, year >= 1 {
      return "Năm vừa rồi"
    }
    
    if let month = components.month, month >= 2 {
      return "\(month) tháng trước"
    }
    
    if let month = components.month, month >= 1 {
      return "Tháng vừa rồi"
    }
    
    if let week = components.weekOfYear, week >= 2 {
      return "\(week) tuần trước"
    }
    
    if let week = components.weekOfYear, week >= 1 {
      return "Tuần vừa rồi"
    }
    
    if let day = components.day, day >= 2 {
      return "\(day) ngày trước"
    }
    
    if let day = components.day, day >= 1 {
      return "Ngày hôm qua"
    }
    
    if let hour = components.hour, hour >= 2 {
      return "\(hour) giờ trước"
    }
    
    if let hour = components.hour, hour >= 1 {
      return "Một giờ trước"
    }
    
    if let minute = components.minute, minute >= 2 {
      return "\(minute) phút trước"
    }
    
    if let minute = components.minute, minute >= 1 {
      return "Một phút trước"
    }
    
    if let second = components.second, second >= 3 {
      return "\(second) giây trước"
    }
    
    return "Vừa xong"
    
  }
  public class var isIpad: Bool {
    if #available(iOS 8.0, *) {
      return UIScreen.main.traitCollection.userInterfaceIdiom == .pad
    } else {
      return UIDevice.current.userInterfaceIdiom == .pad
    }
  }
  public class var isIphone: Bool {
    if #available(iOS 8.0, *) {
      return UIScreen.main.traitCollection.userInterfaceIdiom == .phone
    } else {
      return UIDevice.current.userInterfaceIdiom == .phone
    }
  }
  public class func getImgFromHTMLTag(stringHTML: String) -> [String?] {
    var valueImg = [String?]()
    do {
      let doc: Document = try! SwiftSoup.parse(stringHTML)
      let srcs: Elements = try doc.select("img[src]")
      let srcsStringArray: [String?] = srcs.array().map { try? $0.attr("src").description }
      if srcsStringArray.count > 0 {
        valueImg += srcsStringArray
      }
      // do something with srcsStringArray
    } catch Exception.Error(_ , let message) {
      print(message)
    } catch {
      print("error")
    }
    return valueImg
  }
}
