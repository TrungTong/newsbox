//
//  UserManager.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
class UserManager {
    
    static let shareInstance = UserManager()
    
    let kUserManagerUserToken: String = "UserManagerUserToken"
    
    let kUserManagerIsLogined: String = "UserManagerIsLogined"
    
    var userToken: String {
        get {
            return UserDefaults.standard.string(forKey: kUserManagerUserToken) ?? ""
        }
        set {
            setUserToken(token: newValue)
        }
    }
    
    func setUserToken(token: String) {
        UserDefaults.standard.set(token, forKey: kUserManagerUserToken)
        UserDefaults.standard.synchronize()
    }
  
    var isLogin: Bool {
        get {
            var isLogin: Bool = false
            if UserDefaults.standard.bool(forKey: kUserManagerIsLogined) {
                isLogin = UserDefaults.standard.bool(forKey: kUserManagerIsLogined)
            }
            return isLogin
        }
        set(login) {
            UserDefaults.standard.set(login, forKey: kUserManagerIsLogined)
            UserDefaults.standard.synchronize()
        }
    }
}
