//
//  DataManager.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
class DataManager {
  static let shareInstance = DataManager()
  
  var listSubject = [Subject]()
  var listIDNoti = [String]()
  var user: User?
  
  var listCategorySelected = [Categorie]()
  var listCategory = [Categorie]()
  var listCategoryDefault: [Categorie]?
  
  var listSourceTypeSelected = [String]()
  var listSourceType = [String]()
  var listSourceTypeDefault = [String]()
  
  var listLanguages = [Categorie]()
  var listLanguageSelected = [Categorie]()
  var listLanguageDefault: [Categorie]?
  
  var listCountry = [Categorie]()
  var listCountrySelected = [Categorie]()
  var listCountryDefault: [Categorie]?
  
  var listRegionSelected = [Categorie]()
  var listRegion = [Categorie]()
  var listRegionDefault: [Categorie]?
  
  var listSourceSelected = [Source]()
  var listSource = [Source]()
  
  var stringFrom = ""
  var stringEnd = ""
  var stringSub = ""
  var listUser: [User]?
  func getListSubject() {
    let subjectService = SubjectService()
    subjectService.getListSubject { listSubject, error in
      if let subjects = listSubject {
        DataManager.shareInstance.listSubject = subjects
      }
    }
  }
  //MARK: - get UserInfo
  func getUserInfo(completion: (() -> Void)? = nil) {
    
   /// if DataManager.shareInstance.user == nil {
      let accountService = AccountService()
      accountService.getUserInfo(completionHandler: { user, _ in
        if let userGet = user {
          DataManager.shareInstance.user = userGet
        }
        if let com = completion {
          com()
        }
      })
//    } else {
//      if let com = completion {
//        com()
//      }
//    }
  }
  //MARK: - get list category default
  
  func getListCategoryDefault(completion: (() -> Void)? = nil) {
    
    if DataManager.shareInstance.listCategoryDefault == nil {
      let categorieService = CategorieService()
      categorieService.getListCategorie(type: .categories) { listCategory, error in
        if let category = listCategory {
          DataManager.shareInstance.listCategoryDefault = category
        }
        if let com = completion {
          com()
        }
      }
    } else {
      if let com = completion {
        com()
      }
    }
  }
  
  func getListLanguageDefault(completion: (() -> Void)? = nil) {
    
    if DataManager.shareInstance.listLanguageDefault == nil {
      let categorieService = CategorieService()
      categorieService.getListCategorie(type: .languages) { listLanguaes, error in
        if let languages = listLanguaes {
          DataManager.shareInstance.listLanguageDefault = languages
        }
        if let com = completion {
          com()
        }
      }
    } else {
      if let com = completion {
        com()
      }
    }
  }
  
  func getListCountryDefault(completion: (() -> Void)? = nil) {
    
    if DataManager.shareInstance.listCountryDefault == nil {
      let categorieService = CategorieService()
      categorieService.getListCategorie(type: .countries) { listCountry, error in
        if let countries = listCountry {
          DataManager.shareInstance.listCountryDefault = countries
        }
        if let com = completion {
          com()
        }
      }
    } else {
      if let com = completion {
        com()
      }
    }
  }
  
  func getListRegionDefault(completion: (() -> Void)? = nil) {
    
    if DataManager.shareInstance.listRegionDefault == nil {
      let categorieService = CategorieService()
      categorieService.getListCategorie(type: .regions) { listRegions, error in
        if let regions = listRegions {
          DataManager.shareInstance.listRegionDefault = regions
        }
        if let com = completion {
          com()
        }
      }
    } else {
      if let com = completion {
        com()
      }
    }
  }
  
  func getListSource(completion: (() -> Void)? = nil) {
    let categorieService = CategorieService()
    categorieService.getListSource(completionHandler: { listSource, error in
      if let sources = listSource {
        DataManager.shareInstance.listSource = sources
      }
      if let com = completion {
        com()
      }
    })
  }
  
  //MARK: - get list category show
  func getListSourceType() {
    if let soureTypeFill = self.user?.settings?.sourcetype {
      self.listSourceType = self.listSourceTypeDefault.filter {
        soureTypeFill.contains($0)
      }
      if self.listSourceType.count == 0 {
        self.listSourceType = self.listSourceTypeDefault
      }
    }
  }
  
  func getListCountry() {
    if let defaultCountry = self.listCountryDefault, let countryFill = self.user?.settings?.country {
      self.listCountry = defaultCountry.filter {
        countryFill.contains($0._id!)
      }
      if self.listCountry.count == 0 {
        self.listCountry = defaultCountry
      }
    }
  }
  func getListCategory() {
    if let defaultCategory = self.listCategoryDefault, let categoryFill = self.user?.settings?.category {
      self.listCategory = defaultCategory.filter {
        categoryFill.contains($0._id!)
      }
      if self.listCategory.count == 0 {
        self.listCategory = defaultCategory
      }
    }
  }
  func getListLanguage() {
    if let defaultLanguage = self.listLanguageDefault, let languageFill = self.user?.settings?.lang {
      self.listLanguages = defaultLanguage.filter {
        languageFill.contains($0._id!)
      }
      if self.listLanguages.count == 0 {
        self.listLanguages = defaultLanguage
      }
    }
  }
  func getListRegion() {
    if let defaultRegion = self.listRegionDefault, let regionFill = self.user?.settings?.region {
      self.listRegion = defaultRegion.filter {
        regionFill.contains($0._id!)
      }
      if self.listRegion.count == 0 {
        self.listRegion = defaultRegion
      }
    }
  }
}
