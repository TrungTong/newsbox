//
//  Constant.swift
//  Newsbox
//
//  Created by Trung Tong on 1/31/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

struct Constant {
  static let JWT: String                  = "JWT "
  static let Authorization: String        = "Authorization"
  static let token: String                = "token"
  static let items: String                = "items"
  static let results: String              = "results"
}
struct ConstantUD {
  static let UDURL: String        = "UserDefaultURL"
  static let UDUN: String = "UserDefaultURLUN"
  static let isCellFull: String       = "isCellFull"
}
let heightCellNewsFull: CGFloat = 80
typealias category = (String, EnumCategory)
typealias TopicType = (String, String)
typealias TopicSubject = (String, [Subject])
// Definition:
extension Notification.Name {
  static let NotifyShowFullOrLess = Notification.Name("NotifyShowFullOrLess")
  static let NotifySetting = Notification.Name("NotifySetting")
  static let NotifyHomeResetAll = Notification.Name("NotifyHomeResetAll")
}
