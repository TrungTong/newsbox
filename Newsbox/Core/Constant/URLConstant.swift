//
//  URLConstant.swift
//  Newsbox
//
//  Created by Trung Tong on 1/31/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import UIKit

struct URLConstant {
  static let apiLogin: String                    = "/api-token-auth"
  static let apiGetUserInfo: String              = "/user_info"
  static let apiListUser: String                 = "/v3/users?v=1"
  
  static let apiGetNewsDetails: String           = "/post/"
  static let apiSearchContent: String            = "/search"
  static let apiSearchAggs: String               = "/search/aggs"
  static let apiSubjects: String                 = "/subjects"
  static let apitopics: String                   = "/v4/topics"
  static let apiTopicTypes: String               = "/v4/topic_types"
    
  static let apiListCategory: String             = "/categories"
  static let apiListCountries: String            = "/countries"
  static let apiListRegions: String              = "/regions"
  static let apiListLanguages: String            = "/languages"
  static let apiListSource: String               = "/sources"
    
  static let apiListCart: String                 = "/v4/cart"
  static let apiListCartClearAll: String         = "/v4/cart/clear"
    
  static let apiListShares: String               = "/v4/shares"
  static let apiListShared: String               = "/v4/shared"
  
  static let apiListIssues: String               = "/v4/issues"
  
  static let apiSummaries: String                = "/summaries"
  static let apiSummary: String                  = "/summary"
  
  static let apiSetting: String                  = "/setting"
  static let apisettingNotify: String           = "/setting_notify"
}

