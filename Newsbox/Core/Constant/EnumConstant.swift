//
//  EnumConstant.swift
//  Newsbox
//
//  Created by TrungTong on Jan/31/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
enum EnumErrorCode {
  case success
  case notConnectedToInternet
  case other
  case parseError
  
  var message: String {
    switch self {
    case .success: return "Thành công"
    case .notConnectedToInternet: return "Không thể kết nối! Vui lòng thử lại sau"
    case .other: return "Có lỗi xảy ra! Vui lòng thử lại sau"
    case .parseError: return "Có lỗi xảy ra! Vui lòng thử lại sau"
    }
  }
}
enum typeDomain: String {
  case website = "website"
  case youtube = "youtube"
  case google = "google"
  case facebook = "facebook"
  static let allValues = [website.rawValue,
                          youtube.rawValue,
                          google.rawValue,
                          facebook.rawValue]

}

enum EnumCategory {
    case time
    case categories
    case countries
    case regions
    case languages
    case sourceType
    case source
    case settingCategory
    case settingCountries
    case settingLanguages
    case settingRegions
    case settingNotify
    case settingSourcetype
    var url: String {
        switch self {
        case .categories: return URLConstant.apiListCategory
        case .countries: return URLConstant.apiListCountries
        case .regions: return URLConstant.apiListRegions
        case .languages: return URLConstant.apiListLanguages
        case .source: return URLConstant.apiListSource
        default:
            return ""
        }
    }
}
enum DateFormatString: String {
  case YYYYMMDD = "yyyyMMdd"
  case DDMMYYYY = "dd/MM/yyyy"
  case DATEFULL = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ"
  case DATEFULLNOTSSS = "yyyy-MM-dd'T'HH:mm:ss"
  case DATE = "yyyy-MM-dd"
  case DATEABCDEF = "HH:mm:ss dd-MM-yyyy"
  case DATEABC = "yyyy-MM-dd'T'HH:mm:ss+0000"
  case DATETTT = "yyyy-MM-dd HH:mm:ss.SSSS"
  case DATEFULLSSSS = "yyyy-MM-dd'T'HH:mm:ss.SSSS"
  case DATEFULLALL = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"
}
enum CartType {
    case get
    case post
    case delete
}
enum TypeDialog {
    case user
    case priority
    case issue
}
enum EnumPriority: Int {
    case remarkable = 1
    case important
    case emergency
    var stringValue: String {
        switch self {
        case .remarkable: return "Đáng chú ý"
        case .important: return "Tin quan trọng"
        case .emergency: return "Khẩn cấp"
        }
    }
    static let allValues = [remarkable, important, emergency]
}

