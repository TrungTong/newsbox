//
//  MaskView.swift
//  GTVLiveStream
//
//  Created by TrungTong on Oct/26/17.
//  Copyright © 2017 Nobody. All rights reserved.
//

import Foundation
import UIKit

class MaskView: UIView, Maskable {
  
  public var maskId: String = "MaskView"
  
  @IBOutlet var activityIndicator: UIActivityIndicatorView!
  @IBOutlet var contentView: UIView!
  
  public init() {
    super.init(frame: CGRect.zero)
    
    backgroundColor = UIColor.clear
    self.addSubView(contentView)
    
    activityIndicator.startAnimating()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  deinit {
    activityIndicator.stopAnimating()
  }
}
