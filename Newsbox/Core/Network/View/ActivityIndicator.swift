//
//  ActivityIndicator.swift
//  Jpanet
//
//  Created by Tong Bao Trung on 4/21/17.
//  Copyright © 2017 TrungTong. All rights reserved.
//

import Foundation
import UIKit

class ActivityIndicator: UIActivityIndicatorView, Maskable {
  
  public var maskId: String = "ActivityIndicator"
  
  public override init(activityIndicatorStyle style: UIActivityIndicatorViewStyle = .gray) {
    super.init(activityIndicatorStyle: style)
    
    backgroundColor = UIColor.white
    startAnimating()
  }
  
  required public init(coder: NSCoder) {
    super.init(coder: coder)
  }
  
  deinit {
    stopAnimating()
  }
}
