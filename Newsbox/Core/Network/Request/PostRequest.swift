//
//  PostRequest.swift
//  Newsbox
//
//  Created by Trung Tong on 2/7/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import Alamofire

struct PostRequest: BaseRequestable {
    var headers: HTTPHeaders?
    
    /// The request parameters.
    
    var parameters: [String : Any]?
    var path: String
    var method: Alamofire.HTTPMethod
    //getListCart
    init(url: String = URLConstant.apiListCart) {
        self.path = url
        self.parameters = nil
        self.method = .get
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
    }
    //save a Cart
    init(news: News) {
        self.path = URLConstant.apiListCart
        self.parameters = ["id": news.idNews ?? "",
                           "title": news.title ?? "",
                           "domain": news.domain ?? "",
                           "logo": news.logo ?? "",
                           "lang": news.lang ?? "",
                           "category": news.category ?? "",
                           "collected_time": news.collectedTime ?? ""]
        self.method = .post
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
    }
    //delete a cart
    init(uniqueID: String) {
        self.path = URLConstant.apiListCart + "/" + uniqueID
        self.parameters = nil
        self.method = .delete
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
    }
  
   //delete all
  init(deleteAll: String) {
    self.path = URLConstant.apiListCartClearAll
    self.parameters = nil
    self.method = .get
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }
}
