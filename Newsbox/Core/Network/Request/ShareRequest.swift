//
//  ShareRequest.swift
//  Newsbox
//
//  Created by Trung Tong on 2/7/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import Alamofire

struct ShareRequest: BaseRequestable {
  var headers: HTTPHeaders?
  
  /// The request parameters.
  
  var parameters: [String : Any]?
  var path: String
  var method: Alamofire.HTTPMethod
  //getList Shared or Shared
  init(isShared: Bool) {
    if isShared {
      self.path = URLConstant.apiListShares
    } else {
      self.path = URLConstant.apiListShared
    }
    self.parameters = nil
    self.method = .get
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }
  // get listUser
  init() {
    self.path = URLConstant.apiListUser
    self.parameters = nil
    self.method = .get
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }
  //    //share a news
  init(news: News,
       priority: Int,
       listUser: [Int]) {
    self.path = URLConstant.apiListShares
    self.parameters = ["id": news.idNews ?? "",
                       "title": news.title ?? "",
                       "domain": news.domain ?? "",
                       "logo": news.logo ?? "",
                       "collected_time": news.collectedTime ?? "",
                       "priority": priority,
                       "shares": listUser]
    self.method = .post
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }
  //Ignore or delete a news Share
  init(isIgnore: Bool,
       idNewIgnore: String) {
    if isIgnore {
      self.path = URLConstant.apiListShares +  "/" + idNewIgnore
    } else {
      self.path = URLConstant.apiListShares +  "/" + idNewIgnore
    }
    self.parameters = nil
    self.method = .delete
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }
  
}
