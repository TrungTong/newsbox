//
//  NewsRequest.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import Alamofire

struct NewsRequest: BaseRequestable {
  /// The request parameters.
  
  var parameters: [String : Any]?
  var path: String
  var method: Alamofire.HTTPMethod
  var headers: HTTPHeaders?
  
  //search arg
  init(searchargListCategory: [String],
       listLang: [String],
       listDomain: [String],
       from: String,
       listSource: [String],
       listCountry: [String],
       listRegion: [String],
       listSourcetype: [String],
       search: String,
       to: String,
       pageNumber: Int,
       time: String,
       isSearchArg: Bool) {
    if isSearchArg {
        self.path = URLConstant.apiSearchAggs
    } else {
         self.path = URLConstant.apiSearchContent
    }

    self.parameters = ["category": searchargListCategory,
                       "lang": listLang,
                       "domain": listDomain,
                       "from": from,
                       "source": listSource,
                       "country": listCountry,
                       "region": listRegion,
                       "sourcetype": listSourcetype,
                       "search": search,
                       "to": to,
                       "page_number": pageNumber,
                       "time": time]
    self.method = .post
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }

    //news details
    init(idNews: String) {
        self.path = URLConstant.apiGetNewsDetails + idNews
        self.parameters = nil
        self.method = .get
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
    }
}

