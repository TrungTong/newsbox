//
//  NewsPointRequest.swift
//  Newsbox
//
//  Created by TrungTong on Feb/21/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import Alamofire

struct NewsPointRequest: BaseRequestable {
  var headers: HTTPHeaders?
  
  /// The request parameters.
  
  var parameters: [String : Any]?
  var path: String
  var method: Alamofire.HTTPMethod
  //get List NewsPoint
  init(limit: Int, offset: Int) {
    self.path = URLConstant.apiSummaries + "/" + limit.toString() + "/" + offset.toString()
    self.parameters = nil
    self.method = .get
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }
    //get Details NewsPoint
    init(idNewsPoint: String) {
        self.path = URLConstant.apiSummary + "/" + idNewsPoint
        self.parameters = nil
        self.method = .get
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
    }
}
