//
//  IssueRequest.swift
//  Newsbox
//
//  Created by TrungTong on Feb/19/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import Alamofire

struct IssueRequest: BaseRequestable {
    var headers: HTTPHeaders?
    
    /// The request parameters.
    
    var parameters: [String : Any]?
    var path: String
    var method: Alamofire.HTTPMethod
    //getListIssue
    init(url: String = URLConstant.apiListIssues) {
        self.path = url
        self.parameters = nil
        self.method = .get
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
    }
    //update Issue
    init(idIssue: Int,
         issueTitle: String,
         duedate: String,
         createdBy: Int,
         updatedBy: Int,
         description: String,
         updatedTime: String,
         createdTime: String,
         assignees: [Int],
        completed: Bool,
        starred: Bool,
        important: Bool,
        deleted: Bool) {
        self.path = URLConstant.apiListIssues + "/" + idIssue.toString()
        self.parameters = ["id": idIssue,
                           "title": issueTitle,
                           "duedate": duedate,
                           "created_by": createdBy,
                           "updated_by": updatedBy,
                           "description": description,
                           "updated_time": updatedTime,
                           "created_time": createdTime,
                           "assignees": assignees,
                           "completed": completed,
                           "starred": starred,
                           "important": important,
                           "deleted": deleted,
                           "tags": []]
      if duedate == "" {
        self.parameters?.removeValue(forKey: "duedate")
      }
        self.method = .put
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
    }
  
  //create Issue
  init(issueTitle: String,
       description: String,
       startTime: String,
       duedate: String,
       completed: Bool,
       starred: Bool,
       important: Bool,
       assignees: [Int]) {
    self.path = URLConstant.apiListIssues
    self.parameters = ["id": "",
                       "title": issueTitle,
                       "description": description,
                       "start_time": startTime,
                       "duedate": duedate,
                       "completed": completed,
                       "starred": starred,
                       "important": important,
                       "assignees": assignees,
                       "deleted": false,
                       "tags": []]
    self.method = .post
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }
}

