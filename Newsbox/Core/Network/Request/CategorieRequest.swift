//
//  CategorieRequest.swift
//  Newsbox
//
//  Created by TrungTong on Feb/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import Alamofire

struct CategorieRequest: BaseRequestable {
  var headers: HTTPHeaders?

  /// The request parameters.
  
  var parameters: [String : Any]?
  var path: String
  var method: Alamofire.HTTPMethod
  //getListCategor
  init(listCategory type: EnumCategory) {
    self.path = type.url
    self.parameters = nil
    self.method = .get
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }
  
 // search source
    //edit setting List
    init(listCategory: [String],
         listLang: [String],
         listCountry: [String],
         listRegion: [String],
         listSourceType: [String]) {
        self.path = URLConstant.apiListSource
        self.parameters = ["category": listCategory,
                           "lang": listLang,
                           "country": listCountry,
                           "region": listRegion,
                           "sourcetype": listSourceType]
      
        self.method = .post
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
    }
}
