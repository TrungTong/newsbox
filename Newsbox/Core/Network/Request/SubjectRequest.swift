//
//  SubjectRequest.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import Alamofire

struct SubjectRequest: BaseRequestable {
    var headers: HTTPHeaders?
    
    /// The request parameters.
    
    var parameters: [String : Any]?
    var path: String
    var method: Alamofire.HTTPMethod
    //getListSubject
    init(getListSubject: String) {
        self.path = URLConstant.apiSubjects
        self.parameters = nil
        self.method = .get
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
        
    }
    //getListTopic
    init(getListTopic: String) {
        self.path = URLConstant.apitopics
        self.parameters = nil
        self.method = .get
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
        
    }
    //getListTopicType
    init(getListTopicType: String) {
        self.path = URLConstant.apiTopicTypes
        self.parameters = nil
        self.method = .get
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
        
    }
}
