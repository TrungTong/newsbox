//
//  AccountRequest.swift
//  Newsbox
//
//  Created by TrungTong on Jan/31/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import Alamofire

struct AccountRequest: BaseRequestable {
    /// The request parameters.
    
    var parameters: [String : Any]?
    var path: String
    
    var method: Alamofire.HTTPMethod
    var headers: HTTPHeaders?
    
    init(withUsername userName: String, andPassword password: String) {
        self.path = URLConstant.apiLogin
        self.method = .post
        self.parameters = ["username": userName,
                           "password": password]
    }
    
    init() {
        self.path = URLConstant.apiGetUserInfo
        self.parameters = nil
        self.method = .get
        self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                        "Content-Type" :"application/json"]
    }
}
