//
//  SettingRequest.swift
//  Newsbox
//
//  Created by TrungTong on Feb/22/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import Alamofire

struct SettingRequest: BaseRequestable {
  var headers: HTTPHeaders?
  
  /// The request parameters.
  
  var parameters: [String : Any]?
  var path: String
  var method: Alamofire.HTTPMethod
  //edit setting List
  init(listCategory: [String],
       listLang: [String],
       listCountry: [String],
       listRegion: [String],
       listSouceType: [String]) {
    self.path = URLConstant.apiSetting
    self.parameters = ["category": listCategory,
                       "lang": listLang,
                       "country": listCountry,
                       "region": listRegion,
                       "sourcetype": listSouceType]
    
    self.method = .post
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }
  init() {
    self.path = URLConstant.apisettingNotify
    self.parameters = nil
    self.method = .get
    self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
                    "Content-Type" :"application/json"]
  }

}
//extension SettingRequest {
//    init(listID: [String]) {
//      self.path = URLConstant.apisettingNotify
//    //  self.parameters = listID
//      self.parameters = listID as Any as! [String : Any]
//      self.method = .post
//      self.headers = [Constant.Authorization: UserManager.shareInstance.userToken,
//                      "Content-Type" :"application/json"]
//    }
//}

