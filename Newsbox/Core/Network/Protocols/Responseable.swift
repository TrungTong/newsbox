//
//  Responseable.swift
//  Jpanet
//
//  Created by TrungTong on Apr/19/17.
//  Copyright © 2017 TrungTong. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

extension DataRequest {
  func load(load: Loadable) -> Self {
    print("chuan bi load")
    load.begin()
    return response { (response: DefaultDataResponse) in
      print("load xong roi")
      if let err = response.error {
        load.showMessageError(message: err.localizedDescription)
      }
      load.end()
    }
    }
    func loadWithCallbackError(load: Loadable,
          completionHandler: @escaping( _ error: EnumErrorCode?) -> Void) -> Self {
        load.begin()
        return response { (response: DefaultDataResponse) in
            if let error = response.error as NSError? {
                var errorCode: EnumErrorCode = EnumErrorCode.other
                if error.code == NSURLErrorNotConnectedToInternet {
                    errorCode = EnumErrorCode.notConnectedToInternet
                    completionHandler(errorCode)
                }
            }
            load.end()
        }
  }
}
