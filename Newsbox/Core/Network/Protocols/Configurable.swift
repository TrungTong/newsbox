//
//  Configurable.swift
//  Jpanet
//
//  Created by TrungTong on Apr/19/17.
//  Copyright © 2017 TrungTong. All rights reserved.
//

import Foundation

var defaultConfiguration = Configuration()

protocol Configurable {
  
  /// The `configuration`.
  var configuration: Configuration { get }
  
}

// extension Configurable where Self: Requestable {
extension Configurable {
  /// `Restofire.defaultConfiguration`
  var configuration: Configuration {
    return defaultConfiguration
  }
  
}
