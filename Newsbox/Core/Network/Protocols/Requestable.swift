//
//  Requestable.swift
//  Jpanet
//
//  Created by TrungTong on Apr/19/17.
//  Copyright © 2017 TrungTong. All rights reserved.
//

import Foundation
import Alamofire

extension Requestable {
    
    /// Creates a `DataRequest` from the specified `form`
    ///
    /// - Parameter form: Object conforms to `BaseRequestable` protocol.
    /// - Returns: The created `DataRequest`.
    
    func request(_ form: BaseRequestable) -> DataRequest {
        let baseURL: String = UserDefaults.standard.value(forKey: ConstantUD.UDURL) as? String ?? ""
        let requestPath = baseURL + form.path
        print(requestPath)
        return Alamofire.request(
            requestPath,
            method: form.method,
            parameters: form.parameters,
            encoding: JSONEncoding.default,
            headers: form.headers)
    }
    
    func requestOtherLink(_ form: BaseRequestable) -> DataRequest {
        
        let requestPath = form.path
        print(requestPath)
        return Alamofire.request(
            requestPath,
            method: form.method,
            parameters: form.parameters,
            encoding: JSONEncoding.default,
            headers: form.headers)
    }
    
    func upload(_ form: MBUploadMultiFormDataFormable, completion: ((UploadRequest) -> Void)?) {
        
        let requestPath = form.baseURL + form.path
        
        Alamofire.upload(
            multipartFormData: form.multipartFormData,
            usingThreshold: form.encodingMemoryThreshold,
            to: requestPath,
            method: form.method,
            headers: form.headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    completion?(upload)
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
    }
}

protocol Requestable {
    
}
