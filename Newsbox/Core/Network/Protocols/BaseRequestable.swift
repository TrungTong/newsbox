//
//  BaseRequestable.swift
//  Jpanet
//
//  Created by TrungTong on Apr/19/17.
//  Copyright © 2017 TrungTong. All rights reserved.
//

import Foundation
import Alamofire

protocol BaseRequestable: Configurable {
    
  /// The base URL.
  var baseURL: String { get }
  /// The path relative to base URL.
  var path: String { get }
  
  /// The HTTP Method.
  var method: Alamofire.HTTPMethod { get }
  
  /// The request parameter encoding.
  var encoding: Alamofire.ParameterEncoding { get }
  
  /// The HTTP headers.
  var headers: HTTPHeaders? { get }
  
  /// The request parameters.
  var parameters: [String: Any]? { get }
  
}
// MARK: - Default Implementation
extension BaseRequestable {
    
  /// `configuration.BaseURL`
  var baseURL: String {
    return configuration.baseURL
  }
  
  /// `configuration.method`
  public var method: Alamofire.HTTPMethod {
    return configuration.method
  }
  
  /// `configuration.encoding`
  public var encoding: Alamofire.ParameterEncoding {
    return configuration.encoding
  }
  
  /// `nil`
  public var headers: HTTPHeaders? {
     return nil
  }
  
  /// `nil`
  public var parameters: Any? {
    return [ : ]
  }
}

extension UploadFormable {
  
//  var method: Alamofire.HTTPMethod {
//    return .post
//  }
  var encodingMemoryThreshold: UInt64 {
    return SessionManager.multipartFormDataEncodingMemoryThreshold
  }
}

/// Upload Form protocol, Base protocol for upload request form
protocol UploadFormable: BaseRequestable {
  var encodingMemoryThreshold: UInt64 { get }
  var multipartFormData: (MultipartFormData) -> Void { get }
}

 extension MBUploadMultiFormDataFormable {
  var encodingMemoryThreshold: UInt64 {
    return SessionManager.multipartFormDataEncodingMemoryThreshold
  }
}

/// Conforming to this protocol to create an upload form that contains multiformdata
 protocol MBUploadMultiFormDataFormable: BaseRequestable {
  var encodingMemoryThreshold: UInt64 { get }
  
  var multipartFormData: (MultipartFormData) -> Void { get }
}
