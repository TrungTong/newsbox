//
//  Configuration.swift
//  Jpanet
//
//  Created by TrungTong on Apr/19/17.
//  Copyright © 2017 TrungTong. All rights reserved.
//

import Foundation
import Alamofire

struct Configuration {
  
  /// The base URL. `nil` by default.
  var baseURL: String = UserDefaults.standard.value(forKey: ConstantUD.UDURL) as? String ?? ""
  
  /// The HTTP Method. `.POST` by default.
  var method: Alamofire.HTTPMethod = .post
  
  /// The request parameter encoding. `.JSON` by default.
  var encoding: Alamofire.ParameterEncoding = JSONEncoding.default
  
  /// The HTTP headers. `nil` by default.
  //public var headers: HTTPHeaders = [Constant.Authorization: UserManager.shareInstance.userToken,
    //                                 "Content-Type" :"application/json"]
  
  /// The `Alamofire.DataResponseSerializer`.
  public var dataResponseSerializer: Alamofire.DataResponseSerializer<Any> = Alamofire.DataRequest.jsonResponseSerializer()
  
  /// `Configuration` Intializer
  ///
  /// - returns: new `Configuration` object
  public init() {
  }
  
}
