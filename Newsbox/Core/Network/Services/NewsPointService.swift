//
//  NewsPointService.swift
//  Newsbox
//
//  Created by TrungTong on Feb/21/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import ObjectMapper

struct NewsPointService: Requestable {
    
    // MARK: - Get List Summaries
    
    // [START Get List Summaries
    func getListSummaries(limit: Int,
                          offset: Int,
                          completionHandler: @escaping(_ subNewsPoint: SubNewsPoint?, _ error: EnumErrorCode?) -> Void) {
        
        let newsPointRequest = NewsPointRequest(limit: limit, offset: offset)
        
        request(newsPointRequest).loadWithCallbackError(load: LoadConfig()) { error in
            //error server
            completionHandler(nil, error)
            }.responseObject() { (response: DataResponse<SubNewsPoint>) in
                if let value = response.result.value {
                    //success
                    completionHandler(value, EnumErrorCode.success)
                } else {
                    // success but parse error
                    completionHandler(nil, EnumErrorCode.parseError)
                }
        }
    }
    
    // [END Get List Summaries]
    
    // MARK: - Get Details Summary
    
    // [START Get Details Summary
    func getDetailsSummary(idNewsPoint: String,
                          completionHandler: @escaping(_ newsPoint: NewsPoint?, _ error: EnumErrorCode?) -> Void) {
        
        let newsPointRequest = NewsPointRequest(idNewsPoint: idNewsPoint)
        
        request(newsPointRequest).loadWithCallbackError(load: LoadConfig()) { error in
            //error server
            completionHandler(nil, error)
            }.responseObject() { (response: DataResponse<NewsPoint>) in
                if let value = response.result.value {
                    //success
                    completionHandler(value, EnumErrorCode.success)
                } else {
                    // success but parse error
                    completionHandler(nil, EnumErrorCode.parseError)
                }
        }
    }
    
    // [END Get Details Summary]
}
