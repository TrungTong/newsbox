//
//  NewsService.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import ObjectMapper

struct NewsService: Requestable {
  
  // MARK: - Get list news
  
  // [START Get list news
  func getListNews(listCategory: [String],
                   listLang: [String],
                   listDomain: [String],
                   from: String,
                   listSource: [String],
                   listCountry: [String],
                   listRegion: [String],
                   listSourcetype: [String],
                   search: String,
                   to: String,
                   pageNumber: Int,
                   time: String,
                   completionHandler: @escaping(_ listItems: [Item]?, _ error: EnumErrorCode?) -> Void) {
  
    let newsRequest = NewsRequest(searchargListCategory: listCategory,
                                  listLang: listLang,
                                  listDomain: listDomain,
                                  from: from,
                                  listSource: listSource,
                                  listCountry: listCountry,
                                  listRegion: listRegion,
                                  listSourcetype: listSourcetype,
                                  search: search,
                                  to: to,
                                  pageNumber: pageNumber,
                                  time: time,
                                  isSearchArg: true)
    
    request(newsRequest).loadWithCallbackError(load: LoadConfig()) { error in
      //error server
      completionHandler(nil, error)
      }.responseArray(keyPath: Constant.items) { (response: DataResponse<[Item]>) in
        if let value = response.result.value {
          //success
          completionHandler(value, EnumErrorCode.success)
        } else {
          // success but parse error
          completionHandler(nil, EnumErrorCode.parseError)
        }
    }
  }
  // [END Get list news]
    
    // MARK: - Get list news not category
    
    // [START Get list news not category
    func getListNewsNotCategory(listCategory: [String],
                     listLang: [String],
                     listDomain: [String],
                     from: String,
                     listSource: [String],
                     listCountry: [String],
                     listRegion: [String],
                     listSourcetype: [String],
                     search: String,
                     to: String,
                     pageNumber: Int,
                     time: String,
                     completionHandler: @escaping(_ listNews: [News]?, _ error: EnumErrorCode?) -> Void) {
        let newsRequest = NewsRequest(searchargListCategory: listCategory,
                                      listLang: listLang,
                                      listDomain: listDomain,
                                      from: from,
                                      listSource: listSource,
                                      listCountry: listCountry,
                                      listRegion: listRegion,
                                      listSourcetype: listSourcetype,
                                      search: search,
                                      to: to,
                                      pageNumber: pageNumber,
                                      time: time,
                                      isSearchArg: false)
        
        request(newsRequest).loadWithCallbackError(load: LoadConfig()) { error in
            //error server
            completionHandler(nil, error)
            }.responseArray(keyPath: Constant.items) { (response: DataResponse<[News]>) in
                if let value = response.result.value {
                    //success
                    completionHandler(value, EnumErrorCode.success)
                } else {
                    // success but parse error
                    completionHandler(nil, EnumErrorCode.parseError)
                }
        }
    }
    // [END Get list news not category]
    
   // [START Get news details
    func getNewsDetails(idNews: String,
                     completionHandler: @escaping(_ news: News?, _ error: EnumErrorCode?) -> Void) {
        let newsRequest = NewsRequest(idNews: idNews)
        
        request(newsRequest).loadWithCallbackError(load: LoadConfig()) { error in
            //error server
            completionHandler(nil, error)
            }.responseObject() { (response: DataResponse<News>) in
                if let value = response.result.value {
                    //success
                    completionHandler(value, EnumErrorCode.success)
                } else {
                    // success but parse error
                    completionHandler(nil, EnumErrorCode.parseError)
                }
        }
    }
    // [END Get news details]
}

