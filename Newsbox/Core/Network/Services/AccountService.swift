//
//  AccountService.swift
//  Newsbox
//
//  Created by TrungTong on Jan/31/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import ObjectMapper

struct AccountService: Requestable {
    //login user name and password
    func login(sender: Loadable,
               user: String,
               pass: String,
               completionHandler: @escaping(_ isSuccess: Bool) -> Void) {
        
        let accountRequest = AccountRequest(withUsername: user, andPassword: pass)
        
        request(accountRequest).load(load: sender).responseJSON { (response) in
            
            let result = response.result.value as? [String: Any]
            if let token = result?[Constant.token] as? String {
                UserManager.shareInstance.userToken = Constant.JWT + token
                UserManager.shareInstance.isLogin = true
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
    }
    
    func getUserInfo(completionHandler: @escaping(_ user: User?, _ error: EnumErrorCode?) -> Void) {
        
        let accountRequest = AccountRequest()
        
        request(accountRequest).loadWithCallbackError(load: LoadConfig()) { error in
            //error server
            completionHandler(nil, error)
            }.responseObject() { (response: DataResponse<User>) in
                if let value = response.result.value {
                    //success
                    completionHandler(value, EnumErrorCode.success)
                } else {
                    // success but parse error
                    completionHandler(nil, EnumErrorCode.parseError)
                }
        }
    }
}
