//
//  ShareService.swift
//  Newsbox
//
//  Created by Trung Tong on 2/7/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import ObjectMapper
import MBProgressHUD

struct ShareService: Requestable {
  // MARK: - Get List Shared or Shared
  
  // [START Get List Shared or Shared
  func getListSharedOrShared(isShared: Bool,
                             completionHandler: @escaping(_ cart: Cart?, _ error: EnumErrorCode?) -> Void) {
    
    let shareRequest = ShareRequest(isShared: isShared)
    //show loading
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.showAdded(to: window, animated: true)
    }
    request(shareRequest).loadWithCallbackError(load: LoadConfig()) { error in
      //error server
      //hide loading
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      completionHandler(nil, error)
      }.responseObject() { (response: DataResponse<Cart>) in
        //hide loading
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
          MBProgressHUD.hide(for: window, animated: true)
        }
        if let value = response.result.value {
          //success
          completionHandler(value, EnumErrorCode.success)
        } else {
          // success but parse error
          completionHandler(nil, EnumErrorCode.parseError)
        }
    }
  }
  // [END Get List Shared or Shared]
  
  // [START Get List User
  func getListUser(completionHandler: @escaping(_ listUser: [User]?, _ error: EnumErrorCode?) -> Void) {
    
    let shareRequest = ShareRequest()
    //show loading
//    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
//      MBProgressHUD.showAdded(to: window, animated: true)
//    }
    request(shareRequest).loadWithCallbackError(load: LoadConfig()) { error in
      //error server
      completionHandler(nil, error)
      }.responseArray() { (response: DataResponse<[User]>) in
        //hide loading
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
          MBProgressHUD.hide(for: window, animated: true)
        }
        if let value = response.result.value {
          //success
          completionHandler(value, EnumErrorCode.success)
        } else {
          // success but parse error
          completionHandler(nil, EnumErrorCode.parseError)
        }
    }
  }
  // [END Get List User]
  
  // [START share a News]
  func shareANews(news: News,
                  priority: Int,
                  listUser: [Int],
                  completionHandler: @escaping(_ isSuccess: Bool) -> Void) {
    let shareRequest = ShareRequest(news: news,
                                    priority: priority,
                                    listUser: listUser)
    
    request(shareRequest).load(load: LoadConfig()).responseJSON { (response) in
      
      switch response.result {
      case .success(_):
        
        if let data = response.result.value as? NSDictionary {
          if let newArr = data.object(forKey: "non_field_errors") as? NSArray {
            let yourArray = NSMutableArray(array: newArr)
            if let str = yourArray.firstObject as? String {
              Utils.showMessageError(message: str)
            }
            completionHandler(false)
          } else {
            Utils.showMessageSucces(message: "Chia sẻ bài thành công!")
            completionHandler(true)
          }
        } else {
          Utils.showMessageError(message: "Có lỗi xảy ra. Vui lòng thử lại sau")
          completionHandler(false)
        }
        break
      case .failure(_):
        completionHandler(false)
      }
    }
  }
  // [END share a News]]
  // [START Ignore or delete a news Share]
  func ignoreOrDeleteANews(isIgnore: Bool,
                           idNewIgnore: String,
                           completionHandler: @escaping(_ isSuccess: Bool) -> Void) {
    let shareRequest = ShareRequest(isIgnore: isIgnore, idNewIgnore: idNewIgnore)

    //show loading
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.showAdded(to: window, animated: true)
    }
    request(shareRequest).load(load: LoadConfig()).responseJSON { (response) in
      //hide loading
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      switch response.result {
      case .success(_):
        if let data = response.result.value as? [String: Any], let status = data ["status"] as? Bool {
          if status {
            Utils.showMessageSucces(message: "Xóa bài thành công!")
            completionHandler(true)
          } else {
            Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
            completionHandler(false)
          }
        } else {
          Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
          completionHandler(false)
        }
        break
      case .failure(_):
        Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
        completionHandler(false)
      }
    }
    // [END delete a Cart]
  }
}
