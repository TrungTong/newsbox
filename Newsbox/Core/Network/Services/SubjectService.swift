//
//  SubjectService.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import ObjectMapper

struct SubjectService: Requestable {
    
    // MARK: - Get List Subject
    
    // [START Get List Subject
    
    func getListSubject(completionHandler: @escaping(_ listSubject: [Subject]?, _ error: EnumErrorCode?) -> Void) {
        
        let subjectRequest = SubjectRequest(getListSubject: "")
        
        request(subjectRequest).loadWithCallbackError(load: LoadConfig()) { error in
            //error server
            completionHandler(nil, error)
            }.responseArray() { (response: DataResponse<[Subject]>) in
                if let value = response.result.value {
                    //success
                    completionHandler(value, EnumErrorCode.success)
                } else {
                    // success but parse error
                    completionHandler(nil, EnumErrorCode.parseError)
                }
        }
    }
    // [END Get List Subject
    
    // MARK: - Get List Topic
    
    // [START Get List Topic
    
    // Get List Topic Type
    func getListTopicType(completionHandler: @escaping(_ listTopicType: [TopicType]?, _ error: EnumErrorCode?) -> Void) {
        
        let subjectRequest = SubjectRequest(getListTopicType: "")
        
        request(subjectRequest).load(load: LoadConfig()).responseJSON { response in
            guard let result = response.result.value as? [String: Any] else {
                //error server
                completionHandler(nil, EnumErrorCode.notConnectedToInternet)
                return
            }
            var list = [TopicType]()
            for dic in result {
                if let dicTopic = dic as? TopicType {
                    list.append(dicTopic)
                }
            }
            completionHandler(list, nil)
        }
    }
    
    func getListTopic(completionHandler: @escaping(_ listTopicSubject: [TopicSubject]?, _ error: EnumErrorCode?) -> Void) {
        
        let subjectRequest = SubjectRequest(getListTopic: "")
        
        request(subjectRequest).load(load: LoadConfig()).responseJSON { response in
            guard let result = response.result.value as? [String: Any] else {
                //error server
                completionHandler(nil, EnumErrorCode.notConnectedToInternet)
                return
            }
            var list = [TopicSubject]()
            for dic in result {
                let stringKey = dic.key
                let listSubject = Mapper<Subject>().mapArray(JSONArray: dic.value as! [[String : Any]])
     
                if listSubject.count > 0 {
                    let topicSubject = TopicSubject(stringKey, listSubject)
                    list.append(topicSubject)
                }
            }
            completionHandler(list, nil)
        }
    }
    // [END Get List Topic
}
