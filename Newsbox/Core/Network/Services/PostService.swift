//
//  PostService.swift
//  Newsbox
//
//  Created by Trung Tong on 2/7/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import ObjectMapper
import MBProgressHUD

struct PostService: Requestable {
  // MARK: - Get List Cart
  
  // [START Get List Cart
  func getListCart(url: String?, completionHandler: @escaping(_ cart: Cart?, _ error: EnumErrorCode?) -> Void) {
    var postRequest: PostRequest!
    if let urlString = url {
      postRequest = PostRequest(url: urlString)
    } else {
      postRequest = PostRequest()
    }
   
    request(postRequest).loadWithCallbackError(load: LoadConfig()) { error in
      //error server
      //hide loading
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      completionHandler(nil, error)
      }.responseObject() { (response: DataResponse<Cart>) in
        if let value = response.result.value {
          //success
          completionHandler(value, EnumErrorCode.success)
        } else {
          // success but parse error
          completionHandler(nil, EnumErrorCode.parseError)
        }
        //hide loading
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
          MBProgressHUD.hide(for: window, animated: true)
        }
    }
  }
  // [END Get list cart]
  // [START save a Cart]
  func saveACart(news: News,
                 completionHandler: @escaping(_ isSuccess: Bool) -> Void) {
    
    let postRequest = PostRequest(news: news)
    
    request(postRequest).load(load: LoadConfig()).responseJSON { (response) in
      
      switch response.result {
      case .success(_):
        
        if let data = response.result.value as? NSDictionary {
          if let newArr = data.object(forKey: "non_field_errors") as? NSArray {
            let yourArray = NSMutableArray(array: newArr)
            if let str = yourArray.firstObject as? String {
              Utils.showMessageError(message: str)
            }
            completionHandler(false)
          } else {
            Utils.showMessageSucces(message: "Lưu bài thành công!")
            completionHandler(true)
          }
        } else {
          Utils.showMessageError(message: "Có lỗi xảy ra. Vui lòng thử lại sau")
          completionHandler(false)
        }
        break
      case .failure(_):
        completionHandler(false)
      }
    }
  }
  // [END save a Cart]
  // [START delete a Cart]
  func deleteACart(uniqueID: String,
                   completionHandler: @escaping(_ isSuccess: Bool) -> Void) {
    
    let postRequest = PostRequest(uniqueID: uniqueID)
    //show loading
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.showAdded(to: window, animated: true)
    }
    request(postRequest).load(load: LoadConfig()).responseJSON { (response) in
      //hide loading
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      switch response.result {
      case .success(_):
        if let data = response.result.value as? [String: Any], let detail = data ["detail"] as? String {
          Utils.showMessageError(message: detail)
          completionHandler(false)
        } else {
          Utils.showMessageSucces(message: "Xóa bài thành công")
          completionHandler(true)
        }
        break
      case .failure(_):
        completionHandler(false)
      }
    }
    // [END delete a Cart]
  }
  
  // [START delete all Cart]
  func deleteAllCart(completionHandler: @escaping(_ isSuccess: Bool) -> Void) {
    
    let postRequest = PostRequest(deleteAll: "")
    //show loading
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.showAdded(to: window, animated: true)
    }
    request(postRequest).load(load: LoadConfig()).responseJSON { (response) in
      //hide loading
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      switch response.result {
      case .success(_):
        if let data = response.result.value as? [String: Any], let detail = data ["detail"] as? String {
          Utils.showMessageError(message: detail)
          completionHandler(false)
        } else {
          Utils.showMessageSucces(message: "Xóa bài thành công")
          completionHandler(true)
        }
        break
      case .failure(_):
        completionHandler(false)
      }
    }
    // [END delete all Cart]
  }

}
