//
//  SettingService.swift
//  Newsbox
//
//  Created by TrungTong on Feb/22/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import ObjectMapper
import MBProgressHUD

struct SettingService: Requestable {
  
  // MARK: Edit setting list
  // [START Edit setting list]
  func editSettingList(listCategory: [String],
                       listLang: [String],
                       listCountry: [String],
                       listRegion: [String],
                       listSouceType: [String],
                       completionHandler: @escaping(_ isSuccess: Bool) -> Void) {
    let settingRequest = SettingRequest(listCategory: listCategory,
                                        listLang: listLang,
                                        listCountry: listCountry,
                                        listRegion: listRegion,
                                        listSouceType: listSouceType)
    //show loading
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.showAdded(to: window, animated: true)
    }
    request(settingRequest).load(load: LoadConfig()).responseJSON { (response) in
      //hide loading
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      switch response.result {
      case .success(_):
        if let data = response.result.value as? [String: Any], let status = data["status"] as? String {
          if status == "success" {
            Utils.showMessageSucces(message: "Cập nhật thành công!")
            completionHandler(true)
          } else {
            Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
            completionHandler(false)
          }
        } else {
          Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
          completionHandler(false)
        }
        break
      case .failure(_):
        Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
        completionHandler(false)
      }
    }
    // [Edit setting list]
  }
  // MARK: Get setting notifyList
  // [START Get setting notifyList]
  func getListNotifyList(completionHandler: @escaping(_ listNotfy: [String]?) -> Void) {
    let settingRequest = SettingRequest()
    //show loading
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.showAdded(to: window, animated: true)
    }
    request(settingRequest).load(load: LoadConfig()).responseJSON { (response) in
      //hide loading
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      switch response.result {
      case .success(_):
        if let data = response.result.value as? [String] {
          completionHandler(data)
        } else {
          completionHandler(nil)
         // Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
        }
      case .failure(_):
        completionHandler(nil)
      //  Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
      }
    }
  }
  // [END Get setting notifyList]
  
  // [START Get setting notifyList]
  func postNotifyList(listID: [String], completionHandler: @escaping(_ success: Bool) -> Void) {
 //   let settingRequest = SettingRequest()
    //show loading
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.showAdded(to: window, animated: true)
    }
     let baseURL: String = UserDefaults.standard.value(forKey: ConstantUD.UDURL) as? String ?? ""
    let url = baseURL + URLConstant.apisettingNotify
    var request = URLRequest(url: URL.init(string: url)!)
    request.httpMethod = HTTPMethod.post.rawValue
    let param = self.json(from: listID)
    
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.setValue(UserManager.shareInstance.userToken, forHTTPHeaderField: Constant.Authorization)
    let data = (param?.data(using: .utf8))! as Data
    
    request.httpBody = data
    
    Alamofire.request(request).responseJSON { (response) in
      //hide loading
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      switch response.result {
      case .success(_):
        if let data = response.result.value {
          completionHandler(true)
        } else {
          completionHandler(false)
          // Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
        }
      case .failure(_):
        completionHandler(false)
        //  Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
      }

    }
//    request(settingRequest).load(load: LoadConfig()).responseJSON { (response) in

//      switch response.result {
//      case .success(_):
//        if let data = response.result.value as? [String] {
//          completionHandler(data)
//        } else {
//          completionHandler(nil)
//          // Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
//        }
//      case .failure(_):
//        completionHandler(nil)
//        //  Utils.showMessageError(message: "Có lỗi xảy ra, vui lòng thử lại sau")
//      }
//    }
  }
  // [END Get setting notifyList]
  func json(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
      return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
  }
}
