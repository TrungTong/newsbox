//
//  IssueService.swift
//  Newsbox
//
//  Created by TrungTong on Feb/19/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import ObjectMapper
import MBProgressHUD

struct IssueService: Requestable {
  // MARK: - Get List Issue
  
  // [START Get List Issue
  func getListIssue(url: String?, completionHandler: @escaping(_ subIssue: SubIssue?, _ error: EnumErrorCode?) -> Void) {
    
    var issueRequest: IssueRequest!
    
    if let urlString = url {
      issueRequest = IssueRequest(url: urlString)
      requestOtherLink(issueRequest).loadWithCallbackError(load: LoadConfig()) { error in
        completionHandler(nil, error)
        }.responseObject() { (response: DataResponse<SubIssue>) in
          if let value = response.result.value {
            //success
            completionHandler(value, EnumErrorCode.success)
          } else {
            // success but parse error
            completionHandler(nil, EnumErrorCode.parseError)
          }
      }
    } else {
      issueRequest = IssueRequest()
      //show loading
//      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
//        MBProgressHUD.showAdded(to: window, animated: true)
//      }
      request(issueRequest).loadWithCallbackError(load: LoadConfig()) { error in
        //error server
        //hide loading
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
          MBProgressHUD.hide(for: window, animated: true)
        }
        completionHandler(nil, error)
        }.responseObject() { (response: DataResponse<SubIssue>) in
          if let value = response.result.value {
            //success
            completionHandler(value, EnumErrorCode.success)
          } else {
            // success but parse error
            completionHandler(nil, EnumErrorCode.parseError)
          }
          //hide loading
          if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
            MBProgressHUD.hide(for: window, animated: true)
          }
      }
    }
    
    
  }
  
  // [END Get list Issue]
  
  // MARK: - Update Issue
  
  // [START Update Issue
  func updateIssue(idIssue: Int,
                   issueTitle: String,
                   duedate: String,
                   createdBy: Int,
                   updatedBy: Int,
                   description: String,
                   updatedTime: String,
                   createdTime: String,
                   assignees: [Int],
                   completed: Bool,
                   starred: Bool,
                   important: Bool,
                   deleted: Bool,
                   completionHandler: @escaping(_ isSuccess: Bool) -> Void) {
    let issueRequest = IssueRequest(idIssue: idIssue,
                                    issueTitle: issueTitle,
                                    duedate: duedate,
                                    createdBy: createdBy,
                                    updatedBy: updatedBy,
                                    description: description,
                                    updatedTime: updatedTime,
                                    createdTime: createdTime,
                                    assignees: assignees,
                                    completed: completed,
                                    starred: starred,
                                    important: important,
                                    deleted: deleted)
    //show loading
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.showAdded(to: window, animated: true)
    }
    request(issueRequest).loadWithCallbackError(load: LoadConfig()) { error in
      //error server
      //hide loading
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      completionHandler(false)
      }.responseObject() { (response: DataResponse<Issue>) in
        if let _ = response.result.value {
          //success
          completionHandler(true)
        } else {
          // success but parse error
          completionHandler(false)
        }
        //hide loading
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
          MBProgressHUD.hide(for: window, animated: true)
        }
    }
  }
  // [End Update Issue]
  
  // MARK: - create Issue
  
  // [START Create Issue
  func createIssue(issueTitle: String,
                   duedate: String,
                   description: String,
                   startTime: String,
                   assignees: [Int],
                   completed: Bool,
                   starred: Bool,
                   important: Bool,
                   completionHandler: @escaping(_ isSuccess: Bool) -> Void) {
    let issueRequest = IssueRequest(issueTitle: issueTitle,
                                    description: description,
                                    startTime: startTime,
                                    duedate: duedate,
                                    completed: completed,
                                    starred: starred,
                                    important: important,
                                    assignees: assignees)
    //show loading
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
      MBProgressHUD.showAdded(to: window, animated: true)
    }
    request(issueRequest).loadWithCallbackError(load: LoadConfig()) { error in
      //error server
      //hide loading
      if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
      }
      completionHandler(false)
      }.responseObject() { (response: DataResponse<Issue>) in
        if let _ = response.result.value {
          //success
          completionHandler(true)
        } else {
          // success but parse error
          completionHandler(false)
        }
        //hide loading
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
          MBProgressHUD.hide(for: window, animated: true)
        }
    }
  }
  // [End Create Issue]
  
}
