//
//  CategorieService.swift
//  Newsbox
//
//  Created by Trung Tong on 2/1/18.
//  Copyright © 2018 Trung Tong. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import ObjectMapper

struct CategorieService: Requestable {
    
    // MARK: - Get List Category, countries, regions, languages
    
    // [START Get List Category, countries, regions, languages]
    func getListCategorie(type: EnumCategory,
                          completionHandler: @escaping(_ listCategorie: [Categorie]?, _ error: EnumErrorCode?) -> Void) {
        
        let categorieRequest = CategorieRequest(listCategory: type)
        
        request(categorieRequest).loadWithCallbackError(load: LoadConfig()) { error in
            //error server
            completionHandler(nil, error)
            }.responseArray() { (response: DataResponse<[Categorie]>) in
                if let value = response.result.value {
                    //success
                    completionHandler(value, EnumErrorCode.success)
                } else {
                    // success but parse error
                    completionHandler(nil, EnumErrorCode.parseError)
                }
        }
    }
    // [END Get List Category, countries, regions, languages]
    
    // MARK: - Source
    
    func getListSource(completionHandler: @escaping(_ listSource: [Source]?, _ error: EnumErrorCode?) -> Void) {
        
        let categorieRequest = CategorieRequest(listCategory: .source)
        
        request(categorieRequest).loadWithCallbackError(load: LoadConfig()) { error in
            //error server
            completionHandler(nil, error)
            }.responseArray() { (response: DataResponse<[Source]>) in
                if let value = response.result.value {
                    //success
                    completionHandler(value, EnumErrorCode.success)
                } else {
                    // success but parse error
                    completionHandler(nil, EnumErrorCode.parseError)
                }
        }
    }
    
    // [START Search source]
    func searchSource(listCategory: [String],
                      listLang: [String],
                      listCountry: [String],
                      listRegion: [String],
                      listSourceType: [String],
                      completionHandler: @escaping(_ listSource: [Source]?, _ error: EnumErrorCode?) -> Void) {
        let categorieRequest = CategorieRequest(listCategory: listCategory,
                                                listLang: listLang,
                                                listCountry: listCountry,
                                                listRegion: listRegion,
                                                listSourceType: listSourceType)
        
        request(categorieRequest).loadWithCallbackError(load: LoadConfig()) { error in
            //error server
            completionHandler(nil, error)
            }.responseArray() { (response: DataResponse<[Source]>) in
                if let value = response.result.value {
                    //success
                    completionHandler(value, EnumErrorCode.success)
                } else {
                    // success but parse error
                    completionHandler(nil, EnumErrorCode.parseError)
                }
        }
    }
    // [END Search source]
}
