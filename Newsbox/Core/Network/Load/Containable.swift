//
//  Containable.swift
//  GTVLiveStream
//
//  Created by TrungTong on Oct/25/17.
//  Copyright © 2017 Nobody. All rights reserved.
//

import Foundation
import UIKit
/// Container protocol for `Loadable`, Objects conforms to this protocol can be used as container for the mask
public protocol Containable {
  func containerView() -> UIView?
}

public extension Containable {
  /// Get latest mask on container
  ///
  /// - Returns: If exists, return latest mask, otherwise return nil
  internal func latestMask() -> UIView? {
    var latestMask: UIView? = nil
    if let container = containerView() {
      for subview in container.subviews {
        if let _ = subview as? Maskable {
          latestMask = subview
        }
      }
    }
    return latestMask
  }
}
/// Mask protocol for `Loadable`, View that conforms to this protocol will be treated as mask
public protocol Maskable {
  var maskId: String { get }
}
