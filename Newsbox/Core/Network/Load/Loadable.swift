//
//  Loadable.swift
//  Jpanet
//
//  Created by TrungTong on Apr/19/17.
//  Copyright © 2017 TrungTong. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
//import SwiftMessages

protocol Loadable {
  func showMessageError(message: String)
  
  /// - Returns: Object that conforms to `Maskable` protocol
  func mask() -> Maskable?
  
  /// Mask Container
  ///
  /// - Returns: Object that conforms to `Containable` protocol
  func maskContainer() -> Containable?
  /// Request begin
  
  func begin()
  
  /// Request end
  func end()
  /// Inset
  ///
  /// - Returns: The inset between mask and maskContainer
  func inset() -> UIEdgeInsets
}
// MARK: - Loadable
extension Loadable {
  /// Default show method for `Loadable`, calling this method will add mask on maskContainer
  func show() {
    if let mask = self.mask() as? UIView {
      var isHidden = false
      if let _ = self.maskContainer()?.latestMask() {
        isHidden = true
      }
      self.maskContainer()?.containerView()?.addSubView(mask, insets: self.inset())
      mask.isHidden = isHidden
      
      if let container = self.maskContainer(), let scrollView = container as? UIScrollView {
        scrollView.setContentOffset(scrollView.contentOffset, animated: false)
        scrollView.isScrollEnabled = false
      }
    }
  }
  /// Default hide method for `Loadable`, calling this method will remove mask from maskContainer
  func hide() {
    if let latestMask = self.maskContainer()?.latestMask() {
      latestMask.removeFromSuperview()
      
      if let container = self.maskContainer(), let scrollView = container as? UIScrollView {
        if false == latestMask.isHidden {
          scrollView.isScrollEnabled = true
        }
      }
    }
  }
}
struct LoadConfig: Loadable {
  func maskContainer() -> Containable? {
    return nil
  }
  
  func mask() -> Maskable? {
    return nil
  }
  
  func showMessageError(message: String) {
    //UIApplication.shared.isNetworkActivityIndicatorVisible = false
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
    }
  }
  func inset() -> UIEdgeInsets {
    return UIEdgeInsets.zero
  }
  
  //    func showMessageError(message: String) {
  //        let error = MessageView.viewFromNib(layout: .CardView)
  //        error.configureTheme(.error)
  //        error.configureContent(title: "Thông báo", body: message)
  //        error.button?.isHidden = true
  //        SwiftMessages.show(view: error)
  //
  //    }
  func begin() {
    show()
  }
  
  func end() {
    hide()
  }
  func show() {
 //   UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
//        MBProgressHUD.showAdded(to: window, animated: true)
//    }
  }
  func hide() {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
        MBProgressHUD.hide(for: window, animated: true)
    }
  }
}
class LoadConfigOther: Loadable {
  func showMessageError(message: String) {
    
  }
  
  init(container: Containable? = nil, mask: Maskable? = MaskView(), inset: UIEdgeInsets = UIEdgeInsets.zero) {
    insetMine = inset
    maskMine = mask
    containerMine = container
  }
  
  func mask() -> Maskable? {
    return maskMine
  }
  
  func inset() -> UIEdgeInsets {
    return insetMine
  }
  
  func maskContainer() -> Containable? {
    return containerMine
  }
  
  func begin() {
    show()
  }
  
  func end() {
    hide()
  }
  
  var insetMine: UIEdgeInsets
  var maskMine: Maskable?
  var containerMine: Containable?
}
