//
//  UIAlertController+Messageable.swift
//  GTVLiveStream
//
//  Created by TrungTong on Dec/3/17.
//  Copyright © 2017 Nobody. All rights reserved.
//

import Foundation
import UIKit
extension UIAlertController: Informable {
  public func show() {
    UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: true, completion: nil)
  }
}

extension UIAlertController: Warnable {
  public func show(error: Errorable?) {
    if let err = error {
      if "" != err.message {
        message = err.message
        
        UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: true, completion: nil)
      }
    }
  }
}
