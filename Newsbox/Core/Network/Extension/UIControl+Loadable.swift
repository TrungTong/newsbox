//
//  UIControl+Loadable.swift
//  GTVLiveStream
//
//  Created by TrungTong on Oct/25/17.
//  Copyright © 2017 Nobody. All rights reserved.
//

import Foundation
import UIKit
// MARK: - Making `UIControl` conforms to `Loadable`
extension UIControl: Loadable {
  func showMessageError(message: String) {
    
  }
  
  func inset() -> UIEdgeInsets {
    return UIEdgeInsets.zero
  }

  public func maskContainer() -> Containable? {
    return self
  }
  
  public func mask() -> Maskable? {
    let mask = ActivityIndicator()
    mask.backgroundColor = backgroundColor
    mask.color = tintColor
    return mask
  }
  
  /// Making it disabled when network request begins
  public func begin() {
    isEnabled = false
    show()
  }
  
  /// Making it enabled when the last network request ends
  public func end() {
    if let latestMask = self.maskContainer()?.latestMask() {
      if false == latestMask.isHidden {
        isEnabled = true
      }
    }
    hide()
  }
}
